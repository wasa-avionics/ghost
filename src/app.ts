/*動作確認環境
  macOS High Sierra v10.13.2
  Node  v8.9.1
  npm   v5.6.0

  by Kazuki Fujita 34代 WASA 電装班長

  2020/04/02 36代平社員 will
  動作確認環境
  windows10 pro (insider)
  Node v12.16.1 (今はv.14.18.2でないとfs/promiseがないと言われて動かない.)
  npm  v6.13.4
  electron @6 .1 .9 + [NOTE][WARNING][ATTENTION] こいつとにかくやばい. electronはnodeとは似て非なるv8エンジンを使っているらしくてそのために, 各パッケージをelectron用にrebuildしなおす必要があるのだけど, 新しいバージョンのやつはパッケージ側が対応していない物があり, npm installやrebuildで失敗する.
*/
//#region その他雑多なモジュール
/*
+ --@mapbox / geojson - area @0 .2 .2 +
  --@mapbox / mbtiles @0 .11 .0 +
  --chart.js @2 .8 .0 +
  --electron - builder @20 .40 .2 +
  --electron - packager @13 .1 .1 +
  --electron - rebuild @1 .10 .1 +
  --electron - settings @3 .2 .0 +
  --express @4 .17 .0 +
  --jquery @3 .4 .1 +
  --jsdoc @3 .6 .3 +
  --mapbox - gl @0 .47 .0 +
  --mocha @6 .1 .4 +
  --mv @2 .1 .1 +
  --node - gyp @4 .0 .0 +
  --serialport @7 .1 .5 +
  --sqlite @3 .0 .3 +
  --standard @12 .0 .1 +
  --tone @0 .12 .80 
  `-- xbee-api@0.6.0
  */
//#endregion
/*
  //test commit//
*/
//#region 詰まった場所
/*
[NOTE] electronではpackage.jsonのmainに記述されたファイルがJSのエントリポイント(ざっくりいうと初めに実行されるもの)になり, つまりこのファイル.
[NOTE] 本プロジェクトは骨子からがらっと変わらないDOMをJavascriptとJqueryを使ってこちょこちょ変えるという手法をとっていますが, Javascriptのいい加減さにキレたので
       Jsdocとts - checkでいずれtypescript化するであろう後続の開発の助けになるように型のサポートを受けています. 
[NOTE] CommonJSのrequire/exportsとES moduleのImport/exportを一つのファイルの中で混在させるとバグるので将来的にES moduleにする際はwebpackするか, 完全にES moduleにすること.
[NOTE] ESmoduleを採用した場合は__dirnameなどの一部のグローバル変数は使えなくなるので注意. 
[NOTE] constructorを介してアクセスする関数群, JSとTSの仕様の違いは全てts-ignoreで対処しています. 
[NOTE] ところどころにthis.constructor.(プロパティ名)でコンストラクタ関数にプロパティを追加している部分がありますが, classベースの言語におけるstaticメンバで, 
       インスタンス生成時に別々にならないものに対応するものとして使っています. 
[NOTE] もしエラーがelectron-rebuildやnpm install時に出て, 文字化けしていた場合にはホームディレクトリ(ユーザー名のディレクトリ)に.bashrcを作って, その中に
       :set nobomb
       chcp.com 65001
       export PAGER = less
       をコピペしてください.文字コードを強制してやります. 
[NOTE] sqlite3でdownload(403) みたいなエラーが出たら, electronのバージョンが高すぎるようなので ^ 6.1.0 あたりを入れてください.
       unix系列ならnode version managerというものが使えるのでもし, 他のプロジェクトでもnodeを使っているならどうぞ
[NOTE] ts-checkとjsdocでなんちゃってTSをやるときは, 他ファイルにあるクラスの型情報を, 使いたいときはマウスを当てた時に, typeof import(~~~~)となってしまわないようにするために
//     requireと一緒に@typedef { import("value").Time } Time これもしてやらないと赤線が出る.
[NOTE] Error: Cannot find module 'fs/promises'
Require stack:
    at Module._compile (internal/modules/cjs/loader.js:1158:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1178:10)
    at Module.load (internal/modules/cjs/loader.js:1002:32)
    at Function.Module._load (internal/modules/cjs/loader.js:901:14)
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! Ghost@1.3.4 postinstall: `electron-builder install-app-deps`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the Ghost@1.3.4 postinstall script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\Users\motop\AppData\Roaming\npm-cache\_logs\2022-06-12T12_47_27_894Z-debug.log

というメッセージが出た場合には, nodeのバージョンが古いので, Node v14.18.2にしましょう. 昔はv12.16.1でも動きましたが, nodeの方で何か変わったようです. 
*/
//#endregion
//#region 技術選択の理由
/*
[NOTE] 技術選択肢 テストモジュール: ts - jest / mocha, モジュールバンドラー: webpack / parcel, 出力JSのバージョンダウン(環境によっては最新のJSが動かないことがあるので調整のために必要): babel
       選択: テストモジュール: mocha 理由: jestに比べて小回りが効きそうなのと, typescriptの型のサポートがjestは微妙らしい ?
       選択 : モジュールハンドラー: webpack: parcelの理解は結局webpackを理解しないとダメっぽいので
       非選択: babel mochaTestでもしなんか動かなかったら入れる.
       選択: vscode利用者はcodeMetricsプラグインの導入をおすすめします. 自分が書いたコードがどの程度分かりやすいコードなのか, cyclomatic complexity的な観点で判別してくれます.
*/
//#endregion

import { app, BrowserWindow, ipcMain, Notification, shell } from 'electron';

import Express from 'express';
const exApp = Express(); // マップデータ配信用expressモジュール

import { dirname, join } from 'path'; // path オブジェクト：内部ファイルのURL化に使用
import { format } from 'url'; // url オブジェクト：内部ファイルのURL化に使用
import { homedir } from 'os'; // OSの情報を取得するライブラリ
import { existsSync, mkdirSync, unlinkSync, readdirSync, rmdir } from 'fs';
import mv from 'mv';
import { file } from 'electron-settings';
// [NOTE] 型定義がないためモジュール読み込み
// eslint-disable-next-line @typescript-eslint/no-var-requires
const MBTiles = require('@mapbox/mbtiles');

const dataPath = dirname(file());
if (!existsSync(dataPath)) {
  mkdirSync(dataPath);
}

const tilesDir = join(dataPath, 'mapTiles'); // マップデータのディレクトリ
if (!existsSync(tilesDir)) {
  mkdirSync(tilesDir);
}

// [NOTE]ウィンドウオブジェクトをグローバル変数化する。
// [NOTE]ガベージコレクション時にウィンドウが閉じてしまう現象を防ぐ
let mainWindow: Electron.BrowserWindow | null;
/*eslint-disable*/
//@ts-ignore これはwebpack側にrequireしていることを伝えてアイコンをstatic以下にに排出してもらうために必要.
const ghostIcon = require('./static/ghost.png');
//@ts-ignore これはcss側から速度の表示計に使われているフォント cssではrequireできないのでここで依存を明確してwebpackに伝える.
const font = require('./stylesheets/Segment7Standard.otf');
/*eslint-enable*/

function createWindow(): void {
  // [NOTE]ブラウザウィンドの生成
  /**
   * @private
   * @type {Electron.BrowserWindow}
   */
  mainWindow = new BrowserWindow({
    width: 1600,
    height: 1000,
    webPreferences: {
      // [NOTE] electronのバージョン5以上はこれ入れないとrendererでのrequireがerrorになるので注意.
      // [TODO]ただしセキュリティ上, 脆弱になるので改良すべし
      nodeIntegration: true,
      // [NOTE] electronのバージョン5以上はこれ入れないとwebviewタグをそのまま使えないので
      // [TODO]ただしセキュリティ上, 脆弱になるので改良すべし
      webviewTag: true,
      // [NOTE]node.jsをelectron's web workerで使う際に必要
      // [NOTE]Web Workers により、 JavaScript を OS レベルのスレッドで実行することが可能になります。
      // [NOTE]この機能を使うならスレッドセーフを気にする必要が出てきます. (Web Worker側の関数で教えてくれるらしい)
      nodeIntegrationInWorker: true,
    },
    // [NOTE] __dirNameには現在実行中のソースコードが格納されているディレクトリパスが格納される.
    icon: join(__dirname, 'static', 'ghost.png'),
  });
  mainWindow.loadURL(
    format({
      pathname: join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true,
    })
  );

  // (デバッグ用) デベロッパ用ツールを開く
  // mainWindow.webContents.openDevTools()

  mainWindow.webContents.setFrameRate(30);

  console.log('Opened Ghost on platform =', process.platform);

  mainWindow.on('closed', function () {
    // [NOTE]null にすることでガベージコレクションを有効にしてヒープを解放する
    mainWindow = null;
  });

  let downloadType = 'Normal';
  // [NOTE]ユーザプロファイルフォルダディレクトリとdounloadsディレクトリでパスを繋げて, クリック時にダウンロードフォルダを開けるようにする.
  let downloadDestination = join(homedir(), 'downloads');
  let downloadItem: Electron.DownloadItem | undefined;
  let downloadItemName: string;

  const downloadCompleteNotification = new Notification({
    title: 'File Download Complete',
    body: 'File Download Complete',
    silent: false,
  });
  // [NOTE] ダウンロード終了時, Notificationをクリックしたらファイルが開かれるようにする
  downloadCompleteNotification.on('click', function () {
    shell.openItem(downloadDestination);
  });

  // [NOTE] ダウンロードされたファイルの削除
  function removeDownloadsFolder(): void {
    const downloadsFile = join(downloadDestination, '.downloads');
    // [NOTE] unlinkはファイル/フォルダの削除を行う.
    if (existsSync(downloadsFile)) {
      if (existsSync(join(downloadsFile, downloadItemName))) {
        unlinkSync(join(downloadsFile, downloadItemName));
      }
      if (existsSync(join(downloadsFile, '.DS_Store'))) {
        unlinkSync(join(downloadsFile, '.DS_Store'));
      }
      // [NOTE]readdirsyncはその直下のファイルのリストを同期的に取得
      const files = readdirSync(downloadsFile);
      if (files && !files.length) {
        rmdir(downloadsFile, function (err) {
          if (err) console.log(err);
        });
      }
    }
  }
  // [NOTE] ダウンロードの最中に諸々行うことをここで設定.
  mainWindow.webContents.session.on('will-download', function (_event, item) {
    downloadItem = item;
    downloadItemName = downloadItem.getFilename();
    // [NOTE]ダウンロード先が存在しているかの確認
    if (!existsSync(join(downloadDestination, '.downloads'))) {
      mkdirSync(join(downloadDestination, '.downloads'));
    }
    item.setSavePath(join(downloadDestination, '.downloads', downloadItemName));
    // [NOTE]ダウンロードの状況を確認
    item.on('updated', function (_event, state) {
      if (state === 'interrupted') {
        console.log('Download is interrupted but can be resumed');
      } else if (state === 'progressing') {
        if (item.isPaused()) {
          console.log('Download is paused');
        } else {
          const receivedBytes = item.getReceivedBytes();
          console.log('Received bytes: ', receivedBytes);
          if (mainWindow !== null && mainWindow.webContents !== null) {
            if (downloadType === 'Map') {
              mainWindow.setProgressBar(receivedBytes / 1740890112);
            }
            // [NOTE]html(view側)に送ってうまいこと表示してくれる
            mainWindow.webContents.send(
              'downloadStatus' + downloadType,
              receivedBytes
            );
          }
        }
      }
    });
    item.once('done', function (_event, state) {
      if (state === 'completed') {
        console.log('Download successfully');
        // [NOTE]デスクトップ通知が有効になっていればダウンロード完了を出す.
        if (Notification.isSupported()) {
          downloadCompleteNotification.show();
        }
        // [NOTE]ダウンロードしたときにフォルダとして.downloadという名前が付いてそこに落とされるのでそれを外してdownloadフォルダに格納.
        mv(
          join(downloadDestination, '.downloads', downloadItemName),
          join(downloadDestination, downloadItemName),
          function (err) {
            if (err) console.log(err);
            removeDownloadsFolder();
            if (mainWindow !== null && mainWindow.webContents !== null) {
              mainWindow.webContents.send(
                'downloadState' + downloadType,
                state
              );
            }
          }
        );
      } else {
        console.log('Download failed: ', state);
        removeDownloadsFolder();
        if (mainWindow !== null && mainWindow.webContents !== null) {
          mainWindow.webContents.send('downloadState' + downloadType, state);
        }
      }

      if (mainWindow !== null) {
        // [NOTE]ダウンロード進行状態表示を削除
        mainWindow.setProgressBar(-1);
      }
    });
  });
  // [NOTE] map.tsからメインプロセスに向けてdownloadFilesイベントが発火されるので監視しておく.
  ipcMain.on('downloadFiles', function (_event, type, fileURL, destination) {
    downloadType = type;
    downloadDestination = destination;
    if (mainWindow !== null) {
      // [NOTE] 今のままだと古いURLなので失敗する.
      mainWindow.webContents.downloadURL(fileURL);
    }
  });

  ipcMain.on('downloadCancel', function (_event, type) {
    if (downloadItem !== undefined) {
      downloadType = type;
      downloadItem.cancel();
      downloadItem = undefined;
      if (mainWindow !== null) {
        mainWindow.setProgressBar(-1);
      }
    }
  });
}

// [NOTE]ウィンドウ生成準備完了時に発生するイベントに対するハンドラー設定
app.on('ready', createWindow);

// [NOTE]全てのウィンドウが閉じられた場合に発生するイベントに対するハンドラー設定
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();
});
// [NOTE]consoleにCPU使用率がどの程度が表示する(1秒間隔)
// [NOTE] メインプロセスでのconsole.logはターミナルに表示されるのでデベロッパツールを見ても何も出ないことに注意.
setInterval(function () {
  const appMetrics = app.getAppMetrics();
  const processes = appMetrics.length;
  let sumPercent = 0;
  for (let i = 0; i < processes; i++) {
    sumPercent += appMetrics[i].cpu.percentCPUUsage;
  }
  console.log('CPU Usage: ' + sumPercent);
}, 1000);

// [NOTE]Expressについてはここ https://expressjs.com/en/4x/api.html expressはサーバーのごたごたをうまくやってくれる.
// [NOTE]そのexpressには静的ファイルなるものがあり, 相対パスでのアクセスができるようになる.ここ見て https://teratail.com/questions/149892
exApp.use('/static', require('express').static(join(__dirname, 'static')));

// [NOTE] 引数に与えられたファイルの拡張子に応じたヘッダーの用意
function getContentType(t: string): expressHeader {
  const header: expressHeader = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers':
      'Origin, X-Requested-With, Content-Type, Accept',
    'Cache-Control': 'public, max-age=604800',
  };
  if (t === 'png') {
    header['Content-Type'] = 'image/png';
  }
  if (t === 'jpg') {
    header['Content-Type'] = 'image/jpeg';
  }
  if (t === 'pbf') {
    header['Content-Type'] = 'application/x-protobuf';
    header['Content-Encoding'] = 'gzip';
  }
  return header;
}

type expressHeader = {
  'Access-Control-Allow-Origin': string;
  'Access-Control-Allow-Headers': string;
  'Content-Type'?: string;
  'Cache-Control'?: string;
  'Content-Encoding'?: string;
};
// [NOTE] mapBoxの内部からgetリクエストが飛んでくるのでそのリクエスト処理を行う. mapBoxをローカルで扱うために必要.
exApp.get('/mapTiles/:s/:z/:x/:y.:t', function (req, res) {
  // console.log("GPS requesting tile: (x,y,z) = (" + req.params.x +", " + req.params.y + ", " + req.params.z + ")")
  let mbtiles = new MBTiles(
    join(tilesDir, req.params.s + '.mbtiles'),
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (err: string | null, mbtiles: any) => {
      mbtiles.getTile(
        req.params.z,
        req.params.x,
        req.params.y,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (err: string | null, tile: any) => {
          if (err) {
            console.log(err);
            const header: expressHeader = {
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Headers':
                'Origin, X-Requested-With, Content-Type, Accept',
              'Content-Type': 'text/plain',
            };
            res.set(header);
            res.status(404).send('Tile rendering error: ' + err + '\n');
          } else {
            res.set(getContentType(req.params.t));
            res.send(tile);
          }
        }
      );
      if (err) console.log('error opening database');
    }
  );
  mbtiles = undefined;
  if (mbtiles) console.log(mbtiles);
});

// start up the server
console.log('Map Server listening on port: ' + 3000);
exApp.listen(3000);
