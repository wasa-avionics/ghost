/*
DONE:
  clock, gauge, generator, map, menu, meter, orientation, speech, secret, graphics, serial, graph, acc

TODO:
  gps, nofly-zone
*/
//#region モジュールのimport.
import $ from 'jquery';

// [NOTE] Uncaught Error: Cannot find module というエラーが出る場合にはパスが通っていないようです.
//        ターミナルでnodeと入力して対話用のを立ち上げて, require.resolve.paths(ここに調べるやつ)を入れてそいつがいるところまでパスがあるかどうか調べましょう.
// [NOTE] 昔はJSだったのでここのパスはHTMLがおかれている場所を基準にパスを書かなければなりませんでしたが, 今はwebpackしているのでsetup.ts基準のパスで大丈夫です.
import { GhostMenu } from './lib/menu';
import {
  ReactiveProperty,
  TimeObservable,
  SerialComFreqObservable,
  AirSpeedReactiveProperty,
} from './lib/reactiveProperty';
import { Logger, LoggedData } from './lib/logger';
import { Secret } from './lib/secret';
import { updateEventEmitterToGraphics, GraphicsObjects } from './lib/graphics';

import { DataGenerator } from './lib/input/generator';
import { Serial } from './lib/input/serial';
import { Playback } from './lib/input/playback';

import { TunnelTab } from './lib/toolTabs/tunnelTab';
import { SCWTab } from './lib/toolTabs/scwTab';
import { GraphTab } from './lib/toolTabs/graphTab/graphTab';
import { ToolTabViewerSwitcher } from './lib/toolTabs/tabSwitcher';

import { Clock } from './lib/display/clock';
import { FreqDisplay } from './lib/display/freqDisplay';
import { Meter, MeterInfo } from './lib/display/meter';
import { Gauge } from './lib/display/gauge/gauge';
import { Orientation } from './lib/display/orientation';
import { Speech } from './lib/display/speech';
import { Plain } from './lib/display/plain';
import { GaugeOptions } from './lib/display/gauge/gaugetypes';
import { MapGraphicsUpdaterCreatorCore } from './lib/display/map/mapGraphicsUpdater';
import { MapCreatorCore } from './lib/display/map/mapCreator';
import { MBTileURLCreatorCore } from './lib/display/map/mbTileURLCreator';
import { MBTileDownloaderCore } from './lib/display/map/mbTileDownloader';
import { Map } from './lib/display/map/map';
//#endregion

const windowNode = $(window);

const DegToRad = Math.PI / 180;

// 色を増やすだけで高度・機速の目盛りが増やせます (下から1目盛り目, 2目盛り目...の順です)
// [NOTE] 色の指定と目盛りの個数の指定, 二つのことをここで行う.
const altimeterColor = [
  '#FF0000',
  '#C0C000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#C0C000',
  '#FF0000',
];
const speedMeterColor = [
  '#FF0000',
  '#FF0000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#00A000',
  '#C0C000',
  '#C0C000',
  '#FF0000',
];

const ghostMenu = new GhostMenu();
// [NOTE] 回転数, ラダー, エレベーターの定義
const cadenceOptions: GaugeOptions = {
  isCadence: true,
  // [NOTE] DegToRadではメーターの目盛りは180度で180まで表示するので, 半円で200まで表示するために, 目盛りの間隔を0.9倍にする
  scaleInterval: DegToRad * 0.9,
  arcStart: Math.PI,
  arcEnd: 0,
  isAntiClockwise: false,
  tickMin: 0,
  tickMax: 200,
  tickDrawOffset: 0,
  tickLargeOffset: 0,
  tickLargeSize: 50,
  tickOffset: -90,
  tickMultiplier: 0.9,
  textOffset: 0,
  textMultiplier: 1,
  fixed: 0,
  gaugeCircleCenterPoint: { horizontalRatio: 0.5, verticalRatio: 0.6 },
  gaugeRadius: { RatioToCanvasWidth: 0.4, RatioToCanvasHeight: 0.6 },
};

const rudderOptions: GaugeOptions = {
  isCadence: false,
  scaleInterval: DegToRad,
  arcStart: (2 * Math.PI) / 3,
  arcEnd: Math.PI / 3,
  isAntiClockwise: false,
  tickMin: -60,
  tickMax: 240,
  tickDrawOffset: 0,
  tickLargeOffset: 180,
  tickLargeSize: 30,
  tickOffset: 0,
  tickMultiplier: 1,
  textOffset: -90,
  textMultiplier: 1,
  fixed: 1,
  gaugeCircleCenterPoint: { horizontalRatio: 0.5, verticalRatio: 0.5 },
  gaugeRadius: { RatioToCanvasWidth: 0.5, RatioToCanvasHeight: 0.5 },
};

const elevatorOptions: GaugeOptions = {
  isCadence: false,
  scaleInterval: DegToRad,
  arcStart: (5 * Math.PI) / 6,
  arcEnd: (7 * Math.PI) / 6,
  isAntiClockwise: true,
  tickMin: -60,
  tickMax: 240,
  tickDrawOffset: 90,
  tickLargeOffset: 180,
  tickLargeSize: 30,
  tickOffset: 90,
  tickMultiplier: -1,
  textOffset: -90,
  textMultiplier: -1,
  fixed: 1,
  gaugeCircleCenterPoint: { horizontalRatio: 0.5, verticalRatio: 0.5 },
  gaugeRadius: { RatioToCanvasWidth: 0.5, RatioToCanvasHeight: 0.5 },
};

/**ログを取る項目とそれの順番を指定できます。下の生データ辞書"data"参考 logger.tsの方でinterfaceを定義してあり, ここに書けるのはそちらに定義した変数名だけなので,
 *もしloggedDataインターフェースに存在しないデータを取りたいとなった時には, まずloggedDataインターフェースにキーの名前と型を追加してください.
 */
const logKeys: (keyof LoggedData)[] = [
  'time',
  'altitude',
  'airSpeed',
  'groundSpeed',
  'cadence',
  'rudder',
  'elevator',
  'yaw',
  'pitch',
  'roll',
  'accelX',
  'accelY',
  'accelZ',
  'calSystem',
  'calAccel',
  'calGyro',
  'calMag',
  'longitude',
  'latitude',
  'satellites',
  'hdop',
  'longitudeError',
  'latitudeError',
  'gpsAltitude',
  'gpsCourse',
  'rudderTemp',
  'rudderLoad',
  'rudderVolt',
  'elevatorTemp',
  'elevatorLoad',
  'elevatorVolt',
  'temperature',
  'humidity',
  'airPressure',
];

window.onload = (): void => {
  /**
   * 生データの辞書
   */
  const data: LoggedData = {
    time: new TimeObservable(),
    serialCommunicationFreqObservable: new SerialComFreqObservable(),
    altitude: new ReactiveProperty(0, 0.01, 0, 1023, 2),
    airSpeed: new AirSpeedReactiveProperty(
      0,
      6.30619595750219e-7 * 1.3126,
      0,
      16255401,
      2,
      0.108955808944537
    ),
    // 風洞試験2回目係数k: 6.12515993890615e-7
    // 風洞試験3回目係数k: 6.15180146890866e-7。 6.30619595750219E-07
    // 2018/07/13 追記: 1.3126は2018年第3回TFのログに基づいて算出した補正値 無風だったので対地機速と対気機速で最小二乗法を算出 (間違えなく正確ではないけどマシにはなるはず)
    // 機速受信値は interrupts / kiloseconds。これを meters / second に直す係数k
    // スリット数は100、FALLINGを検知するのでそのまま使う
    // 0.001 (i/ks -> i/s) * p (i/s -> m/s)
    // 16255401 = 10 /  6.15180146890866e-7
    groundSpeed: new ReactiveProperty(0, 0.001, 0, 12000, 2),
    cadence: new ReactiveProperty(0, 0.0009375, 0, 213333, 0),
    // 回転数受信値は interrupts / kiloseconds。これを rotation / minute に直す係数r
    // スリット数は32、CHANGEを検知するので2倍で64
    // r = 0.0009375
    // = 0.001 (i/ks -> i/s) * 60 (i/s -> i/m)/ 64(i/m -> r/m)
    // 213333 = 200 / 0.0009375
    rudder: new ReactiveProperty(0, 0.1, -1500, 3000, 1),
    elevator: new ReactiveProperty(0, 0.1, -1500, 3000, 1),
    accelX: new ReactiveProperty(0, 0.01, -2000, 4000, 2),
    accelY: new ReactiveProperty(0, 0.01, -2000, 4000, 2),
    accelZ: new ReactiveProperty(0, 0.01, -2000, 4000, 2),
    yaw: new ReactiveProperty(360, 1, 0, 360, 0),
    pitch: new ReactiveProperty(0, 0.01, 0, 36000, 1),
    roll: new ReactiveProperty(0, 0.01, 0, 36000, 1),
    calSystem: new ReactiveProperty(0, 1, 0, 4, 0),
    calAccel: new ReactiveProperty(0, 1, 0, 4, 0),
    calGyro: new ReactiveProperty(0, 1, 0, 4, 0),
    calMag: new ReactiveProperty(0, 1, 0, 4, 0),
    longitude: new ReactiveProperty(
      1395238890,
      0.0000001,
      1395188890,
      100000,
      6
    ),
    latitude: new ReactiveProperty(359752780, 0.0000001, 359702780, 100000, 6),
    satellites: new ReactiveProperty(0, 1, 0, 12, 0),
    hdop: new ReactiveProperty(0, 0.01, 1, 3000, 2),
    longitudeError: new ReactiveProperty(50, 0.1, 0, 1000, 1),
    latitudeError: new ReactiveProperty(50, 0.1, 0, 1000, 1),
    gpsAltitude: new ReactiveProperty(0, 0.01, 1, 1000, 2),
    gpsCourse: new ReactiveProperty(0, 0.01, 0, 36000, 2),
    rudderTemp: new ReactiveProperty(20, 1, 0, 40, 0),
    rudderLoad: new ReactiveProperty(0, 1, 0, 1500, 0),
    rudderVolt: new ReactiveProperty(1130, 0.01, 900, 360, 2),
    elevatorTemp: new ReactiveProperty(20, 1, 0, 40, 0),
    elevatorLoad: new ReactiveProperty(0, 1, 0, 1500, 0),
    elevatorVolt: new ReactiveProperty(1130, 0.01, 900, 360, 2),
    temperature: new ReactiveProperty(0, 0.01, 0, 40, 1),
    humidity: new ReactiveProperty(50, 1, 0, 100, 0),
    airPressure: new ReactiveProperty(101325, 0.01, 90000, 30000, 0),
  };

  // ツールバーの設定
  const weatherTab = new SCWTab('SCW');
  const graphTab1 = new GraphTab('Graphs(1)', {
    object: {
      'Cadence | Altitude': {
        Cadence: {
          reactiveProperty: data.cadence,
          axis: 1,
        },
        Altitude: {
          reactiveProperty: data.altitude,
          axis: 2,
        },
      },
      Speed: {
        Air: {
          reactiveProperty: data.airSpeed,
          axis: 1,
        },
        Ground: {
          reactiveProperty: data.groundSpeed,
          axis: 1,
        },
      },
    },
  });
  const graphTab2 = new GraphTab('Graphs(2)', {
    object: {
      Orientation: {
        Yaw: {
          reactiveProperty: data.yaw,
          axis: 1,
        },
        Pitch: {
          reactiveProperty: data.pitch,
          axis: 1,
        },
        Roll: {
          reactiveProperty: data.roll,
          axis: 1,
        },
      },
    },
  });
  const windTunnelTab = new TunnelTab('Tunnel', data.airSpeed, data.cadence);
  const toolTabs = new ToolTabViewerSwitcher(
    windowNode,
    $('#tool-picker'),
    $('#tool-viewer'),
    [weatherTab, graphTab1, graphTab2, windTunnelTab]
  );

  const freqDisplay = new FreqDisplay(
    data.time,
    data.serialCommunicationFreqObservable,
    $('#freq-display')
  );
  const clock = new Clock(data.time, $('#date-display'), $('#time-display'));

  const mapGraphicsUpdaterCreator = new MapGraphicsUpdaterCreatorCore(
    data.longitude,
    data.latitude,
    data.yaw,
    data.longitudeError,
    data.latitudeError,
    data.hdop,
    data.accelX,
    data.accelY,
    data.groundSpeed,
    data.gpsCourse
  );

  const mapCreator = new MapCreatorCore($('#map'));

  const mbTileURLCreator = new MBTileURLCreatorCore();
  const mbTileDownloader = new MBTileDownloaderCore($('#drag-drop-layover'));
  const map = new Map(
    mapCreator,
    mbTileURLCreator,
    mbTileDownloader,
    mapGraphicsUpdaterCreator,
    ghostMenu,
    $('#mapDragDrop'),
    $('#import-map-button')
  );

  const gpsSatellite = new Plain(data.satellites, $('#gps-satellites'));
  const gpsCourse = new Plain(data.gpsCourse, $('#gps-course'));
  const gpsHdop = new Plain(data.hdop, $('#gps-hdop'));
  const gpsAltitude = new Plain(data.gpsAltitude, $('#gps-altitude'));
  const longitudeError = new Plain(
    data.longitudeError,
    $('#gps-longitude-error')
  );
  const latitudeError = new Plain(data.latitudeError, $('#gps-latitude-error'));

  const altitudeMeter = new MeterInfo(
    data.altitude,
    $('#altitudeMeterArrow'),
    $('#altitudeMeterValue')
  );
  const airSpeedMeter = new MeterInfo(
    data.airSpeed,
    $('#airSpeedMeterArrow'),
    $('#airSpeedMeterValue')
  );
  const groundSpeedMeter = new MeterInfo(
    data.groundSpeed,
    $('#groundSpeedMeterArrow'),
    $('#groundSpeedMeterValue')
  );
  const rightMeter = new Meter(
    windowNode,
    'Altitude',
    true,
    $('#altitude'),
    $('#altitudeMeter'),
    [altitudeMeter],
    altimeterColor
  );
  const leftMeter = new Meter(
    windowNode,
    'Speed',
    false,
    $('#speed'),
    $('#speedMeter'),
    [airSpeedMeter, groundSpeedMeter],
    speedMeterColor
  );

  const cadenceGauge = new Gauge(
    data.cadence,
    windowNode,
    $('#cadenceValue'),
    $('#cadenceGaugeNeedle'),
    $('#cadenceGaugeOutline'),
    cadenceOptions
  );
  const rudderGauge = new Gauge(
    data.rudder,
    windowNode,
    $('#rudderValue'),
    $('#rudderGaugeNeedle'),
    $('#rudderGaugeOutline'),
    rudderOptions
  );
  const elevatorGauge = new Gauge(
    data.elevator,
    windowNode,
    $('#elevatorValue'),
    $('#elevatorGaugeNeedle'),
    $('#elevatorGaugeOutline'),
    elevatorOptions
  );

  const yawOrientation = new Orientation(data.yaw, $('#yawPlane'));
  const pitchOrientation = new Orientation(data.pitch, $('#pitchPlane'));
  const rollOrientation = new Orientation(data.roll, $('#rollPlane'));

  const yawValue = new Plain(data.yaw, $('#orientation-yaw'), -360);
  const pitchValue = new Plain(data.pitch, $('#orientation-pitch'));
  const rollValue = new Plain(data.roll, $('#orientation-roll'));

  const accelX = new Plain(data.accelX, $('#accel-x'));
  const accelY = new Plain(data.accelY, $('#accel-y'));
  const accelZ = new Plain(data.accelZ, $('#accel-z'));

  const calSystem = new Plain(data.calSystem, $('#cal-system'));
  const calAccel = new Plain(data.calAccel, $('#cal-accel'));
  const calGyro = new Plain(data.calGyro, $('#cal-gyro'));
  const calMag = new Plain(data.calMag, $('#cal-mag'));

  const rudderTemp = new Plain(data.rudderTemp, $('#rudderTemp'));
  const rudderLoad = new Plain(data.rudderLoad, $('#rudderLoad'));
  const rudderVolt = new Plain(data.rudderVolt, $('#rudderVolt'));
  const elevatorTemp = new Plain(data.elevatorTemp, $('#elevatorTemp'));
  const elevatorLoad = new Plain(data.elevatorLoad, $('#elevatorLoad'));
  const elevatorVolt = new Plain(data.elevatorVolt, $('#elevatorVolt'));

  const temperature = new Plain(data.temperature, $('#temperature'));
  const humidity = new Plain(data.humidity, $('#humidity'));
  const airPressure = new Plain(data.airPressure, $('#air-pressure'));

  const speech = new Speech(
    data.groundSpeed,
    $('#speech-icon'),
    $('#speech-status'),
    $('#speech-button')
  );

  const graphicObjects: GraphicsObjects = [
    map,
    gpsSatellite,
    gpsCourse,
    gpsHdop,
    gpsAltitude,
    longitudeError,
    latitudeError,
    altitudeMeter,
    airSpeedMeter,
    groundSpeedMeter,
    cadenceGauge,
    rudderGauge,
    elevatorGauge,
    yawOrientation,
    pitchOrientation,
    rollOrientation,
    yawValue,
    pitchValue,
    rollValue,
    calSystem,
    calAccel,
    calGyro,
    calMag,
    accelX,
    accelY,
    accelZ,
    rudderTemp,
    rudderLoad,
    rudderVolt,
    elevatorTemp,
    elevatorLoad,
    elevatorVolt,
    temperature,
    humidity,
    airPressure,
  ];

  const logger = new Logger(
    data,
    logKeys,
    $('#log-icon'),
    $('#log-status'),
    $('#log-button'),
    $('#log-filename'),
    $('#select-log-button'),
    $('#log-dir')
  );
  const graphicsManager = new updateEventEmitterToGraphics(
    graphicObjects,
    $('#graphic-icon'),
    $('#graphic-status'),
    $('#graphic-button')
  );

  const playback = new Playback(
    graphicsManager,
    data,
    logger,
    $('#playback-icon'),
    $('#playback-status'),
    $('#playback-button')
  );
  const dataGenerator = new DataGenerator(
    graphicsManager,
    logger,
    data,
    ghostMenu,
    $('#debug-icon'),
    $('#debug-status'),
    $('#debug-button')
  );
  const serial = new Serial(
    graphicsManager,
    logger,
    data,
    $('#serial-list'),
    $('#refresh-serial'),
    $('#connect-icon'),
    $('#connect-status'),
    $('#connect-button')
  );

  const secret = new Secret($('#cover'));
  const objectArray = [
    clock,
    freqDisplay,
    map,
    altitudeMeter,
    airSpeedMeter,
    rightMeter,
    leftMeter,
    cadenceGauge,
    rudderGauge,
    elevatorGauge,
    yawOrientation,
    pitchOrientation,
    rollOrientation,
    dataGenerator,
    logger,
    playback,
    graphicsManager,
    serial,
    speech,
    toolTabs,
    secret,
  ];
  console.log('Debug', objectArray);
  ghostMenu.update();
};
