import EventEmitter from 'events';
// [NOTE] serialMultiplierという謎のものが入っているので(多分単位のためのもの)この性質が確定し次第名前がまた変更される可能性が有り.

/**
 * 値の変更時に外部にupdateイベントを発火します(reactive)
 */
export class ReactiveProperty extends EventEmitter {
  /**
   * データ本体
   */
  protected value: number | Date;
  /**
   *[NOTE] Arduinoの方では, floatの演算が怖い/遅い/シリアル通信で送るのが面倒(floatのデータ構造が面倒)なので使ってなくて,全てintで送られてくるのですが,
   *       単位がめちゃめちゃになっている中で, 整数値から小数への変換を行ってやる必要があり, その変換のためにserialMultiplierが存在しています.
   */
  public intToFloatMultiplierForUnit: number;
  /**
   * 数値の自動生成を行う際に必要. Math.randomでランダムに作られた値にinputSizeでかけた値に足す.
   */
  protected inputOffsetForRandomGenerator: number;
  /**
   * 数値の自動生成を行う際に必要. 0~1でしか生成できないMath.Randomにこの値を掛けて, 値を生成.
   */
  protected inputSizeForRandomGenerator: number;
  /**
   * 小数点以下をどこまで表示するか. toFixedで用いる.0~20の整数値.
   */
  protected outputFixed: number;
  protected lastValue: number | Date;
  /**
   * 前回の更新時と今回の更新時で同じ値であるかどうか.
   */
  protected dupe: boolean;

  constructor(
    initial: Date | number = 0,
    serialMultiplier = 1,
    inputOffsetForRandomGenerator = 0,
    inputSizeForRandomGenerator = 1,
    outputFixed = 1
  ) {
    super();
    this.value = initial;
    this.intToFloatMultiplierForUnit = serialMultiplier;
    this.inputOffsetForRandomGenerator = inputOffsetForRandomGenerator;
    this.inputSizeForRandomGenerator = inputSizeForRandomGenerator;
    this.outputFixed = outputFixed;
    this.lastValue = initial;
    this.dupe = false;
  }

  /**
   * このreactivePropertyを監視しているものに向けて, 値が変化したことを知らせたいのでupdateイベントをemitする.
   */
  public setValue(val: number | Date): boolean {
    this.value = val;
    this.dupe = this.value === this.lastValue;
    this.lastValue = this.value;
    this.emit('update');
    return true;
  }

  public getValue(): number {
    return parseFloat(
      ((this.value as number) * this.intToFloatMultiplierForUnit).toFixed(
        this.outputFixed
      )
    );
  }

  public getRawValue(): number | Date {
    return this.value;
  }

  /**
   * 動作確認のために使う. ランダムな値の生成
   */
  public setRandom(): void {
    // [XXX] purseIntをなぜかませるのか. 多分小数点以下を切りたかった感じだと思われます. JSは小数点周りが怪しいので
    this.setValue(
      parseInt(
        (
          Math.random() * this.inputSizeForRandomGenerator +
          this.inputOffsetForRandomGenerator
        ).toString()
      )
    );
  }
  /**
   * 前回の更新時と今回の更新時に同じ値かどうかを返す.
   */
  public isDupe(): boolean {
    return this.dupe;
  }
}

/**
 * きっかり一秒ごとに監視先にupdateイベントを発火するクラス.
 */
export class TimeObservable extends ReactiveProperty {
  // [TODO] ここのpublicは後で直す.
  // [NOTE] HTML上で日付と時刻を別々にしているために, 時刻と日付を分ける.
  public dateString: string;
  public timeString: string;
  constructor() {
    super(new Date(), 1, 0, 1, 0);
    this.dateString = (this.value as Date).toLocaleDateString();
    this.timeString = (this.value as Date).toLocaleTimeString();
    this.updateTime();
  }

  private updateTime(): void {
    this.value = new Date();
    this.dateString = this.value.toLocaleDateString();
    this.timeString = this.value.toLocaleTimeString();
    this.createTimeoutEmittingUpdateEventInPreciselyOneSecond(this.value);
  }

  private createTimeoutEmittingUpdateEventInPreciselyOneSecond(
    value: Date
  ): void {
    // [NOTE] 一秒きっかりごとに更新したいために, valueを更新したら早速そのvalueを用いて上手く1秒後に更新が来るようにしてやる.
    setTimeout(() => {
      this.updateTime();
      this.emit('update');
    }, 1000 - (value.getTime() % 1000));
  }

  public getValue(): number {
    return Date.now();
  }
  // [NOTE] timeはランダムな値を生成しなくても毎秒更新されるのが分かるので, 不要. undefinedの実行時エラーを防止するために空の関数?.
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public setRandom(): void {}
}

/**
 * serial通信の頻度を管理するクラス.
 * TimeObservableクラスと合わせて用い, resetメソッドを一秒後に登録することで, このクラスのvalueは周波数の意味になる.
 * 一つにしてしまえばよいとも思われるかもしれないが, 時間のほうは一つだけsetup.tsで定義した後,
 * 参照をたらいまわしにされて, いろいろなクラスから参照されるので, 分けておいたほうが自然.
 */
export class SerialComFreqObservable extends ReactiveProperty {
  setRandom(): void {
    this.increment();
  }
  /**
   * [XXX] なんでbooleanを返しているのですかね.
   */
  increment(): boolean {
    (this.value as number)++;
    this.emit('update');
    return true;
  }

  reset(): void {
    this.value = 0;
  }
}

export class AirSpeedReactiveProperty extends ReactiveProperty {
  /**
   * プロペラ機速計は速度が上がってくると, プロペラの回転時の抵抗が無視できなくってきて, 値がずれていくので, せめてもの調整用.
   * 現在は, 送られてきた機速が1m/sを超えると, この値を足して調整するようにしている.
   */
  private serialOffset: number;

  constructor(
    initial: Date | number = 0,
    serialMultiplier = 1,
    inputOffset = 0,
    inputSize = 1,
    outputFixed = 1,
    serialOffset = 0
  ) {
    super(initial, serialMultiplier, inputOffset, inputSize, outputFixed);
    this.serialOffset = serialOffset;
  }
  /**
   *  プロペラ計測なので, 送られてきた機速が1m/sを超えると, serialOffsetの値を足して, 高速度域の速度を真の値に近づける処理を行う.
   */
  getValue(): number {
    const airSpeed = parseFloat(
      ((this.value as number) * this.intToFloatMultiplierForUnit).toFixed(
        this.outputFixed
      )
    );
    if (airSpeed > 1) {
      return airSpeed + this.serialOffset;
    } else {
      return airSpeed;
    }
  }
}
