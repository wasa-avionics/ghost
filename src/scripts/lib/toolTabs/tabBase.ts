import EventEmitter from 'events';
import { ToolTabViewerSwitcher } from './tabSwitcher';

interface UpdatableTab {
  updateContent(): void;
}

interface HidableTab {
  hideContent(): void;
}

/**
 * 現状では左下に情報が色々と出てくるところのバックエンド側を担当するモジュール タブの基本クラス
 */
export class EmptyTab extends EventEmitter implements UpdatableTab, HidableTab {
  /**
   * 名前は公開する. tabSwitcherで使う.
   */
  public name: string;
  /**
   * HTMLElementを入れておくと, タブがクリックされた時にtabSwitcherがviewに配置してくれる.
   */
  protected content: undefined | JQuery;
  /**
   * このタブのことを扱ってくれるtabSwitcherへの参照を保持. 相互参照になってるにでtabSwitcherと密結合.個人的には直したい.
   */
  public tabSwitcher: undefined | ToolTabViewerSwitcher;
  /**
   * tabSwitcherから呼ばれる. 初期化もtabSwitcher側で行われるので順番に注意
   */
  public ClickableTabNameNode: JQuery<HTMLElement> | undefined;
  constructor(name: string) {
    super();
    this.name = name;
    this.content = undefined;
    //#region イベントリスナーの登録
    // [NOTE] tabSwitcherから流れてくる.
    this.on('click', (): void => {
      this.displayContent();
    });
    this.on('unclick', (): void => {
      this.hideContent();
    });
    //#endregion
  }

  // [NOTE] tabSwitcherプロパティの定義はtabSwitcher内で行っています. 闇
  protected displayContent(): void {
    if (this.tabSwitcher) {
      this.tabSwitcher.display(this);
      this.updateContent();
    }
  }
  // [NOTE] tabSwitcherからHTMLElementを拾ってくる時に必要.
  public getContent(): undefined | JQuery {
    return this.content;
  }

  public setContent(content: JQuery): void {
    this.content = content;
  }

  // [NOTE] 継承先でオーバーライドされる.
  updateContent(): void {
    return;
  }

  hideContent(): void {
    return;
  }
}
