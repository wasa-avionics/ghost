import { ReactiveProperty } from '../../reactiveProperty';

export type graphTitleNames = 'Speed' | 'Cadence | Altitude' | 'Orientation';

export type chartKeyNames =
  | 'Air'
  | 'Ground'
  | 'Yaw'
  | 'Pitch'
  | 'Roll'
  | 'Cadence'
  | 'Altitude';

// [NOTE] 暗黙的に, [graphTitleNamesにある名前], [colorDictionaryにある名前], [valueWithAxis]という三段階でデータを構成してくれと
//        制限が課せられているのはよくないかもしれない.
export type SecondLayerObjectForChart = {
  [P in chartKeyNames]?: ValueWithAxis;
};

export type FirstLayerObjectForChart = {
  [p in graphTitleNames]?: SecondLayerObjectForChart;
};

export interface ObjectForChart {
  object: FirstLayerObjectForChart;
}

export interface ValueWithAxis {
  // [NOTE] 生データが入る
  reactiveProperty: ReactiveProperty;
  // [NOTE] Ghost中央の両側にある軸のどちらに表示するか. 使うのは高度が右側であることをいうときだけだと思われます
  axis: number;
}
