// [NOTE] グラフ描画用のタブ. 多分左下に出てくる部分でchart.jsでグラフ描画してる部分.
import $ from 'jquery';
import { EmptyTab } from '../tabBase';
import { UpdatableChart } from './upDatableChart';
import {
  ObjectForChart,
  graphTitleNames,
  FirstLayerObjectForChart,
  SecondLayerObjectForChart,
} from './graphTabTypeDeterminer';

export class GraphTab extends EmptyTab {
  private dataObjects: FirstLayerObjectForChart;
  private graphTitledChart: { [P in graphTitleNames]?: UpdatableChart };
  private graphCount: number;
  static tabs: EmptyTab[];
  constructor(name: string, objectForChart: ObjectForChart) {
    super(name);
    this.dataObjects = objectForChart.object;
    this.graphTitledChart = {};
    this.content = $('<div></div>');
    this.content.attr('class', 'vert-wrapper tool-viewer-inner');
    this.content.css({ width: '100%' });
    this.graphCount = Object.keys(objectForChart.object).length;

    (Object.keys(this.dataObjects) as graphTitleNames[]).forEach(
      (key): void => {
        this.initializeChartData(key);
        const chartNode: JQuery<HTMLCanvasElement> = this.createChartNode(key);
        this.createChart(key, chartNode);
        this.appendChartNode(chartNode);
      }
    );

    // [NOTE] このupdateイベントはserialとgeneratorから呼ばれた時刻とともに発火される.
    this.on('update', (time): void => {
      this.updateTab(time);
    });
    // [NOTE このresizeイベントはtabSwitcher側から呼ばれる.
    this.on('resize', (): void => {
      this.displayContent();
    });
    // [NOTE] 全てのGraphTabを一括管理できるようにする.
    GraphTab.tabs.push(this);
  }

  private initializeChartData(graphTitleName: graphTitleNames): void {
    const secondLayerObjects: SecondLayerObjectForChart = this.dataObjects[
      graphTitleName
    ] as Required<SecondLayerObjectForChart>;
    // [NOTE] 初期化
    this.graphTitledChart[graphTitleName] = new UpdatableChart(
      secondLayerObjects,
      graphTitleName,
      undefined
    );
  }

  private appendChartNode(chartNode: JQuery<HTMLCanvasElement>): void {
    // [NOTE] 作ったインライン要素を入れるブロック要素の作成.
    const chartContainer = $("<div class='chart-container'></div>");
    chartContainer.css('height', (100 / this.graphCount).toFixed(1) + '%');
    // [NOTE] インライン要素をブロック要素に入れる.
    chartNode.appendTo(chartContainer);
    if (this.content) {
      chartContainer.appendTo(this.content);
    }
  }

  private createChartNode(
    graphTitleName: graphTitleNames
  ): JQuery<HTMLCanvasElement> {
    const chartNode: JQuery<HTMLCanvasElement> = $('<canvas></canvas>');
    // [NOTE] インライン要素の作成
    chartNode.attr('id', graphTitleName + '-chart');
    chartNode.attr('class', 'graph-outline');
    return chartNode;
  }

  private createChart(
    graphTitleName: graphTitleNames,
    chartNode: JQuery<HTMLCanvasElement>
  ): void {
    if (!this.graphTitledChart[graphTitleName]) {
      console.log(new Error('存在しないUpdatableChartが参照されました.'));
      return;
    }
    if (!this.graphTitledChart[graphTitleName]?.getIsCreated()) {
      this.graphTitledChart[graphTitleName]?.createChart(chartNode);
      return;
    }
    this.graphTitledChart[graphTitleName]?.update();
  }

  /**
   * updatableChartに対してデータの更新を行う
   * @param {Number} time
   */
  private updateTab(time: number): void {
    // kindの中身ごとに
    (Object.keys(this.graphTitledChart) as graphTitleNames[]).forEach((key) => {
      this.graphTitledChart[key]?.reflrectCurrentData(time);
      if (this.tabSwitcher && this.tabSwitcher.nowSelectedTab === this) {
        this.graphTitledChart[key]?.update();
      }
    });
  }

  /**
   * tabBaseのオーバーライド
   */
  public updateContent(): void {
    (Object.keys(this.graphTitledChart) as graphTitleNames[]).forEach((key) => {
      this.graphTitledChart[key]?.render();
    });
  }
  /**
   * tabBaseのオーバーライド
   */
  public hideContent(): void {
    (Object.keys(this.graphTitledChart) as graphTitleNames[]).forEach((key) => {
      this.graphTitledChart[key]?.stop();
    });
  }
}

GraphTab.tabs = [];
