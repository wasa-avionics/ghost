import { ChartOptions, ChartData, ChartPoint } from 'chart.js';
import Chart from 'chart.js';
import {
  chartKeyNames,
  SecondLayerObjectForChart,
  graphTitleNames,
} from './graphTabTypeDeterminer';
import { ReactiveProperty } from '../../reactiveProperty';

/**
 * データの入ったオブジェクトと, このインスタンス一つで担当するchartのグラフタイトル(graphTitleNames型に相当)を引数に取って, Chartの更新を担当する.
 * このクラスの担当するオブジェクトはgraphTitleNames分一つだけ.
 * [TODO] これもreactiveに動かすことができそうなのでやる.
 */
export class UpdatableChart {
  private timeLabels: Array<number>; // chartDataの中にLabelがあり, そこに突っ込む用の値. グラフの横軸になる.
  private data: ChartData; // labelとdatasetの入れ物. ここにtimeLabelsを突っ込み, datasetsを用意してひたすらpushしていく. グラフの横軸とプロットを保持する.
  private chartPointData: { [P in chartKeyNames]?: ChartPoint[] }; // グラフ上の座標データ. updateの度にセットする.
  private chart: Chart | undefined; // chart本体
  private DictionaryToPlotValue: {
    [P in chartKeyNames]?: ReactiveProperty;
  };
  private Options: ChartOptions; // いたるところでいじっていたのでここに置く必要があった.
  private static readonly colorDictionary: {
    [P in chartKeyNames]: string;
  } = {
    Air: '#1E90FF',
    Ground: '#FA8072',
    Yaw: '#7FFF00',
    Pitch: '#B22222',
    Roll: '#8A2BE2',
    Cadence: '#0000FF',
    Altitude: '#FF0000',
  };
  private isCreated: boolean;

  // [NOTE] 間違ってもこのクラスでFirstLayerをいじることのないようにSecondLayerを受け取る.
  public constructor(
    SecondLayerObject: SecondLayerObjectForChart,
    graphTitle: graphTitleNames,
    option?: ChartOptions
  ) {
    // [NOTE] 配列とオブジェクトの初期化
    this.timeLabels = [];
    this.data = {
      labels: this.timeLabels, // ここで参照渡ししているので, timeLabelsをいじればchartにも情報が伝達される.
      datasets: [],
    };
    this.chartPointData = {};

    this.DictionaryToPlotValue = {};
    this.Options =
      option === undefined ? this.createOption(graphTitle) : option;
    this.isCreated = false;

    this.setData(SecondLayerObject);
  }

  /**
   * オブジェクト内のreactivePropertへの参照をDatasetとDictionaryに持たせる.
   * @param secondLayerObject 各種reactivePropertyへの参照が入っているオブジェクト
   */
  private setData(
    secondLayerObject: SecondLayerObjectForChart | undefined
  ): void {
    if (secondLayerObject === undefined) {
      console.log(new Error('Objectがundefinedです.'));
      return;
    }
    this.getSecondLayerkeys(secondLayerObject).forEach((key) => {
      type notOptional = Required<SecondLayerObjectForChart>;
      this.prepareDataset(key, secondLayerObject as notOptional);

      if ((secondLayerObject as notOptional)[key].axis === 2) {
        this.enableSecondYAxis(this.Options);
      }
      this.prepareDictionary(key, secondLayerObject as notOptional);
    });
  }
  //#region 補助的なメソッド
  /**
   * 各keyごとにグラフを書くために, 各keyごとにDatasetsへデータの参照を渡す.Datasetsは別々にpushした場合には一つのグラフ上に二つの線を書いてくれる.
   * @param key Datasetを用意するkey
   * @param color keyに対応したグラフの色を決定する色
   * @param secondLayerObject 参照の入れ物
   */
  private prepareDataset(
    key: chartKeyNames,
    secondLayerObject: Required<SecondLayerObjectForChart>
  ): void {
    if (this.data.datasets === undefined) {
      console.log(new Error('ChartDataSetsがundefinedです'));
      return;
    }

    // [NOTE] 配列を入れろとchartが言っているので一つだけデフォルト値が入っている配列で初期化. 何か入れておかないとundefinedのままになるので必要.
    const defaultPoint = this.createChartPoint(0, 0);
    this.chartPointData[key] = [defaultPoint];

    this.data.datasets.push({
      label: key,
      backgroundColor: this.stringToColour(key),
      borderColor: this.stringToColour(key),
      data: this.chartPointData[key], // この参照により, PointDataへガンガンpushすれば新しいプロットがでるようになっている.
      fill: false,
      // [NOTE] optionにあわせているのでoptionを見て.
      yAxisID: 'y-axis-' + secondLayerObject[key].axis,
    });
  }

  /**
   * 新しいデータが来た時のChartの更新に備えて, 辞書にreactivePropertyへの参照を保持する.
   * @param key secondLayerobjectに入っていることが保障されているkey
   * @param secondLayerObject 参照の入れ物
   */
  private prepareDictionary(
    key: chartKeyNames,
    secondLayerObject: Required<SecondLayerObjectForChart>
  ): void {
    if (this.DictionaryToPlotValue[key] === undefined) {
      this.DictionaryToPlotValue[key] = secondLayerObject[key].reactiveProperty;
    }
  }
  //#endregion

  /**
   * プロットを新しく追加する. Chartをupdateする前にこれを呼ばなければならない.
   * 更新される横ラベルとプロットの一致はそれぞれ同時に一つづつ更新され, 古いものが削られることによって保障されている.
   * @param time 現在の時間. この時間がグラフの横軸に増える.
   */
  public reflrectCurrentData(time: number): void {
    if (this.chart === undefined) {
      console.log(new Error('Chartが作成されていません'));
      return;
    }
    const timeLabelLength = this.pushCurrentTimeToLimitedLengthXAxisWith(
      time,
      200
    );
    // [NOTE] 各キーごとに異なるData配列があるので別々にpushする
    (Object.keys(this.chartPointData) as chartKeyNames[]).forEach((key) => {
      const newChartPoint: ChartPoint = this.createChartPoint(
        time,
        this.DictionaryToPlotValue[key]?.getValue()
      );
      this.pushCurrentDataToPlotDataWith(
        newChartPoint,
        this.chartPointData[key] as ChartPoint[],
        timeLabelLength
      );
    });
  }
  //#region 補助的なメソッド
  /**
   * 与えられたChartPointをプロットデータに追加し, 横軸の長さとプロットの数をそろえる.
   * @param newChartPoint 新しく追加するプロットデータ
   * @param plotDataArray 追加されるプロットデータ
   * @param timeLabelLength 時間の横軸とデータの数を合わせるために必要な現在の横軸のコマ数
   */
  private pushCurrentDataToPlotDataWith(
    newChartPoint: ChartPoint,
    plotDataArray: ChartPoint[],
    timeLabelLength: number
  ): void {
    plotDataArray.push(newChartPoint);
    // 横軸と不一致が起きるので, 生データのほうが長くなったら切り落とす.
    while (plotDataArray.length > timeLabelLength) {
      plotDataArray.shift();
    }
  }

  /**
   * 新しく横軸に時間を追加し, 横軸の限界値を超えないように配列を調整したのち, 現在の横軸に使われている配列の長さを返す.
   * @param time 新しく横軸に追加する時間
   * @param lengthLimit 横軸に表示する時間のコマ数の限界値
   */
  private pushCurrentTimeToLimitedLengthXAxisWith(
    time: number,
    lengthLimit: number
  ): number {
    this.timeLabels.push(time);
    while (this.timeLabels.length > lengthLimit) {
      this.timeLabels.shift();
    }
    return this.timeLabels.length;
  }
  //#endregion

  /**
   * 引数に受け取ったノードにChartを作成する. はじめはstopメソッドで停止しているので,render/updateメソッドを呼び出して描画, 更新を行わせてください.
   * @param chartNode
   */
  public createChart(chartNode: JQuery<HTMLCanvasElement>): void {
    if (this.chart !== undefined) {
      return;
    }
    this.chart = new Chart(chartNode, {
      type: 'line',
      data: this.data,
      options: this.Options,
    }).stop();
    this.isCreated = true;
  }

  public stop(): void {
    this.chart?.stop();
  }

  public render(): void {
    this.chart?.render();
  }

  public update(): void {
    this.chart?.update();
  }

  public getIsCreated(): boolean {
    return this.isCreated;
  }

  //#region 補助的な関数群.

  /**
   * 与えられた値からChartPointを生成する.
   * @param time x軸の値
   * @param yAxisPlotData y軸の値
   */
  private createChartPoint(
    time: number,
    yAxisPlotData: number | undefined
  ): ChartPoint {
    if (yAxisPlotData === undefined) {
      console.log(new Error('Chartに入れるDataにundefinedが渡されました.'));
    }
    return { x: time, y: yAxisPlotData };
  }

  private getSecondLayerkeys(
    SecondLayerObject: SecondLayerObjectForChart
  ): Array<chartKeyNames> {
    return Object.keys(SecondLayerObject) as Array<chartKeyNames>;
  }
  /**
   * グラフのタイトルはデータによって変わるので, その名前を引数にとってoptionを動的に作成する.
   * @param graphTitle グラフのタイトルにつける名前. 二段目の名前が入る.
   */
  private createOption(graphTitle: string): ChartOptions {
    return {
      maintainAspectRatio: false,
      title: {
        display: true,
        text: graphTitle,
        fontColor: 'white',
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true,
      },
      legend: {
        labels: {
          fontColor: 'white',
        },
      },
      animation: {
        duration: 0,
      },
      scales: {
        xAxes: [
          {
            display: true,
            type: 'time',
            distribution: 'linear',
            bounds: 'ticks',
            scaleLabel: {
              display: true,
              labelString: 'Time',
              fontColor: 'white',
            },
            time: {
              unit: 'second',
              parser: 'x',
              tooltipFormat: 'll HH:mm:ss',
              displayFormats: {
                second: 'HH:mm:ss',
              },
            },
            gridLines: {
              display: false,
              color: 'rgba(255, 255, 255, 1)',
            },
            ticks: {
              fontColor: 'white',
            },
          },
        ],
        yAxes: [
          {
            display: true,
            gridLines: {
              color: 'rgba(255, 255, 255, 1)',
            },
            ticks: {
              fontColor: 'white',
            },
            position: 'left',
            id: 'y-axis-1',
          },
          {
            display: false,
            gridLines: {
              color: 'rgba(255, 0, 0, 1)',
            },
            ticks: {
              fontColor: 'white',
            },
            position: 'right',
            id: 'y-axis-2',
          },
        ],
      },
    };
  }
  private stringToColour(str: chartKeyNames): string {
    const color = UpdatableChart.colorDictionary[str as chartKeyNames];
    // [NOTE] typescriptを騙して型をすり抜けてきた場合にもchartに表示する色がないと困るので.
    return color !== undefined ? color : this.stringToRandomColour(str);
  }

  /**
   * もしcolorDictionaryに対応する色が存在しない場合には, 有名なハッシュ関数を用いて, 文字列から色を生成します.
   * chart.jsはHexもRGBも受け付けるので大丈夫です.
   */
  private stringToRandomColour(strNotIncludedInDictionary: string): string {
    let hash = 0;
    for (let i = 0; i < strNotIncludedInDictionary.length; i++) {
      hash = strNotIncludedInDictionary.charCodeAt(i) + ((hash << 5) - hash); // ハッシュの作成
    }
    let colour = 'rgba(';
    for (let i = 0; i < 3; i++) {
      const value = (hash >> (i * 8)) & 0xff; //この&で0~255の範囲に決定できる.
      colour += value.toString() + ',';
    }
    colour += '1)';
    return colour;
  }

  private enableSecondYAxis(options: ChartOptions): void {
    // [NOTE] 単に再帰的に全てのoptionalを外すための型.それ以外の意味はない.
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    type DeepRequired<T> = T extends any[]
      ? DeepRequiredArray<T>
      : T extends object
      ? DeepRequiredObject<T>
      : T;
    type DeepRequiredArray<T> = {
      [P in keyof T]: DeepRequired<NonNullable<T[P]>>;
    };
    type DeepRequiredObject<T> = {
      [P in NonFunctionPropertyNames<T>]: DeepRequired<NonNullable<T[P]>>;
    };
    type NonFunctionPropertyNames<T> = {
      [K in keyof T]: T[K] extends Function ? never : K;
    }[keyof T];

    // 右側y軸を必要に応じて設定
    (options as DeepRequired<ChartOptions>).scales.yAxes[0].gridLines.color =
      'rgba(0, 0, 255, 1)';
    (options as DeepRequired<ChartOptions>).scales.yAxes[1].display = true;
  }

  //#endregion
}
