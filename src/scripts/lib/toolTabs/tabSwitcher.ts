//@ts-check
import $ from 'jquery';
import EventEmitter from 'events';
import { EmptyTab } from './tabBase';
// [NOTE] 画面のタブへのクリックに際して, 動的にview側にHTMLElementを削除, 作成し, リサイズ時にwindowから伝わってくるresizeや, clickを現在表示中のタブに伝える.
export class ToolTabViewerSwitcher extends EventEmitter {
  /**
   * resizeイベントを拾ってくるために必要
   */
  private windowNode: JQuery<Window & typeof globalThis>;
  /**
   * ユーザーにタブを選択をさせられるようにタブの種類を表示しておくノード. tabArrayで渡されたTabはccreateClickableTabNameVisibleListWithにて全てここにappendされる.
   */
  private pickerNode: JQuery<HTMLElement>;
  /**
   * 選択されたタブの中身を表示するノード
   */
  private viewerNode: JQuery<HTMLElement>;
  /**
   * クリック時に切り替えられるタブの集合.
   */
  private tabArray: EmptyTab[];
  private tabCount: number;
  /**
   * タブの中身が詳細に画面左下に表示されているタブを格納しておく
   * Tabsの中で直近でクリックされたものが入っている.
   * Tabsの中から参照される.
   */
  public nowSelectedTab: undefined | EmptyTab;
  constructor(
    windowNode: JQuery<Window & typeof globalThis>,
    pickerNode: JQuery<HTMLElement>,
    viewerNode: JQuery<HTMLElement>,
    tabArray: EmptyTab[]
  ) {
    super();
    this.windowNode = windowNode;
    this.pickerNode = pickerNode;
    this.viewerNode = viewerNode;
    this.tabArray = tabArray;
    this.tabCount = 0;
    this.initializeClickableTabNameList();

    //#region イベントリスナーの登録
    // [NOTE] windowはブラウジングコンテキストであり, ウィンドウサイズを変えたりするとwindowからイベントが発火される
    this.windowNode.on('resize', () => {
      // [NOTE] viewerNodeで実際に表示されているものは, 画面サイズ変更の影響を受けるので更新を行う.
      this.nowSelectedTab?.emit('resize');
    });
    //#endregion
  }

  //[NOTE] これはtabs.js側から呼ばれます. clickイベントが発火された後で, そのタブの中のイベントリスナ内から呼ばれます
  public display(newlySelectedTab: EmptyTab): void {
    this.ChangeViewerNodeContentWith(newlySelectedTab);
    this.ChangeSelectedTabInto(newlySelectedTab);
  }

  //#region privateなメソッド群
  private initializeClickableTabNameList(): void {
    if (this.tabArray === undefined || this.tabArray.length <= 0) {
      return;
    }
    this.tabCount = this.tabArray.length;
    for (let i = 0; i < this.tabCount; i++) {
      this.createClickableTabNameListWith(this.tabArray[i]);
    }
    // [NOTE] はじめはとりあえずtabArrayの一番はじめのタブ(現在はSCWタブ)に対して表示をするように, クリックイベントを流しておく.
    this.tabArray[0].emit('click');
  }

  /**
   * 画面左側のタブ一覧の表示の初期化. ユーザーがクリックして表示されるタブを変更できるような状態にする責任を負う.
   */
  private createClickableTabNameListWith(tabObject: EmptyTab): void {
    tabObject.tabSwitcher = this;
    const tabNode = this.createHTMLElementOfClickableTabNameWith(tabObject);
    tabNode.appendTo(this.pickerNode);
    // [NOTE] そのタブの文字が表示されているノードをクリックしたら, バックエンド側のタブの実体にクリックされたことを通知するように設定.
    tabNode.on('click', function () {
      tabObject.emit('click');
    });
    tabObject.ClickableTabNameNode = tabNode;
  }

  private createHTMLElementOfClickableTabNameWith(
    tabObject: EmptyTab
  ): JQuery<HTMLElement> {
    // [NOTE] タブの個数が変化しても等間隔を維持できるようにしておく.
    const tabWidth = 100 / this.tabCount + '%';
    const tabNode = $('<div></div>');
    tabNode.attr('id', tabObject.name);
    tabNode.attr('class', 'hori-inner middle-text center-text tab');
    tabNode.html(tabObject.name);
    tabNode.css('width', tabWidth);
    return tabNode;
  }

  private ChangeViewerNodeContentWith(newlySelectedTab: EmptyTab): void {
    const tabContent = newlySelectedTab.getContent();
    this.viewerNode.children().detach(); //一つ前に表示されていたものを解除. html('')やempty()だと既存のイベントハンドラー達が消える
    if (tabContent) {
      this.viewerNode.append(tabContent);
    }
  }

  private ChangeSelectedTabInto(newlySelectedTab: EmptyTab): void {
    // [NOTE] 既にcurrentTabがあった場合には別のタブがクリックされたために, 表示されなくなることを通知し, 背景色を黒色に変える.
    this.nowSelectedTab?.emit('unclick');
    this.nowSelectedTab?.ClickableTabNameNode?.css({
      'background-color': '#000000',
    });

    this.nowSelectedTab = newlySelectedTab;
    // [NOTE] タブの文字が表示されているノードをクリックしたらその背景色を青色に変える.
    this.nowSelectedTab.ClickableTabNameNode?.css({
      'background-color': '#0000EE',
    });
  }
  //#endregion
}
