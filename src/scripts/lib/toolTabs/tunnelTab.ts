import { EmptyTab } from './tabBase';
import $ from 'jquery';
import { ReactiveProperty } from '../reactiveProperty';
import fs from 'fs';
import path from 'path';
import os from 'os';
import settings from 'electron-settings';

// [NOTE] 風洞試験用に使うタブ
type dataTableForTunnelTab = {
  Time: () => number;
  T: JQuery<HTMLElement>;
  P: JQuery<HTMLElement>;
  dH: JQuery<HTMLElement>;
  U: JQuery<HTMLElement>;
  i1: JQuery<HTMLElement>;
  i2: JQuery<HTMLElement>;
  k1: JQuery<HTMLElement>;
  k2: JQuery<HTMLElement>;
};
//#region TunnelTab
export class TunnelTab extends EmptyTab {
  data1: ReactiveProperty;
  data2: ReactiveProperty;
  enabled: boolean;
  descriptionNode1: JQuery<HTMLElement>;
  tmpTNode: JQuery<HTMLElement>;
  tmpPNode: JQuery<HTMLElement>;
  tmpMNode: JQuery<HTMLElement>;
  formulaNode: JQuery<HTMLElement>;
  rawData1: JQuery<HTMLElement>;
  rawData2: JQuery<HTMLElement>;
  temperatureNode: JQuery<HTMLElement>;
  pressureNode: JQuery<HTMLElement>;
  manometerNode: JQuery<HTMLElement>;
  speedometerNode: JQuery<HTMLElement>;
  speedData1: JQuery<HTMLElement>;
  speedData2: JQuery<HTMLElement>;
  paramK1Node: JQuery<HTMLElement>;
  paramK2Node: JQuery<HTMLElement>;
  paramK1: JQuery<HTMLElement>;
  paramK2: JQuery<HTMLElement>;
  aveParamK1Node: JQuery<HTMLElement>;
  aveParamK2Node: JQuery<HTMLElement>;
  aveParamK1: JQuery<HTMLElement>;
  aveParamK2: JQuery<HTMLElement>;
  plotButton: JQuery<HTMLElement>;
  delButton: JQuery<HTMLElement>;
  clearButton: JQuery<HTMLElement>;
  exportButton: JQuery<HTMLElement>;
  tableWrapper: JQuery<HTMLElement>;
  dataTableNode: JQuery<HTMLElement>;
  dataList: string[];
  dataLength: number;
  dataTable: dataTableForTunnelTab;
  constructor(name: string, data1: ReactiveProperty, data2: ReactiveProperty) {
    super(name);
    this.data1 = data1;
    this.data2 = data2;
    this.enabled = false;
    this.content = $(
      "<div class='tool-viewer-inner' style='overflow-y:auto'></div>"
    );
    this.descriptionNode1 = $(
      '<div>風洞試験用のタブです。通常は使いません。</div>'
    );
    this.tmpTNode = $(
      "<div>風温(℃) T:<input id='tunnelTemp' step='0.1' type='number' value='25'/>℃</div>"
    );
    this.tmpPNode = $(
      "<div>測定時の大気圧(Pa) P:<input id='tunnelPres' type='number' value='101325'/>Pa</div>"
    );
    this.tmpMNode = $(
      "<div>マノメータの読み(Pa) Δh: <input id='tunnelMano' step='0.01' type='number' value='0.01'/>Pa</div>"
    );
    this.formulaNode = $(
      "<div>風速 U=395.673 × √(Δh × (1 + 0.00366 × T) / P) = <input id='tunnelSpeed'  type='number' disabled/>m/s</div>"
    );
    this.rawData1 = $(
      "<div>機速計1:<input id='speedRawData1' type='number'/ value='0' disabled />回インタラプト/s</div>"
    );
    this.rawData2 = $(
      "<div>機速計2:<input id='speedRawData2' type='number'/ value='0' disabled />回インタラプト/s</div>"
    );

    this.temperatureNode = this.tmpTNode.find('#tunnelTemp');
    this.pressureNode = this.tmpPNode.find('#tunnelPres');
    this.manometerNode = this.tmpMNode.find('#tunnelMano');
    this.speedometerNode = this.formulaNode.find('#tunnelSpeed');
    this.speedData1 = this.rawData1.find('#speedRawData1');
    this.speedData2 = this.rawData2.find('#speedRawData2');

    this.paramK1Node = $(
      "<div>係数k'1: <input id='paramK1' type='number' disabled /></div>"
    );
    this.paramK2Node = $(
      "<div>係数k'2: <input id='paramK2' type='number' disabled /></div>"
    );
    this.paramK1 = this.paramK1Node.find('#paramK1');
    this.paramK2 = this.paramK2Node.find('#paramK2');

    this.aveParamK1Node = $(
      "<div>最小二乗法k1: <input id='aveParamK1' type='number' disabled /></div>"
    );
    this.aveParamK2Node = $(
      "<div>最小二乗法k2: <input id='aveParamK2' type='number' disabled /></div>"
    );
    this.aveParamK1 = this.aveParamK1Node.find('#aveParamK1');
    this.aveParamK2 = this.aveParamK2Node.find('#aveParamK2');

    this.plotButton = $('<button>Plot</button>');
    this.plotButton.on('click', () => {
      this.plot();
      this.updateAverage();
    });
    this.delButton = $('<button>Delete</button>');
    this.delButton.on('click', () => {
      this.remove();
      this.updateAverage();
    });
    this.clearButton = $('<button>Clear</button>');
    this.clearButton.on('click', () => {
      this.clear();
      this.updateAverage();
    });
    this.exportButton = $('<button>Export</button>');
    this.exportButton.on('click', () => {
      this.updateAverage();
      this.exportCSV();
    });

    this.tableWrapper = $('<div></div>');
    this.dataTableNode = $("<table id='tunnelTable'></table>");
    this.tableWrapper.append(this.dataTableNode);
    this.dataList = ['Time', 'dH', 'U', 'i1', 'i2', 'k1', 'k2', 'T', 'P'];
    this.dataLength = this.dataList.length;
    // [XXX] function以外でのthisの解消をしてしまったが動くか不明
    this.dataTable = {
      Time: Date.now,
      T: this.temperatureNode,
      P: this.pressureNode,
      dH: this.manometerNode,
      U: this.speedometerNode,
      i1: this.speedData1,
      i2: this.speedData2,
      k1: this.paramK1,
      k2: this.paramK2,
    };
    this.createTable();
    this.content.append(this.descriptionNode1);
    this.content.append('<br/>');
    this.content.append(this.tmpTNode);
    this.content.append(this.tmpPNode);
    this.content.append(this.tmpMNode);
    this.content.append(this.formulaNode);
    this.content.append(this.rawData1);
    this.content.append(this.rawData2);
    this.content.append(this.paramK1Node);
    this.content.append(this.paramK2Node);
    this.content.append(this.aveParamK1Node);
    this.content.append(this.aveParamK2Node);
    this.content.append(this.plotButton);
    this.content.append(this.delButton);
    this.content.append(this.clearButton);
    this.content.append(this.exportButton);
    this.content.append(this.tableWrapper);
    this.updateCalculation();
    this.setContent(this.content);
    this.data1.on('update', () => {
      this.speedData1.val(this.data1.getRawValue() as number);
      this.updateCalculation();
    });
    this.data2.on('update', () => {
      this.speedData2.val(this.data2.getRawValue() as number);
      this.updateCalculation();
    });
    this.temperatureNode.on('change keydown paste input', () => {
      this.updateCalculation();
    });
    this.pressureNode.on('change keydown paste input', () => {
      this.updateCalculation();
    });
    this.manometerNode.on('change keydown paste input', () => {
      this.updateCalculation();
    });
    this.speedometerNode.on('change input', () => {
      this.updateCalculation();
    });
  }

  updateCalculation(): void {
    const temperature: number = this.temperatureNode.val() as number;
    const pressure: number = this.pressureNode.val() as number;
    const dH: number = this.manometerNode.val() as number;
    let speed = 0;
    if (pressure === 0) {
      this.speedometerNode.val(speed);
    } else {
      speed =
        395.673 *
        Math.sqrt(((dH - 16.7) * (1 + 0.00366 * temperature)) / pressure);
      this.speedometerNode.val(speed);
    }
    const i1 = this.data1.getRawValue() as number;
    let k1 = 0;
    if (i1 !== 0) k1 = speed / i1;
    const i2 = this.data2.getRawValue() as number;
    let k2 = 0;
    if (i2 !== 0) k2 = speed / i2;
    this.paramK1.val(k1);
    this.paramK2.val(k2);
  }

  createTable(): void {
    const dataTableTopRow = $('<tr></tr>');
    for (let i = 0; i < this.dataLength; i++) {
      dataTableTopRow.append($('<th>' + this.dataList[i] + '</th>'));
    }
    this.dataTableNode.append(dataTableTopRow);
  }

  plot(): void {
    const plotRow = $('<tr></tr>');
    plotRow.append($('<th>' + new Date().toLocaleTimeString() + '</th>'));
    for (let i = 1; i < this.dataLength; i++) {
      // [NOTE] typescriptでは文字列リテラルを扱うには型を明示的に宣言しないといけない.
      const value = parseFloat(
        (this.dataTable[
          this.dataList[i] as keyof dataTableForTunnelTab
        ] as JQuery<HTMLElement>).val() as string
      );
      plotRow.append($('<th>' + value + '</th>'));
    }
    this.dataTableNode.append(plotRow);
  }

  remove(): void {
    if (this.dataTableNode.find('tr').length > 1) {
      this.dataTableNode.find('tr').last().remove();
    }
  }

  clear(): void {
    this.dataTableNode.empty();
    this.createTable();
  }

  exportCSV(): void {
    /**
     * @type {any}
     */
    const exportDir: string = settings.get(
      'log.dirName',
      path.join(os.homedir(), 'documents', 'GhostLogs')
    ) as string;
    if (!fs.existsSync(exportDir)) {
      fs.mkdirSync(exportDir);
    }
    let fileNumber = 1;
    let fileSafe = false;
    let fileName = 'tunnel1.csv';
    while (!fileSafe) {
      fileSafe = true;
      fileName = 'tunnel' + fileNumber + '.csv';
      fs.readdirSync(exportDir).forEach((file) => {
        if (path.basename(file).toString() === fileName) {
          fileNumber += 1;
          fileSafe = false;
        }
      });
    }
    const exportFile = path.join(exportDir, fileName);
    let writeStr = 'TunnelLog,,Average,';
    writeStr += this.aveParamK1.val() + ',';
    writeStr += this.aveParamK2.val() + '\n';
    this.dataTableNode.children().each((_i, obj1) => {
      $(obj1)
        .children()
        .each((j, obj2) => {
          writeStr += $(obj2).html();
          if (j === this.dataLength - 1) writeStr += '\n';
          else writeStr += ',';
        });
    });
    fs.writeFile(exportFile, writeStr, function (err) {
      if (err) throw err;
      console.log('tunnel data saved');
    });
  }

  updateAverage(): void {
    const uIndex = this.dataList.indexOf('U');
    const i1index = this.dataList.indexOf('i1');
    const i2index = this.dataList.indexOf('i2');
    const uList: number[] = [];
    const i1list: number[] = [];
    const i2list: number[] = [];
    this.dataTableNode.children().each(function (i, obj) {
      if (i > 0) {
        uList.push(parseFloat($($(obj).children()[uIndex]).html()));
        i1list.push(parseInt($($(obj).children()[i1index]).html()));
        i2list.push(parseInt($($(obj).children()[i2index]).html()));
      }
    });
    const aveParam1 = this.lsm(i1list, uList).a;
    const aveParam2 = this.lsm(i2list, uList).a;
    this.aveParamK1.val(aveParam1);
    this.aveParamK2.val(aveParam2);
  }

  // 最小二乗法を求めてくれるやつ
  lsm(xList: number[], yList: number[]): { a: number; b: number } {
    const n = xList.length;
    let sigmaX = 0;
    let sigmaY = 0;
    let sigmaXY = 0;
    let sigmaXX = 0;
    for (let i = 0; i < n; i++) {
      sigmaX += xList[i];
      sigmaY += yList[i];
      sigmaXY += xList[i] * yList[i];
      sigmaXX += xList[i] * xList[i];
    }
    let a = 0;
    let b = 0;
    const divb = n * sigmaXX - sigmaX * sigmaX;
    if (divb !== 0) {
      a = (n * sigmaXY - sigmaX * sigmaY) / divb;
      b = (sigmaXX * sigmaY - sigmaXY * sigmaX) / divb;
    }
    return { a: a, b: b };
  }
}
