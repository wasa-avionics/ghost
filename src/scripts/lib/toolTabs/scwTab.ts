import { EmptyTab } from './tabBase';
import $ from 'jquery';

/**
 * https://supercweather.com から雲の様子を引っ張ってくる用のタブ
 */
export class SCWTab extends EmptyTab {
  constructor(name: string) {
    super(name);
    this.setContent(
      $(
        "<webview id='scw' class='tool-viewer-inner' src='https://supercweather.com'></webview>"
      )
    );
  }
}
