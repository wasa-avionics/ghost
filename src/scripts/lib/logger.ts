import path from 'path';
import os from 'os';
import fs from 'fs';
const { dialog } = require('electron').remote;
import EventEmitter from 'events';
import settings from 'electron-settings';
import { MessageBoxReturnValue, OpenDialogReturnValue } from 'electron';
import {
  TimeObservable,
  AirSpeedReactiveProperty,
  ReactiveProperty,
  SerialComFreqObservable,
} from './reactiveProperty';
/*eslint-disable*/
const Tone = require('tone');
const logOn = require('../../static/log-on.png');
const logOff = require('../../static/log-off.png');
/*eslint-enable*/

//#region ログを取れる項目についての型の定義.
/**
 *setup.tsにて, 実際にログをとられる実体の設定とログを取る際の順番の指定ができます.
 */
export type LoggedData = {
  time: TimeObservable;
  serialCommunicationFreqObservable: SerialComFreqObservable;
  altitude: ReactiveProperty;
  airSpeed: AirSpeedReactiveProperty;
  groundSpeed: ReactiveProperty;
  cadence: ReactiveProperty;
  rudder: ReactiveProperty;
  elevator: ReactiveProperty;
  accelX: ReactiveProperty;
  accelY: ReactiveProperty;
  accelZ: ReactiveProperty;
  yaw: ReactiveProperty;
  pitch: ReactiveProperty;
  roll: ReactiveProperty;
  calSystem: ReactiveProperty;
  calAccel: ReactiveProperty;
  calGyro: ReactiveProperty;
  calMag: ReactiveProperty;
  longitude: ReactiveProperty;
  latitude: ReactiveProperty;
  satellites: ReactiveProperty;
  /**
   * Horizontal Dilution of Precisionの略. GPSにおける水平精度低下率.
     測位点はUERE(利用者等価測距誤差UERE(UserEquivalent Ranging Error))に水平精度低下率HDOPをかけたものの2
     倍を半径とする円内に95%が入ると言われているので, map.tsにて利用する際はこの計算を行っている.
     参考文献: https://www.journal.ieice.org/conts/kaishi_wadainokiji/199912/19991201.html
   */
  hdop: ReactiveProperty;
  /**
   * UEREはGPSモジュールからArduinoを介して送られてくる. (向こうではTinyGPSライブラリ(パーサ, GPSモジュールからはただのバイト列がガンガン送られてくるのでそれに意味づけ(c++での型に落とし込む)をして, 扱いやすくするやつ)
     を用いてGPSモジュールから頑張って拾ってきている)
   */
  longitudeError: ReactiveProperty;
  /**
  * UEREはGPSモジュールからArduinoを介して送られてくる. (向こうではTinyGPSライブラリ(パーサ, GPSモジュールからはただのバイト列がガンガン送られてくるのでそれに意味づけ(c++での型に落とし込む)をして, 扱いやすくするやつ)
   を用いてGPSモジュールから頑張って拾ってきている)
  */
  latitudeError: ReactiveProperty;
  gpsAltitude: ReactiveProperty;
  gpsCourse: ReactiveProperty;
  rudderTemp: ReactiveProperty;
  rudderLoad: ReactiveProperty;
  rudderVolt: ReactiveProperty;
  elevatorTemp: ReactiveProperty;
  elevatorLoad: ReactiveProperty;
  elevatorVolt: ReactiveProperty;
  temperature: ReactiveProperty;
  humidity: ReactiveProperty;
  airPressure: ReactiveProperty;
};

//#endregion
//#region 型定義
/**
 * fs.writeFileはディレクトリを再帰的に作成してくれないらしい?ので, writeFileする際にはまずディレクトリの存在確認を行い, 作成し, ファイルのパスに含まれるディレクトリが存在することを示すこの型にしてから行ってください.
 */
type dirExistPath = { path: string; checked: boolean };
//#endregion

/**
 * CSV形式のファイルを組み立てて選択されたパスにログを出力することを担当するモジュール.
 */
export class Logger extends EventEmitter {
  private data: LoggedData;
  // [TODO] playbackから見られているのでplaybackにもこのキーをコンストラクタで渡してやれば良いのでは?どうせconstだし.
  public logKeys: (keyof LoggedData)[];
  private iconNode: JQuery<HTMLElement>;
  private statusNode: JQuery<HTMLElement>;
  private toggleNode: JQuery<HTMLElement>;
  private fileNameNode: JQuery<HTMLElement>;
  private dirSelectNode: JQuery<HTMLElement>;
  /**
   * 現ログ出力先の表示
   */
  private dirNode: JQuery<HTMLElement>;
  private logDirectory: string;
  /**
   * ログを取る機能が有効化されているかどうか
   */
  private enabled: boolean;
  /**
   *ファイルに書き込み中かどうか. 排他制御用
   */
  private unlocked: boolean;
  /**
   * ログの保存先を選択している最中かどうか
   */
  private toggling: boolean;
  /**
   * input. ユーザーはここにファイル名を入力できる.
   */
  private fileName: string;
  /**
   * ログが記載されていると判別するためのヘッダ
   */
  static logHeader = 'ghostlog';
  constructor(
    data: LoggedData,
    logKeys: (keyof LoggedData)[],
    iconNode: JQuery<HTMLElement>,
    statusNode: JQuery<HTMLElement>,
    toggleNode: JQuery<HTMLElement>,
    fileNameNode: JQuery<HTMLElement>,
    dirSelectNode: JQuery<HTMLElement>,
    dirNode: JQuery<HTMLElement>
  ) {
    super();
    this.data = data;
    this.logKeys = logKeys;
    this.iconNode = iconNode; // トグルボタンの絵アイコン
    this.statusNode = statusNode; // ONかOFFかの文字を表示する
    this.toggleNode = toggleNode; // トグルボタン
    this.fileNameNode = fileNameNode; // ログを入れるファイル名
    this.dirSelectNode = dirSelectNode; // ログを入れるディレクトリを選択するためのボタン
    this.dirNode = dirNode; // ログを入れるディレクトリ名
    //[NOTE] デフォルトではログはドキュメントフォルダの中に入る.
    this.logDirectory = settings.get(
      'log.dirName',
      path.join(os.homedir(), 'documents', 'GhostLogs')
    ) as string;
    this.enabled = false;
    this.unlocked = true;
    this.toggling = false;
    this.fileName = '';
    //#region イベントリスナーの登録.
    this.dirSelectNode.on('click', () => {
      this.setLogDir();
    });
    this.toggleNode.on('click', () => {
      this.toggleLog();
    });
    this.on('open', () => {
      this.open();
    });
    this.on('close', () => {
      this.close();
    });
    this.on('data', () => {
      this.logData();
    });
    //#endregion
    this.dirNode.html(this.logDirectory + '/');
  }

  //#region イベントが来たら呼ばれるメソッド群
  private setLogDir(): void {
    // [NOTE] srcFileNameにはユーザーが選択したファイルのパスが配列に入れられて返る
    dialog
      .showOpenDialog({
        title: 'Select Log Directory',
        defaultPath: this.logDirectory,
        properties: [
          'openDirectory',
          'treatPackageAsDirectory',
          'createDirectory',
          'promptToCreate',
        ],
      })
      .then((srcFileName: OpenDialogReturnValue) => {
        // [NOTE] ユーザーが未選択のまま閉じた場合にundefinedが返るのでこのif文は必要.
        if (!srcFileName.filePaths) {
          return;
        }
        // [NOTE] 一番目に選択されたパスを選び, あとは無視
        const dirPath = srcFileName?.filePaths[0];
        this.logDirectory = dirPath;
        settings.set('log.dirName', this.logDirectory);
        // [NOTE] view側にも新たな保存先を反映する.
        this.dirNode.html(this.logDirectory + '/');
      });
  }

  /**
   * ログをどこに保存するか決定する. toggleとは一度押したら有効化されて, 二度目に押したら無効化されることを意味している.
   */
  private toggleLog(): void {
    if (this.toggling) {
      return;
    }
    this.toggling = true;
    // [NOTE] 既にログが有効化されていたら, 無効化するイベントを流す.
    if (this.enabled) {
      this.emit('close');
      return;
    }
    this.prepareOutputFileForLogging();
  }

  //#region 補助的な関数 ログを出力するファイルを作成し, ヘッダを安全に書き込む.
  /**
   * ログを吐き出すファイル名に入力がされているか, 既に存在するファイルかを確認し, ヘッダを書き込んでログ用のファイルを準備する.
   */
  private prepareOutputFileForLogging(): void {
    // [NOTE] 何もいれていない場合はファイルを作らない. (引数指定して, 前もって型ガードかなんかでこの判定は別の場所でやるべき)
    if (!this.fileNameNode.val()) {
      this.toggling = false;
      return;
    }
    this.fileName = this.createFullPathForCSV(
      this.logDirectory,
      this.fileNameNode.val()
    );
    // [NOTE] ログ用のファイルが既に存在するかどうかで処理を分け, 上書きするかどうかをユーザー側に知らせる.
    fs.promises.access(this.fileName).then(
      async () => {
        {
          const res: MessageBoxReturnValue = await this.confirmWeatherToIgnoreExistingFile(
            this.fileNameNode.val()
          );
          // [NOTE] 既に存在するファイルに上書きする場合にはヘッダを新しく書き込むことはしないので, もしlogKeyを変更することがあれば, ここも変えてください.
          // [NOTE] ついでに, logKeyを変えた場合には, playbackで再生できるように, 旧ログデータへの対応もしなければなりません.
          if (res.response === 0) this.emit('open');
          else this.toggling = false;
        }
      },
      () => {
        const safepath: dirExistPath = this.createDirectory(
          this.logDirectory,
          this.fileName
        );
        const header: string = this.createHeader(this.logKeys);
        this.writeHeaderSafely(safepath, header).then(
          () => {
            this.emit('open');
          },
          (reason: Error) => {
            console.log(reason);
            this.toggling = false;
          }
        );
      }
    );
  }

  /**
   * ディレクトリがあることが確認されたパスに, 与えられたヘッダを書き込みます.
   */
  private writeHeaderSafely(
    FullPathToFile: dirExistPath,
    header: string
  ): Promise<void> {
    if (FullPathToFile.checked === false) {
      return Promise.reject(
        new Error('ディレクトリが存在するか不明なパスです. ')
      );
    }
    return fs.promises.writeFile(FullPathToFile.path, header);
  }

  /**
   * 与えられたディレクトリパスとファイル名から完全なパスを作成し, 返します.
   */
  private createFullPathForCSV(
    logDirectory: string,
    baseName: string | number | string[] | undefined
  ): string {
    return path.join(logDirectory, baseName + '.csv');
  }

  /**
   *与えられたファイル名のファイルが既に存在していることを知らせるMessageBoxを表示し, ユーザーの選択結果を返します.
   */
  private confirmWeatherToIgnoreExistingFile(
    fileName: string | number | string[] | undefined
  ): Promise<MessageBoxReturnValue> {
    return dialog.showMessageBox({
      title: 'File Already Exists',
      type: 'warning',
      buttons: ['Ignore', 'Cancel'],
      message: 'File "' + fileName + '.csv" already exists',
      detail: 'Ignoring will append data to file',
    });
  }

  /**
   * 引数で指定されたディレクトリとフルパスで指定されたファイルのディレクトリ部分がどちらも存在するかを確認し, なければ作成し, ディレクトリが作成されていることが確認されたファイルパスを返します.
   */
  private createDirectory(
    logDirectory: string,
    FullPathToFile: string
  ): dirExistPath {
    // [NOTE] 引数で渡されたディレクトリ名, もしくはフルパスで渡されたファイルのディレクトリ部分が存在しない場合は作成. (片方だけでよくない...? だってファイル名はlogDirectryを用いて作られてるんだし.)
    if (!fs.existsSync(logDirectory)) {
      fs.mkdirSync(logDirectory);
    }
    if (!fs.existsSync(path.dirname(FullPathToFile))) {
      fs.mkdirSync(path.dirname(FullPathToFile));
    }
    return { path: FullPathToFile, checked: true } as dirExistPath;
  }

  /**
   * ログのファイルだと判別できるヘッダの書き込みと, どんなデータがあるのかをlogKeysから取ってきて, CSVの二段目に全て書き込む.
   */
  private createHeader(logKeys: (keyof LoggedData)[]): string {
    // [NOTE] ログデータであるということを示すためのcsvの第一行目につけるヘッダ. playback.jsで確認のために使う.
    // [NOTE} playbackでは改行でsplitして確認するので改行まで入れるないとだめ
    let initialStr = Logger.logHeader + '\n';
    // [NOTE] デバッグ用
    console.log(initialStr);
    // [NOTE] CSV用に, 最初にどんなデータがあるのか, 羅列する.
    for (let i = 0; i < logKeys.length - 1; i++) {
      initialStr += logKeys[i] + ',';
    }
    // [NOTE] 羅列後に改行を挟みたいので, 最後の一個のStringを入れた後で改行.
    initialStr += logKeys[logKeys.length - 1] + '\n';
    return initialStr;
  }
  //#endregion

  /**
   * [NOTE] ログを実際に取り始める.
   */
  private async open(): Promise<void> {
    const synth = new Tone.Synth().toMaster();
    // [NOTE] 1.5秒後にログ取りを開始する. ログ取りを開始する前に3回, ログ取開始と共に4回目の音を鳴らしてユーザーに知らせる
    synth.triggerAttackRelease('C4', '8n');
    await this.playLoggingStartSoundInFiveSeconds('C4', synth);
    await this.playLoggingStartSoundInFiveSeconds('C4', synth);
    await this.playLoggingStartSoundInFiveSeconds('C5', synth);
    console.log('openning logger');
    this.toggling = false;
    this.enabled = true;
    this.statusNode.html('Logging...');
    this.iconNode.attr('src', logOn);
    // [NOTE] ログを取り始める際の初回のemitはここで成され, 次回以降はSerialもしくはgenerator側からemitが発せられる.
    this.emit('data');
    return;
  }

  private playLoggingStartSoundInFiveSeconds(
    soundPitch: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    synth: any
  ): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(() => {
        synth.triggerAttackRelease(soundPitch, '8n');
        resolve();
      }, 500);
    });
  }

  private close(): void {
    console.log('closing logger');
    this.toggling = false;
    // [NOTE] enabledをfalseにすればログ取りは停止する.
    this.enabled = false;
    this.statusNode.html('Logging Disabled');
    this.iconNode.attr('src', logOff);
  }

  /**
   * 実際にログを書き出す
   */
  private async logData(): Promise<void> {
    // [NOTE] ログが有効化されていないならemitでイベントが来てもログを吐かない
    if (!this.enabled) {
      return;
    }
    // [NOTE] 排他制御
    if (!this.unlocked) {
      console.log('hit write lock');
      return;
    }
    this.unlocked = false;
    const writeStr: string = this.createCSVFrameForLog(this.data, this.logKeys);
    return fs.promises.appendFile(this.fileName, writeStr).then(() => {
      this.unlocked = true;
    });
  }

  private createCSVFrameForLog(
    data: LoggedData,
    logKeys: (keyof LoggedData)[]
  ): string {
    const dataCount = logKeys.length - 1;
    let writeStr = '';
    // [NOTE] CSVで書き出すためにカンマを一つ一つ打っていき, 最後に改行文字を入れる. CSVはカンマと改行でファイルを読み取るので.
    // [NOTE] ここで暗黙的にdataはlogKeysの順番に書き込まれるようにされている. playbackの方は工夫しないと, もし, logKeysを変更した時に, 過去のログデータを読み取れなくなってしまう.
    for (let i = 0; i < dataCount; i++) {
      writeStr += data[logKeys[i]].getValue() + ',';
    }
    writeStr += data[logKeys[dataCount]].getValue() + '\n';
    return writeStr;
  }
  //#endregion
}
