import $ from 'jquery';
// [NOTE] serial通信のためのライブラリ
import SerialPort, { PortInfo, OpenOptions } from 'serialport';
// [NOTE] XBeeのAPIフレームへのデータの変換, APIフレームからデータをオブジェクトに変換するやつ.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { XBeeAPI } = require('xbee-api');
const C = XBeeAPI.constants;
// const serialUtil = SerialPort.SerialPort
import EventEmitter from 'events';
import { GraphTab } from '../toolTabs/graphTab/graphTab';
import { updateEventEmitterToGraphics } from '../graphics';
import { Logger, LoggedData } from '../logger';

/*eslint-disable*/
const connectOn = require('../../../static/connect-icon.png');
const connectOff = require('../../../static/disconnect-icon.png');
/*eslint-enable*/

// [NOTE] ポートの接続, 切り替え, 解除を担当. かつ, 送られてくるデータをこちらで扱えるように開封するモジュール.
export class Serial extends EventEmitter {
  private graphicsManager: updateEventEmitterToGraphics;
  private logger: Logger;
  private data: LoggedData;
  /**
   * selectボタンであり, このノードにappendすることで選択できるものが増える. listNode自体
   */
  private listNode: JQuery<HTMLElement>;
  private refreshNode: JQuery<HTMLElement>;
  private iconNode: JQuery<HTMLElement>;
  private statusNode: JQuery<HTMLElement>;

  /**
   * このノードはボタンではなく, ただ画像があるだけだが, このノードをクリックすることで, 実際にポート接続が始まるようにしたい.
   */
  private toggleNode: JQuery<HTMLElement>;
  //[NOTE] XbeeAPI型は@typesが存在しないのでany型
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private xbee: any;
  private connection: SerialPort | undefined;
  private toggleEnable: boolean;
  constructor(
    graphicsManager: updateEventEmitterToGraphics,
    logger: Logger,
    data: LoggedData,
    listNode: JQuery<HTMLElement>,
    refreshNode: JQuery<HTMLElement>,
    iconNode: JQuery<HTMLElement>,
    statusNode: JQuery<HTMLElement>,
    toggleNode: JQuery<HTMLElement>
  ) {
    super();
    this.graphicsManager = graphicsManager;
    this.logger = logger;
    this.data = data;
    this.listNode = listNode;
    this.refreshNode = refreshNode;
    this.iconNode = iconNode;
    this.statusNode = statusNode;
    this.toggleNode = toggleNode;
    this.xbee = undefined;
    this.connection = undefined;
    this.toggleEnable = true;
    this.listPorts();
    //#region イベントリスナーの登録
    this.refreshNode.on('click', () => {
      this.listPorts();
    });
    this.toggleNode.on('click', () => {
      this.toggleConnection();
    });
    //#endregion
  }
  //#region イベントが来たら呼ばれるメソッド群

  private listPorts(): void {
    this.listNode.empty();
    // [XXX] comNameというフィールドはportInfoには存在しないので書き直した
    SerialPort.list().then((ports: PortInfo[]) => {
      ports.forEach((port): void => {
        const comId = this.excludeUnrelatedStringInMac(port.path);
        this.listNode.append(
          this.createSerialPortHTMLElementWith(comId, port.path)
        );
      });
    }),
      (): void => {
        this.listNode.append($("<option value='null'>ERROR</option"));
      };
  }

  private toggleConnection(): void {
    if (this.listNode.val() === null || this.listNode.val() === 'null') return;

    if (this.toggleEnable) {
      this.statusNode.html('Serial Loading...');
      this.toggleEnable = false;
      setTimeout(() => {
        this.toggleEnable = true;
      }, 1000);

      if (this.connection) this.closePort();
      else this.openPort();
    }
  }
  //#region 補助的なメソッド群. イベントが来たら呼ばれるメソッド群について.
  private excludeUnrelatedStringInMac(path: string): string {
    /*
      OSX example portInfo
      {
        comName: '/dev/tty.usbmodem1421',
          manufacturer: 'Arduino (www.arduino.cc)',
            serialNumber: '752303138333518011C1',
              pnpId: undefined,
                locationId: '14500000',
                  productId: '0043',
                    vendorId: '2341'
      }

      Linux example portInfo
      {
        comName: '/dev/ttyACM0',
          manufacturer: 'Arduino (www.arduino.cc)',
            serialNumber: '752303138333518011C1',
              pnpId: 'usb-Arduino__www.arduino.cc__0043_752303138333518011C1-if00',
                locationId: undefined,
                  productId: '0043',
                    vendorId: '2341'
      }

      Windows example portInfo
      {
        comName: 'COM3',
          manufacturer: 'Arduino LLC (www.arduino.cc)',
            serialNumber: '752303138333518011C1',
              pnpId: 'USB\\VID_2341&PID_0043\\752303138333518011C1',
                locationId: 'Port_#0003.Hub_#0001',
                  productId: '0043',
                    vendorId: '2341'
      }
      */
    // [NOTE] 上を見れば分かるように, windowsではCOMで表示されるが, Macでは頭にdevがつくのでそれをここで外す.
    const basename = path.split('/');
    return basename[basename.length - 1];
  }

  private createSerialPortHTMLElementWith(
    comId: string,
    path: string
  ): JQuery<HTMLElement> {
    return $(
      "<option id='" + comId + "'value='" + path + "'>" + path + '</option>'
    );
  }
  //#endregion
  //#endregion
  //#region ポートの接続, 切断, 再接続を担当するメソッド群

  // [NOTE] ポートの設定とXBeeAPIの設定を行う
  private openPort(): void {
    // [NOTE] シリアルポートを一覧にして表すlistNodeはselectタグがついているので, listNodeをクリックして選択したらlistNodeに入っているシリアルポートの値は一つだけ
    const portName = this.listNode.val() as string;
    console.log('attempt connection to', portName);

    this.xbee = this.createXbeeAPI();
    this.connection = this.createSerialPortWith(portName);
    // [NOTE] ポートから流れてきたデータはXbeeのparserに流すようにする. parserは流れてくるコマンド列をいい感じのオブジェクトにしてくれる.
    //        parserを監視することで, データが流れて着次第処理できるようになる.
    this.connection.pipe(this.xbee.parser);
    // [NOTE] xbee.builderから流す先は, 接続しているポートにする.
    //        builderをいったん介すことで, オブジェクトをいい感じのコマンド列にしてくれる
    this.xbee.builder.pipe(this.connection);

    this.connection.open((err) => this.openingPortCallback(err));
  }

  private openingPortCallback(err: Error | null | undefined): void {
    if (err) {
      this.displayOpeningError(err);
      return;
    }
    this.displayOpeningSuccess();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this.xbee.parser.on('data', (frame: any): void => {
      this.unpackData(frame);
    });
    this.xbee.on('error', (): void => {
      this.retryPort();
    });
    // [XXX] 向こうにこちらが生きていることを示すための送り返す用?
    const testFrameObj = {
      type: 0x10, // xbee_api.constants.FRAME_TYPE.ZIGBEE_TRANSMIT_REQUEST
      destination64: '0000000000000000',
      // [XXX] これは何.
      data: [0xfe, 0xef], // Can either be string or byte array.
    };
    this.xbee.builder.write(testFrameObj);
  }

  private closePort(): void {
    this.connection?.close((): void => {
      // [NOTE] connectionが閉じられた際に行う処理.
      this.closeConnectionWithNotifyingIconNode();
      this.statusNode.html('Serial Disconnected');
      // [NOTE] xbee側に引っ付いているイベントリスナーをすべて取り外す.
      this.xbee.parser.removeAllListeners();
      this.xbee.removeAllListeners();
    });
  }

  private retryPort(): void {
    console.log('connection retrying');
    if (!this.connection) {
      return;
    }
    this.toggleEnable = false;
    this.connection.close((): void => {
      this.closeConnectionWithNotifyingIconNode();
      this.openPort();
      setTimeout((): void => {
        this.toggleEnable = true;
      }, 1000);
    });
  }
  //#region 補助的なメソッド群. ポートの接続, 切断, 再接続を担当するメソッド群について
  private closeConnectionWithNotifyingIconNode(): void {
    console.log('connection closed');
    this.connection = undefined;
    this.iconNode.attr('src', connectOff);
  }

  private displayOpeningError(err: Error): void {
    console.log('connection failed', err);
    this.iconNode.attr('src', connectOff);
    this.statusNode.html('Serial Disconnected');
  }

  private displayOpeningSuccess(): void {
    console.log('connection opened');
    this.iconNode.attr('src', connectOn);
    this.statusNode.html('Serial Connected');
  }

  // [TODO] もしかしたらinterfaceでoptionを実装してpartialで渡せるかもしれないので修正する.
  // serialPortのリファレンス https://serialport.io/docs/api-stream
  private createSerialPortWith(portName: string): SerialPort {
    return new SerialPort(portName, {
      // [NOTE] autoOpenはfalseにしておかないとcallbackが呼ばれない.
      autoOpen: false,
      baudRate: 57600,
      dataBits: 8,
      stopBits: 1,
      flowControl: false,
    } as OpenOptions);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private createXbeeAPI(): any {
    // [NOTE] xneeAPIのリファレンス https: //www.npmjs.com/package/xbee-api
    /*eslint-disable*/
    return new XBeeAPI.XBeeAPI({
      api_mode: 2, //Xbeeのエスケープ処理が入るモード パケットの中身に変なコマンドが入らないようにしてくれる.
      parser_buffer_size: 512,
      builder_buffer_size: 512,
      /*eslint-enable*/
    });
  }
  //#endregion
  //#endregion
  //#region シリアル通信で送られてきたデータの開封を担当するメソッド群

  /**
   * これはArduinoで作成しているパケットと対応しているので, 変更の際はArudionoも併せて変更してください.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private unpackData(frame: any): void {
    // console.log('got frame type: ', frame.type)
    // var ft = exports.FRAME_TYPE = {}; ft[0x90] = "ZigBee Receive Packet (AO=0) (0x90)"とのこと. ZigBee規格では, ヘッダ込みで127byte, データのみなら90byteほど1パケットに含まれる.
    if (frame.type === 0x90) {
      const buffer = frame.data;
      //#region パケットの開封作業
      const cadence = this.unpackUnsigned32(buffer, 1);
      const airSpeed = this.unpackUnsigned32(buffer, 5);
      let heading = this.unpackSigned16(buffer, 9);
      const roll = this.unpackSigned16(buffer, 11);
      const pitch = this.unpackSigned16(buffer, 13);
      const accelX = this.unpackSigned16(buffer, 15);
      const accelY = this.unpackSigned16(buffer, 17);
      const accelZ = this.unpackSigned16(buffer, 19);
      // [XXX] 恐らく, 180度を0度として基準にしたいため?
      heading += 180;
      // [NOTE] 1byteに2bitづつで, 四つの意味あるデータが入っているので&を取って拾う.
      const calibAccelerometer = buffer[21] & 0x03; //       0x03は0b00000011
      const calibMagnetometer = (buffer[21] & 0x0c) >> 2; // 0x0cは0b00001100
      const calibGyrosensor = (buffer[21] & 0x30) >> 4; //   0x30は0b00110000
      const calibSystem = (buffer[21] & 0xc0) >> 6; //       0xc0は0b11000000
      const altitude = this.unpackUnsigned16(buffer, 22);
      // if (altitude > 10) altitude = 10
      // else if (altitude < 0) altitude = 0
      const longitude = this.unpackUnsigned32(buffer, 24);
      const latitude = this.unpackUnsigned32(buffer, 28);
      const groundSpeed = this.unpackSigned16(buffer, 32);
      const gpsAltitude = this.unpackUnsigned32(buffer, 34);
      const satellites = buffer[38];
      const hdop = this.unpackUnsigned32(buffer, 39);
      const gpsCourse = this.unpackUnsigned16(buffer, 43);
      const longitudeError = this.unpackUnsigned16(buffer, 73);
      const latitudeError = this.unpackUnsigned16(buffer, 75);
      const temperature = this.unpackSigned16(buffer, 45);
      const airPressure = this.unpackUnsigned32(buffer, 47);
      const humidity = this.unpackUnsigned16(buffer, 51);
      const rudderPos = this.unpackSigned16(buffer, 55);
      const rudderLoad = this.unpackUnsigned16(buffer, 57);
      const rudderTemp = this.unpackUnsigned16(buffer, 59);
      const rudderVolt = this.unpackUnsigned16(buffer, 61);
      const elevatorPos = this.unpackSigned16(buffer, 65);
      const elevatorLoad = this.unpackUnsigned16(buffer, 67);
      const elevatorTemp = this.unpackUnsigned16(buffer, 69);
      const elevatorVolt = this.unpackUnsigned16(buffer, 71);
      //#endregion
      // [NOTE] 時間はここでも入れており, ログのほうは, 下のほうでemitしてからsetup.tsのdataの中の時間を拾いに行くのでマイクロ秒単位では, ずれるかも.
      const timeNow = Date.now();
      //#region 各dataへの値のセット. セット時に各データの内部でupdateイベントが飛ぶのでここで改めて行う必要はない.
      this.data.cadence.setValue(cadence);
      this.data.rudder.setValue(rudderPos);
      this.data.elevator.setValue(elevatorPos);
      this.data.altitude.setValue(altitude);
      this.data.airSpeed.setValue(airSpeed);
      this.data.groundSpeed.setValue(groundSpeed);
      this.data.accelX.setValue(accelX);
      this.data.accelY.setValue(accelY);
      this.data.accelZ.setValue(accelZ);
      this.data.yaw.setValue(heading);
      this.data.pitch.setValue(pitch);
      this.data.roll.setValue(roll);
      this.data.calSystem.setValue(calibSystem);
      this.data.calAccel.setValue(calibAccelerometer);
      this.data.calGyro.setValue(calibGyrosensor);
      this.data.calMag.setValue(calibMagnetometer);
      this.data.latitude.setValue(latitude);
      this.data.longitude.setValue(longitude);
      this.data.satellites.setValue(satellites);
      this.data.hdop.setValue(hdop);
      this.data.gpsAltitude.setValue(gpsAltitude);
      this.data.gpsCourse.setValue(gpsCourse);
      this.data.longitudeError.setValue(longitudeError);
      this.data.latitudeError.setValue(latitudeError);
      this.data.rudderLoad.setValue(rudderLoad);
      this.data.rudderTemp.setValue(rudderTemp);
      this.data.rudderVolt.setValue(rudderVolt);
      this.data.elevatorLoad.setValue(elevatorLoad);
      this.data.elevatorTemp.setValue(elevatorTemp);
      this.data.elevatorVolt.setValue(elevatorVolt);
      this.data.temperature.setValue(temperature);
      this.data.airPressure.setValue(airPressure);
      this.data.humidity.setValue(humidity);
      //#endregion
      this.data.serialCommunicationFreqObservable.increment();

      // [NOTE] 新しいデータが来たので, グラフタブ, ロガー, 表示計に通知.
      for (let i = 0; i < GraphTab.tabs.length; i++) {
        GraphTab.tabs[i].emit('update', timeNow);
      }
      this.logger.emit('data');
      this.graphicsManager.emit('update');

      // テストパケットの受信
    } else if (frame.type === C.FRAME_TYPE.AT_COMMAND_RESPONSE) {
      console.log('Node identifier:', frame);
    }
  }
  //#region 補助的なメソッド群. ポートの接続, 切断, 再接続を担当するメソッド群について

  /**
   * 符号"無し"32bit長の開封. ビッグエンディアンで開封.
   */
  private unpackUnsigned32(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    buffer: any,
    startIndex: number
  ): number {
    return (
      buffer[startIndex] |
      (buffer[startIndex + 1] << 8) |
      (buffer[startIndex + 2] << 16) |
      (buffer[startIndex + 3] << 24)
    );
  }

  /**
   * 符号"無し"16bit長の開封. ビッグエンディアンで開封.
   */
  private unpackUnsigned16(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    buffer: any,
    startIndex: number
  ): number {
    const value = buffer[startIndex] | (buffer[startIndex + 1] << 8);
    return value;
  }

  /**
   * 符号"有り"16bit長の開封. ビッグエンディアンで開封.
   */
  private unpackSigned16(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    buffer: any,
    startIndex: number
  ): number {
    let value = this.unpackUnsigned16(buffer, startIndex);
    // [NOTE] 2byte signed から 4byte signedへの変換については, 補数表示になっている時のみの話なので(つまり, 負の数があり得るところのみ. 0x8000との&をとっているところで2byte時の最上位bit, 符号bitが立っているかどうかを確認 )
    // 17bit目から32bit目までを1で埋めてやればよい.(これは, Arduino側でint16_tとなっており, そもそも17~32bit目はデータが入っていないため.) で, これを行うために17bit目に1がある0x10000を引いている.
    // そもそも2byteのままでいいじゃないかと思うかもしれないが, JavascriptにもTSにも4byteの数値型しかないのです.
    if (value & 0x8000) {
      value -= 0x10000; // 2byte signed から 4byte signed に変換
    }
    return value;
  }

  //#endregion
  //#endregion
}
