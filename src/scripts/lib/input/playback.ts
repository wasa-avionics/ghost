import fs from 'fs';
import path from 'path';
// [NOTE] remoteとは, electronのメインプロセス(package.jsonのmainで指定したところ)でしか使えないものをレンダラープロセスでも使えるようにする機能.
// [NOTE] やりすぎると, どこがメインプロセスになるのかわからなくなるので注意.
const { dialog } = require('electron').remote;
import EventEmitter from 'events';
import { updateEventEmitterToGraphics } from '../graphics';
import { Logger, LoggedData } from '../logger';
import { ReactiveProperty } from '../reactiveProperty';
import { OpenDialogReturnValue } from 'electron';
/*eslint-disable*/
const playbackOn = require('../../../static/playback-on.png');
const playbackOff = require('../../../static/playback-off.png');
/*eslint-enable*/

// [WARNING] LoggedData型にある文字列に依存して, 旧ログとの整合性がとれないかもしれないと思われるかもしれないが, インデックスで配列を探査しているので,
//           setup.tsのlogKeysの配列の順番を入れ替えずに名前だけ変えたりしても大丈夫.(ロガーに出力される名前が変わる.)
//           た だ し, logkeysの順番を入れ替えたり, LoggedData型を変更したりするととたんに後方互換性が失われるので注意!!!!!!!!!!
//           [TODO] 後方互換性を維持するための構造を考える. 具体的には, インデックスで回すのではなくて, 専用のobjectを用意して, 文字列検索を掛けて現在のloggedDataとの整合性を図るとか. ただしその際はloggedDataに対しては消去が許されなくなるが.
//           [TODO] 依存が激しいので修正する.. thisは基本的に禁止にしてイミュータブル性を確保する方向性で. 初めて読む際のオーバーヘッドを小さく.
//           [TODO] Promiseの使い方が微妙なので修正すべき. Promiseひとつひとつの粒度はもっとちいさくすべき. ただし, then()の中身ではthisは使えないのでawait式で結果を引っ張ってきて外で扱うこと.
/**
 *  ログデータから状況を復元するモジュール.
 */
export class Playback extends EventEmitter {
  private graphicsManager: updateEventEmitterToGraphics;
  private data: LoggedData;
  private logger: Logger;
  private iconNode: JQuery<HTMLElement>;
  private statusNode: JQuery<HTMLElement>;
  private toggleNode: JQuery<HTMLElement>;
  private enabled: boolean;
  /**
   * ロガーから出力されたデータが「,」区切りでstringで入っているcsvファイルが,
   * ひとまず改行区切りで配列になって入る場所
   */
  private input: string[];
  private index: number;
  /**
   *  一行一行タイミング込みでplayBackするために, CSVファイルの時間の項を拾ってきて,読み取って行く際に, いれる場所.
   */
  private currentFrameTime: number;
  private playbackLength: number;
  /**
   * inputに改行区切りで分割されたデータがさらにcsvのスタイルに従って,
   * 「,」区切りでパースされて配列になって入る場所
   */
  private dataPacket: string[];
  private dataLength: number;
  // [XXX] 何に使う予定だったか不明
  //private lastTime: number;
  constructor(
    graphicsManager: updateEventEmitterToGraphics,
    data: LoggedData,
    logger: Logger,
    iconNode: JQuery<HTMLElement>,
    statusNode: JQuery<HTMLElement>,
    toggleNode: JQuery<HTMLElement>
  ) {
    super();
    this.graphicsManager = graphicsManager;
    this.data = data;
    this.logger = logger;
    this.iconNode = iconNode;
    this.statusNode = statusNode;
    this.toggleNode = toggleNode;
    this.enabled = false;
    this.input = [];
    this.index = 0;
    this.currentFrameTime = 0;
    this.playbackLength = 0;
    this.dataPacket = [];
    this.dataLength = this.logger.logKeys.length;
    //this.lastTime = 0;

    this.toggleNode.on('click', () => {
      this.toggle();
    });
  }

  /**
   * 一度選択したら有効化して, もう一度押したら無効化する.
   */
  private toggle(): void {
    // [NOTE] 一度押したら再生, 二度目に押したら中止ということをしたいため. enabledを見てから各dataにsetValueしているのでここで中止にしても, 裏でplaybackし続けるということはない.
    this.enabled = !this.enabled;
    if (!this.enabled) {
      this.displayPlaybackEnable();
      return;
    }
    this.selectPlaybackFile();
  }

  /**
   * プレイバックを再生するログファイルを選択させ, 一番最初に選んだファイルを返す.
   */
  private async selectPlaybackFile(): Promise<void> {
    const dialogResult: OpenDialogReturnValue = await this.OpenDialogForUserSelectingFile();

    if (dialogResult.canceled) {
      return Promise.reject(new Error('ダイアログがキャンセルされました.'));
    }

    if (!dialogResult.filePaths) {
      return Promise.reject(
        new Error('ダイアログでファイルが選択されませんでした.')
      );
    }

    return this.accessToReadFile(dialogResult.filePaths[0]);
  }

  private async accessToReadFile(filePath: string): Promise<void> {
    const data = await fs.promises
      .access(filePath)
      .then(
        () => {
          return fs.promises.readFile(filePath, {
            encoding: 'utf8' as BufferEncoding,
          });
        },
        () => {
          this.displayLoadError(path.basename(filePath.toString()));
        }
      )
      .catch(() => {
        this.displayLoadError(path.basename(filePath.toString()));
      });
    if (!data) {
      return;
    }
    this.parseCSVData(data);
    return;
  }

  private parseCSVData(data: string): void {
    // [NOTE] まずCSVを一行一行切り出す.
    this.input = data.split('\n');
    this.playbackLength = this.input.length;
    // [NOTE] ヘッダーを見てログデータであるかの確認
    if (this.input[0] === Logger.logHeader) {
      this.index = 1;
      // [NOTE] 次にCSVを一データごとに切り出す.
      this.dataPacket = this.input[this.index].split(',');
      this.currentFrameTime = parseInt(
        this.dataPacket[this.logger.logKeys.indexOf('time')]
      );
      this.displayPlaybackEnable();
      this.displayFrames();
    } else {
      this.displayWrongType();
    }
  }

  private displayFrames(): void {
    if (!this.enabled) {
      this.displayPlaybackDisable();
      return;
    }
    if (this.dataPacket.length !== this.dataLength) {
      // [NOTE] ログデータの種類の数が一致しない
      if (this.dataPacket.length !== 1 || this.dataPacket[0] !== '') {
        this.displayDataError((this.index + 1).toString()); // エラーの場所を特定
      }
      this.enabled = false;
      this.displayPlaybackDisable();
      return;
    }
    this.setCSVLoggedValue();
    this.playbackNextCSVLine();
  }

  //#region  補助的なメソッド
  private setCSVLoggedValue(): void {
    // [NOTE] ログデータに書き込む順序も含めて決定しているので, このようにindexを回せる.
    for (let i = 0; i < this.dataLength; i++) {
      const key: keyof LoggedData = this.logger.logKeys[i];
      const data = this.data[key] as ReactiveProperty;
      // [NOTE] setup.tsにある各データにログデータから得られたデータを単位付きで書き込む.
      data.setValue(
        parseFloat(this.dataPacket[i]) / data.intToFloatMultiplierForUnit
      );
    }
    // [NOTE] 書き込んだので表示にupdateイベントを発火
    this.graphicsManager.emit('update');
    console.log(this.index);
  }

  private playbackNextCSVLine(): void {
    // [NOTE] ログが最後まで再生されたかどうか
    if (this.index >= this.playbackLength) {
      return;
    }
    this.index++;
    this.dataPacket = this.input[this.index].split(',');
    console.log(this.dataPacket);
    const oldFrameTime = this.currentFrameTime;
    this.currentFrameTime = parseInt(
      this.dataPacket[this.logger.logKeys.indexOf('time')]
    );
    // [NOTE] FrameRateまで再現するための計算
    setTimeout(() => {
      this.displayFrames();
    }, this.currentFrameTime - oldFrameTime);
  }

  private OpenDialogForUserSelectingFile(): Promise<OpenDialogReturnValue> {
    return dialog.showOpenDialog({
      title: 'Select Log File',
      properties: ['openFile', 'treatPackageAsDirectory'],
      filters: [{ name: 'csv', extensions: ['csv'] }],
    } as Electron.OpenDialogOptions);
  }

  private displayPlaybackEnable(): void {
    this.iconNode.attr('src', playbackOn);
    this.statusNode.html('Playback Enabled');
  }

  private displayPlaybackDisable(): void {
    this.iconNode.attr('src', playbackOff);
    this.statusNode.html('Playback Disabled');
  }
  //#endregion
  //#region エラー表示用ダイアログの表示を行う関数群
  private displayLoadError(filename: string): void {
    dialog.showMessageBox({
      title: 'File Invalid',
      type: 'error',
      buttons: ['OK'],
      message: 'File Invalid',
      detail: 'file ' + filename + ' is invalid',
    });
  }

  private displayWrongType(): void {
    dialog.showMessageBox({
      title: 'File Invalid',
      type: 'error',
      buttons: ['OK'],
      message: 'File Invalid',
      detail: 'Invalid ghost log',
    });
  }

  private displayDataError(line: string): void {
    dialog.showMessageBox({
      title: 'Data Invalid',
      type: 'error',
      buttons: ['OK'],
      message: 'Data Invalid',
      detail: line + ': Data is invalid',
    });
  }
  //#endregion
}
