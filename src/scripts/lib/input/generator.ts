import EventEmitter from 'events';
const MenuItem = require('electron').remote.MenuItem;
import { updateEventEmitterToGraphics } from '../graphics';
import { Logger, LoggedData } from '../logger';
import { GhostMenu } from '../menu';
// [NOTE] electronの, ユーザー側で使いたい設定を保存しておけるモジュール
import settings from 'electron-settings';
import { GraphTab } from '../toolTabs/graphTab/graphTab';
/*eslint-disable*/
const debugOn = require('../../../static/debug-on.png');
const debugOff = require('../../../static/debug-off.png');
/*eslint-enable*/

/**
 * その名の通り, 各データの中身を生み出す. RandomUpdateメソッドを呼び出し, 適当な値を生成してくれるようにする.
 */
export class DataGenerator extends EventEmitter {
  private graphicsManager: updateEventEmitterToGraphics;
  private logger: Logger;
  /**
   * setup.ts内部で宣言されているdataが入ります.
   */
  private data: LoggedData;
  private menu: GhostMenu;
  private interval: NodeJS.Timeout | undefined;
  private iconNode: JQuery<HTMLElement>;
  private statusNode: JQuery<HTMLElement>;
  private toggleNode: JQuery<HTMLElement>;
  private generatorEnabled: boolean;

  constructor(
    graphicsManager: updateEventEmitterToGraphics,
    logger: Logger,
    data: LoggedData,
    menu: GhostMenu,
    iconNode: JQuery,
    statusNode: JQuery,
    toggleNode: JQuery
  ) {
    super();
    this.graphicsManager = graphicsManager;
    this.logger = logger;
    this.data = data;
    this.menu = menu;
    this.interval = undefined;
    this.iconNode = iconNode;
    this.statusNode = statusNode;
    this.toggleNode = toggleNode;
    this.generatorEnabled = settings.get('generator.enabled') as boolean;
    // [NOTE] このtoggleイベントを発火することで, generatorEnabledのtrue/falseを入れ替える.
    this.toggleNode.on('click', () => {
      this.emit('toggle');
    });
    this.on('toggle', () => {
      this.toggleStatus();
    });
    this.setupMenu();
    this.setStatus();
  }

  /**
   * loggerとgraphicManagerに更新のイベントを発火する
   */
  private randomUpdate(): void {
    // [NOTE] this.dataオブジェクトのキー全てについて, setRandomを実行.
    //        mapの方が高速. mapは元の配列に対して変更を行わず, 新しい配列を作成する. ここで配列を得たいとかではなくて単に高速なために使っているぽい. keysさえ全部とってこれればいいので(この点ではforeachでもmapでもいい).
    (Object.keys(this.data) as (keyof LoggedData)[]).map((key): void => {
      this.data[key].setRandom();
    });
    const timeNow = Date.now();
    for (let i = 0; i < GraphTab.tabs.length; i++) {
      GraphTab.tabs[i].emit('update', timeNow);
    }
    // [NOTE] このloggerへのdataイベントの発火により, 各種データを記録してくれるようにする.
    this.logger.emit('data');
    this.graphicsManager.emit('update');
  }

  /**
   * クリック一度で機能をONにして, 二度目で機能をOFFにする
   */
  private toggleStatus(): void {
    this.generatorEnabled = !this.generatorEnabled;
    settings.set('generator.enabled', this.generatorEnabled);
    this.setStatus();
  }

  /**
   * electronにアプリケーションメニューを追加する.
   */
  private setupMenu(): void {
    this.menu.subMenuOfEdit.insert(
      2,
      new MenuItem({
        label: 'Toggle Data Generator',
        click: (): void => {
          this.toggleStatus();
        },
        type: 'checkbox',
        checked: this.generatorEnabled,
      })
    );
  }
  // [NOTE] このチェックボックスメニューがONになっているときに, 今は, 0.2秒ごとに記録をとるように
  //        randomUpdate関数内のイベント発火によってロガーに記録してもらうようにする.
  private setStatus(): void {
    if (this.generatorEnabled) {
      this.interval = setInterval(() => {
        this.randomUpdate();
      }, 200);
      this.displayGeneratorEnable();
    } else {
      if (this.interval) {
        clearInterval(this.interval);
      }
      this.displayGeneratorDisable();
    }
    this.menu.generatorLabel = this.generatorEnabled;
  }
  //#region 補助的なメソッド
  private displayGeneratorEnable(): void {
    console.log('data generator ON');
    this.iconNode.attr('src', debugOn);
    this.statusNode.html('Data Generator Enabled');
  }

  private displayGeneratorDisable(): void {
    console.log('data generator OFF');
    this.iconNode.attr('src', debugOff);
    this.statusNode.html('Data Generator Disabled');
  }
  //#endregion
}
