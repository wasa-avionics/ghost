import $ from 'jquery';
import EventEmitter from 'events';

/*eslint-disable*/
// @ts-ignore webpack側に伝えて, 出力してもらうために必要 
const atumori = require('../../static/secret.mp3')
const secretPng = require('../../static/secret.png')
/*eslint-enable*/

// [NOTE] これはキーボード↑↑↓↓←→←→AB エネミーコントローラー!!!!
const secretKeys = [38, 38, 40, 40, 37, 39, 37, 39, 65, 66];
/**
 * お遊び用. あつもりが表示される. 多分ブラウザのイベント系統の動作確認用に使う
 */
export class Secret extends EventEmitter {
  private coverNode: JQuery<HTMLElement>;
  private audio: HTMLAudioElement;
  private keyPosition: number;

  constructor(coverNode: JQuery<HTMLElement>) {
    super();
    this.coverNode = coverNode;
    this.audio = new Audio('./static/secret.mp3');
    /**
     * 隠し入力キーの長さに対して, 入力がどこまで行われたかを保持しておく.
     */
    this.keyPosition = 0;
    // [NOTE] keydownイベントはキーが押された時にイベントバブリングの結果, ブラウザ側から出されているように見えるイベントです
    //        keydownイベントでは文字はJS特有の文字コードとしてある数が送られてきます.
    //        その文字コードを表す数はevent.keyCodeで取得できます.
    $(document).on('keydown', (event) => {
      this.handle(event);
    });
    this.on('display', () => {
      this.display();
    });
  }
  //#region イベントが来たら呼ばれるメソッド群
  private handle(
    event: JQuery.KeyDownEvent<Document, undefined, Document, Document>
  ): void {
    if (event.keyCode === secretKeys[this.keyPosition]) {
      // [NOTE] 入力が下の方にあるkeysのlengthに一致したらdisplayイベントを発火する
      // [NOTE] 一つ上のif文で, 入力されたキーが一致していることの確認は済んでいる
      if (++this.keyPosition === secretKeys.length) {
        this.emit('display');
        this.keyPosition = 0;
      }
    } else this.keyPosition = 0;
  }

  private display(): void {
    this.audio.play();
    this.coverNode.prepend("<img id='secret' src=" + secretPng + '>');
    this.coverNode.show();
    setTimeout(() => {
      this.coverNode.empty();
      this.coverNode.hide();
    }, 2500);
  }
  //#endregion
}
