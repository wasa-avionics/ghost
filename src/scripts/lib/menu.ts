import Electron from 'electron';
const remote = Electron.remote;
const { Menu, MenuItem } = remote;
/**
 * [NOTE] electronの左上の方に表示されるメニューの記述. electronはメニューにどんなものを置くかも自由に決定できる.
 */
export class GhostMenu {
  private topMenu: Electron.Menu;
  /**
   * [NOTE] map内からアクセスされます.
   */
  public subMenuOfEdit: Electron.Menu;
  private subMenuOfView: Electron.Menu;
  private subMenuOfWindow: Electron.Menu;
  private subMenuOfHelp: Electron.Menu;
  private subMenuOfApp: Electron.Menu | null;
  private subMenuOfSpeech: Electron.Menu | null;
  /**
   * [NOTE] generatorから使われる.
   */
  public generatorLabel: boolean | undefined;

  constructor() {
    this.topMenu = new Menu();

    this.subMenuOfEdit = new Menu();
    this.subMenuOfView = new Menu();
    this.subMenuOfWindow = new Menu();
    this.subMenuOfHelp = new Menu();
    this.subMenuOfApp = null;
    this.subMenuOfSpeech = null;

    // [NOTE] roleについてはここから選択してます. https://www.electronjs.org/docs/api/menu-item
    //#region Editメニューへの子メニューの追加.
    this.subMenuOfEdit.append(
      new MenuItem({
        role: 'copy',
      })
    );
    this.subMenuOfEdit.append(
      new MenuItem({
        role: 'paste',
      })
    );
    //#endregion
    //#region Viewメニューへの子メニューの追加
    this.subMenuOfView.append(
      new MenuItem({
        role: 'reload',
      })
    );
    this.subMenuOfView.append(
      new MenuItem({
        role: 'forceReload',
      })
    );
    this.subMenuOfView.append(
      new MenuItem({
        role: 'toggleDevTools',
      })
    );
    this.subMenuOfView.append(
      new MenuItem({
        type: 'separator',
      })
    );
    this.subMenuOfView.append(
      new MenuItem({
        role: 'togglefullscreen',
      })
    );
    //#endregion
    //#region Helpメニューへの子メニューの追加
    this.subMenuOfHelp.append(
      new MenuItem({
        label: 'Learn More',
        click: (): void => {
          Electron.shell.openExternal('https://youtu.be/MJdz3i44dIc');
        },
      })
    );
    //#endregion
    //#region  process.platform === 'darwin' はMac用の設定です
    if (process.platform === 'darwin') {
      this.configChildMenuForMac();
    } else {
      this.configChildMenuForAnyOsExceptMac();
    }
    //#region トップメニューへの, 一段階目の階層の各メニューの追加
    this.topMenu.append(
      new MenuItem({
        label: 'Edit',
        submenu: this.subMenuOfEdit,
      })
    );
    this.topMenu.append(
      new MenuItem({
        label: 'View',
        submenu: this.subMenuOfView,
      })
    );
    this.topMenu.append(
      new MenuItem({
        label: 'Window',
        submenu: this.subMenuOfWindow,
      })
    );
    this.topMenu.append(
      new MenuItem({
        label: 'Help',
        submenu: this.subMenuOfHelp,
      })
    );
    //#endregion
    this.update();
  }

  update(): void {
    Menu.setApplicationMenu(this.topMenu);
  }

  private configChildMenuForMac(): void {
    this.subMenuOfApp = new Menu();
    //#region Appメニューへの子メニューの追加とトップメニューへのAppメニューの追加
    this.subMenuOfApp.append(
      new MenuItem({
        role: 'about',
      })
    );
    this.subMenuOfApp.append(
      new MenuItem({
        type: 'separator',
      })
    );
    this.subMenuOfApp.append(
      new MenuItem({
        role: 'services',
        submenu: [],
      })
    );
    this.subMenuOfApp.append(
      new MenuItem({
        type: 'separator',
      })
    );
    this.subMenuOfApp.append(
      new MenuItem({
        role: 'hideOthers',
      })
    );
    this.subMenuOfApp.append(
      new MenuItem({
        role: 'unhide',
      })
    );
    this.subMenuOfApp.append(
      new MenuItem({
        type: 'separator',
      })
    );
    this.subMenuOfApp.append(
      new MenuItem({
        role: 'quit',
      })
    );
    this.topMenu.append(
      new MenuItem({
        label: 'Ghost',
        submenu: this.subMenuOfApp,
      })
    );
    //#endregion
    this.subMenuOfSpeech = new Menu();
    //#region Speechメニューへの子メニューの追加とトップメニューへのSpeechメニューの追加
    this.subMenuOfSpeech.append(
      new MenuItem({
        role: 'startSpeaking',
      })
    );
    this.subMenuOfSpeech.append(
      new MenuItem({
        role: 'stopSpeaking',
      })
    );
    //#endregion
    //#region Editメニューへの子メニューの追加
    this.subMenuOfEdit.append(
      new MenuItem({
        type: 'separator',
      })
    );
    this.subMenuOfEdit.append(
      new MenuItem({
        label: 'Speech',
        submenu: this.subMenuOfSpeech,
      })
    );
    //#endregion
    //#region Windowメニューへの子メニューの追加
    this.subMenuOfWindow.append(
      new MenuItem({
        role: 'close',
      })
    );
    this.subMenuOfWindow.append(
      new MenuItem({
        role: 'minimize',
      })
    );
    this.subMenuOfWindow.append(
      new MenuItem({
        role: 'zoom',
      })
    );
    this.subMenuOfWindow.append(
      new MenuItem({
        type: 'separator',
      })
    );
    this.subMenuOfWindow.append(
      new MenuItem({
        role: 'front',
      })
    );
    //#endregion
  }
  private configChildMenuForAnyOsExceptMac(): void {
    this.subMenuOfWindow.append(
      new MenuItem({
        role: 'minimize',
      })
    );
    this.subMenuOfWindow.append(
      new MenuItem({
        role: 'close',
      })
    );
  }
}
