import EventEmitter from 'events';
import settings from 'electron-settings';
import { Gauge } from './display/gauge/gauge';
import { MeterInfo } from './display/meter';
import { Orientation } from './display/orientation';
import { Plain } from './display/plain';
import { Map } from './display/map/map';
/*eslint-disable*/
const graphicsStart = require('../../static/graphics-start.png');
const graphicsStop = require('../../static/graphics-stop.png');
/*eslint-enable*/
// [NOTE] GraphicsObjectsとして扱える(updateイベントが流れてきたら自身のGraphicsを更新する責任をもつ)実体の集合に使う型
export type GraphicsObjects = (Map | Plain | MeterInfo | Gauge | Orientation)[];

// [NOTE] GraphicsObjectsに対して, updateイベントを一括で発信するための仲介役. 画面のGraphicsのONOFFの切り替えに際してのGraphicsobjectの有効無効化の責任も負う.
class Graphics extends EventEmitter {
  // [NOTE] setup.tsのgraphicObjectsが入る
  private graphicArray: GraphicsObjects;
  private iconNode: JQuery<HTMLElement>;
  private statusNode: JQuery<HTMLElement>;
  private toggleNode: JQuery<HTMLElement>;
  private graphicsEnabled: boolean;

  constructor(
    graphicArray: GraphicsObjects,
    iconNode: JQuery<HTMLElement>,
    statusNode: JQuery<HTMLElement>,
    toggleNode: JQuery<HTMLElement>
  ) {
    super();
    this.graphicArray = graphicArray;
    this.iconNode = iconNode;
    this.statusNode = statusNode;
    this.toggleNode = toggleNode;
    this.graphicsEnabled = settings.get('graphics.enabled', true) as boolean;
    //#region イベントリスナーの登録
    // [NOTE] IDにgraphic-buttonとあるノードへのクリックの監視
    this.toggleNode.on('click', () => {
      this.toggleGraphics();
    });
    // [NOTE] 自分自身にupdateイベントが来たら表示を更新する.
    this.on('update', () => {
      this.updateGraphics();
    });
    //#endregion
    this.setupStatus();
  }
  // [NOTE] 有効無効をユーザー側にpng画像で知らせる
  setupStatus(): void {
    if (this.graphicsEnabled) {
      this.statusNode.html('Graphics Running');
      this.iconNode.attr('src', graphicsStart);
      // [NOTE] 有効化されているなら自分自身にupdateイベントを発信し, GraphicsObjectsへ更新を通知
      this.emit('update');
    } else {
      this.statusNode.html('Graphics Paused');
      this.iconNode.attr('src', graphicsStop);
    }
  }
  //#region イベントが来たら呼ばれるメソッド群
  // [NOTE] 表示系統のONOFFの切り替えを行う
  private toggleGraphics(): void {
    this.graphicsEnabled = !this.graphicsEnabled;
    settings.set('graphics.enabled', this.graphicsEnabled);
    this.setupStatus();
  }
  // [NOTE] updateイベントがこのGraphicsManagerに飛んで来たらgraphicObjectsへupdateイベントを発火して伝える.
  // [NOTE] 基本的に全てのgraphicsの更新を行いたいときはGraphicsManagerへupdateイベントを発火する.
  private updateGraphics(): void {
    if (this.graphicsEnabled) {
      const size = this.graphicArray.length;
      for (let i = 0; i < size; i++) {
        this.graphicArray[i].emit('update');
      }
    }
  }
  //#endregion
}
export { Graphics as updateEventEmitterToGraphics };
