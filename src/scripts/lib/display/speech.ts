import EventEmitter from 'events';
const synth = window.speechSynthesis; // 喋らせるライブラリ (現在Macのみ対応?) windowsにも対応したぽいです.
import { ReactiveProperty } from '../reactiveProperty';
/*eslint-disable*/
const speechOn = require('../../../static/speech-on.png');
const speechOff = require('../../../static/speech-off.png');
/*eslint-enable*/
export class Speech extends EventEmitter {
  data: ReactiveProperty | undefined;
  iconNode: JQuery<HTMLElement> | undefined;
  statusNode: JQuery<HTMLElement> | undefined;
  toggleNode: JQuery<HTMLElement> | undefined;
  enabled: boolean | undefined;
  interval: NodeJS.Timeout | undefined;
  /**
   * [NOTE] speechObjectの存在意義が不明
   */
  constructor(
    data: ReactiveProperty,
    iconNode: JQuery<HTMLElement>,
    statusNode: JQuery<HTMLElement>,
    toggleNode: JQuery<HTMLElement> /*, speechObject*/
  ) {
    super();
    this.data = data;
    this.iconNode = iconNode;
    this.statusNode = statusNode;
    this.toggleNode = toggleNode;
    //this.speechObject = speechObject
    this.enabled = false;
    this.interval = undefined;
    this.toggleNode.on('click', () => {
      this.toggleSpeech();
    });
  }
  /**
   * speech機能の有効無効の切り替え.
   */
  private toggleSpeech(): void {
    this.enabled = !this.enabled;
    if (this.enabled) {
      this.iconNode?.attr('src', speechOn);
      this.statusNode?.html('Speech Enabled');
      this.interval = this.createIntervalToSayAfter(1500);
    } else {
      this.disposeIntervalIfExist(this.interval);
      this.iconNode?.attr('src', speechOff);
      this.statusNode?.html('Speech Disabled');
    }
  }

  private sayValue(): void {
    if (this.data) {
      this.speak(this.data.getValue().toString(), undefined);
    }
  }

  /**
   * @param str しゃべらせるテキスト
   * @param delay 何ミリ秒後にしゃべらせるか
   */
  private speak(str: string, delay?: number): void {
    if (delay === undefined) synth.speak(new SpeechSynthesisUtterance(str));
    else
      setTimeout(function () {
        synth.speak(new SpeechSynthesisUtterance(str));
      }, delay);
  }
  //#region 補助的なメソッド
  private createIntervalToSayAfter(delay: number): NodeJS.Timeout {
    return setInterval(() => {
      this.sayValue();
    }, delay);
  }

  private disposeIntervalIfExist(interval: NodeJS.Timeout | undefined): void {
    if (!interval) return;
    clearInterval(interval);
  }
  //#endregion
}
