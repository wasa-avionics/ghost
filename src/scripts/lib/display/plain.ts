import EventEmitter from 'events';
import { ReactiveProperty } from '../reactiveProperty';
// [NOTE] データを重複なくただ表示するモジュール
class PlainValue extends EventEmitter {
  private data: ReactiveProperty;
  private valueNode: JQuery<HTMLElement>;
  /**
   * 何らかの理由で表示する値に足しておきたい時に用いる. いまのところはyawで-360してつかっている.
   */
  private valueOffset: number;
  constructor(data: ReactiveProperty, valueNode: JQuery, valueOffset = 0) {
    super();
    this.data = data;
    this.valueNode = valueNode;
    this.valueOffset = valueOffset;
    // [NOTE] GraphicsManagerからupdateが発火される
    this.on('update', () => {
      this.update();
    });
  }

  private update(): void {
    if (this.data.isDupe()) return;
    this.valueNode.html((this.data.getValue() + this.valueOffset).toString());
  }
}
export { PlainValue as Plain };
