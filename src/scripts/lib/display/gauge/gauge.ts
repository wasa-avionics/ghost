import EventEmitter from 'events';
import { ReactiveProperty } from '../../reactiveProperty';
import {
  CanvasPoint,
  createArc,
  polarToCartesianCoordinates,
  createLine,
  translateInCartesian,
  createCanvasPoint,
  negateCanvasPoint,
  createText,
} from './canvasRapper';
import { GaugeOptions } from './gaugetypes';

type GaugeConstant = {
  outerCircleRadius: number;
  outerCircleLineThickness: number;
  thinScaleThickness: number;
  scaleTextSize: number;
  needleLength: number;
  innerCircleRadiusForThickScaleEdge: number;
  thickScaleThickness: number;
  innerCircleRadiusForScaleText: number;
  fineTuningX: number;
  fineTuningY: number;
  innerCircleRadiusForThinScaleEdge: number;
  textDistanceFromGauge: number;
  scaleTextPositionTuningValueForOneDigit: number;
};

/**
 * ゲージの円弧と目盛りとテキスト位置の調整のために使用される. ゲージの位置が気に入らなかったらここをいじる.
 * 値はいい感じになるように設定されており, 特にこれと言って決まっていない.
 */
const positionTuning: GaugeConstant = {
  outerCircleRadius: 0.9,
  outerCircleLineThickness: 0.02,
  thinScaleThickness: 0.01,
  scaleTextSize: 0.15,
  needleLength: 0.75,
  innerCircleRadiusForThickScaleEdge: 0.775,
  thickScaleThickness: 0.05,
  innerCircleRadiusForScaleText: 0.65,
  fineTuningX: 0.12,
  fineTuningY: 0.07,
  innerCircleRadiusForThinScaleEdge: 0.85,
  textDistanceFromGauge: 0.1,
  scaleTextPositionTuningValueForOneDigit: 0.06,
};

/**
 * 画面の右側に表示されるゲージの表示を担当する.
 */
export class Gauge extends EventEmitter {
  private data: ReactiveProperty;
  private windowNode: JQuery<Window & typeof globalThis>;
  /**
   * 7セグフォントの数字のあるノード.
   */
  private valueNode: JQuery<HTMLElement>;
  private needleNode: JQuery<HTMLElement>;
  private canvasNode: HTMLCanvasElement;
  private gaugeOptions: GaugeOptions;
  private canvasContext: CanvasRenderingContext2D | null;
  private gaugeCircleCenter: CanvasPoint;
  private radius: number;
  private adjustedGaugeConst: GaugeConstant;
  public lastValue: number | undefined;
  private width: number | undefined;
  private height: number | undefined;

  constructor(
    data: ReactiveProperty,
    windowNode: JQuery<Window & typeof globalThis>,
    valueNode: JQuery,
    needleNode: JQuery,
    /**
     * 型でCanvasElement以外は弾くようにしたかったけれどもJQueryを使う以上無理みたいです.
     */
    canvasNode: JQuery<HTMLElement>,
    gaugeOptions: GaugeOptions
  ) {
    super();
    this.data = data;
    this.windowNode = windowNode;
    this.valueNode = valueNode;
    this.needleNode = needleNode;
    this.canvasNode = canvasNode[0] as HTMLCanvasElement;
    this.gaugeOptions = gaugeOptions;
    this.canvasContext = this.canvasNode.getContext('2d');
    this.radius = 0;
    this.adjustedGaugeConst = this.updateGaugeConst(0); // デフォルト値
    this.gaugeCircleCenter = createCanvasPoint(0, 0); // デフォルト値
    this.windowNode.on('resize', () => {
      this.resizeGauge();
    });
    this.on('update', () => {
      this.updateData();
    });
    this.resizeGauge();
  }

  private updateData(): void {
    const val = this.data.getValue();
    if (this.data.isDupe()) return;

    const deg = this.calculateCurrentNeedleDegree(
      val,
      this.gaugeOptions.tickMultiplier,
      this.gaugeOptions.tickOffset
    );
    this.valueNode.html(val.toFixed(this.gaugeOptions.fixed));
    this.needleNode.animate(
      { degree: deg }, // step関数はこのプロパティごとに動く.(複数あれば複数に対してstepに登録された関数が動く) プロパティ名は特に意味はない.
      {
        duration: 30, // 30ミリ秒で動かすものとする.(自由に変更して)
        step: (now: number): void => {
          this.needleNode.css({
            '-webkit-transform': 'rotate(' + now + 'deg)',
          });
        },
      }
    );
    this.lastValue = val;
  }

  /**
   * ゲージの円型メーター部分の描画とサイズ変更を行う.
   */
  private resizeGauge(): void {
    this.updateMetrics();
    this.repositionNeedle();
    this.repositionValue();
    this.drawGauge();
  }

  /**
   * 設定を読み込んで, ゲージの中心, Canvasの幅, 高さ, ゲージを書く際に必要になる定数の更新を行う
   */
  private updateMetrics(): void {
    this.width = this.canvasNode.scrollWidth;
    this.height = this.canvasNode.scrollHeight;

    this.gaugeCircleCenter = createCanvasPoint(
      this.width * this.gaugeOptions.gaugeCircleCenterPoint.horizontalRatio,
      this.height * this.gaugeOptions.gaugeCircleCenterPoint.verticalRatio
    );

    const radiusCandidate1 =
      this.width * this.gaugeOptions.gaugeRadius.RatioToCanvasWidth;
    const radiusCandidate2 =
      this.height * this.gaugeOptions.gaugeRadius.RatioToCanvasHeight;

    // [NOTE] ゲージ円の基準はcanvasの幅か高さの小さいほうにしないとはみ出てしまうので小さい方をとる.
    this.radius =
      radiusCandidate1 > radiusCandidate2 ? radiusCandidate2 : radiusCandidate1;
    this.adjustedGaugeConst = this.updateGaugeConst(this.radius);
  }

  private updateGaugeConst(radius: number): GaugeConstant {
    const newPositionTuning: GaugeConstant = {} as GaugeConstant;
    (Object.keys(positionTuning) as (keyof GaugeConstant)[]).forEach((key) => {
      newPositionTuning[key] = radius * positionTuning[key];
    });
    return newPositionTuning;
  }

  private repositionNeedle(): void {
    this.needleNode.css({
      height: this.adjustedGaugeConst.needleLength + 'px',
      // [NOTE] 針の高さは画面の縦の半分の大きさから針自身の高さを引いたもの. 針の中心をcanvasの中心にもってくるため.
      top:
        this.gaugeCircleCenter.y - this.adjustedGaugeConst.needleLength + 'px',
    });
  }

  /**
   * 回転数以外の7セグ表示について位置調整を行う.
   * 回転数とそれ以外では親要素が異なり, 同じ設定だと回転数の7セグ表示部がどこかに飛んで行ってしまうために必要
   */
  private repositionValue(): void {
    if (this.gaugeOptions.isCadence) {
      return;
    }
    this.valueNode.css({
      width: this.radius * 0.6 + 'px',
      height: this.radius * 0.18 + 'px',
      left: this.gaugeCircleCenter.x - this.radius * 0.3 + 'px',
      top: this.gaugeCircleCenter.y + this.radius * 0.08 + 'px',
      'font-size': this.radius * 0.2 + 'px',
    });
  }

  /**
   * GaugeOptionsを用いて, ゲージの円型メーター部分の描画を行う
   */
  private drawGauge(): void {
    const canvas = this.canvasNode;
    const context = this.canvasContext;
    canvas.width = this.width as number;
    canvas.height = this.height as number;
    if (!context) {
      return;
    }
    createArc(
      context,
      this.gaugeCircleCenter,
      this.adjustedGaugeConst.outerCircleRadius,
      this.gaugeOptions.arcStart,
      this.gaugeOptions.arcEnd,
      '#00FFFF',
      this.adjustedGaugeConst.outerCircleLineThickness,
      this.gaugeOptions.isAntiClockwise
    );
    context.stroke();
    context.beginPath();
    this.createScale(context);
  }

  private createScale(context: CanvasRenderingContext2D): void {
    for (
      let tick = this.gaugeOptions.tickMin;
      tick <= this.gaugeOptions.tickMax;
      tick += 10 //ベースは10としているだけで, scaleIntervalを変えることで調節可能.
    ) {
      const rad = this.calculateRadian(tick);
      this.createScaleText(context, tick, rad);
      const innerEdge: CanvasPoint = this.calculateScaleLineInnerEdgeXYPosition(
        tick,
        rad
      );
      const outerEdge: CanvasPoint = this.calculateScaleLineOuterEdgeXYPosition(
        rad
      );
      const startPoint = this.translateToCanvasCoordinate(
        innerEdge,
        this.gaugeCircleCenter
      );
      const endPoint = this.translateToCanvasCoordinate(
        outerEdge,
        this.gaugeCircleCenter
      );
      const lineWidth = this.decideScaleLineType(tick);
      context.beginPath();
      createLine(context, startPoint, endPoint, lineWidth, '#FFFFFF');
      context.stroke();
    }
  }

  /**
   * ゲージの目盛りにつける文字を作成する.
   * canvasにstrokeメソッドで描画する処理についての責任は持たないので, 呼び出した側の責任でstrokeメソッドする
   */
  private createScaleText(
    context: CanvasRenderingContext2D,
    tick: number,
    rad: number
  ): void {
    if (!this.isThickScale(tick)) {
      return;
    }
    const textPosition: CanvasPoint = this.calculateScaleTextXYPosition(rad);
    const axisAdjustedtextPosition: CanvasPoint = this.translateToCanvasCoordinate(
      textPosition,
      this.gaugeCircleCenter
    );
    const fineTunedTextPosition: CanvasPoint = this.fineTuneTextPosition(
      tick,
      axisAdjustedtextPosition
    );

    const text = this.calculateScaleTextNumber(tick).toString();
    const fillStyle = '#FFFFFF'; //文字の色
    const font =
      'italic ' +
      this.adjustedGaugeConst.scaleTextSize.toFixed(3) +
      'px sans-serif'; // fontの指定. cssと同じ
    const textBaseline = 'top'; //基準線をどこにひくか
    createText(
      context,
      text,
      fineTunedTextPosition,
      fillStyle,
      textBaseline,
      font
    );
    context.stroke();
  }

  //#region 補助的なメソッド群

  private calculateCurrentNeedleDegree(
    val: number,
    tickMultiplier: number,
    tickOffset: number
  ): number {
    return val * tickMultiplier + tickOffset;
  }

  /**
   * ゲージの大きな目盛りについている文字の数字を計算する.
   * @param tick
   */
  private calculateScaleTextNumber(tick: number): number {
    return (
      (tick + this.gaugeOptions.textOffset) * this.gaugeOptions.textMultiplier
    );
  }
  /**
   * scaleIntervalが今のところついでにラジアンへの変換もやってしまっているので, ラジアンへと変換された角度を返す
   * こいつのやってることはいまいち明確になっていないのでダメ.要修正.
   * @param tick 調整前の純粋な角度
   */
  private calculateRadian(tick: number): number {
    return (
      (tick + this.gaugeOptions.tickDrawOffset) *
      this.gaugeOptions.scaleInterval
    );
  }

  /**
   * ゲージの目盛りを描く際に, 目盛りが太い目盛りなのか細い目盛りなのかを判別し, その真偽値を返す
   */
  private isThickScale(tick: number): boolean {
    return (
      (tick + this.gaugeOptions.tickLargeOffset) %
        this.gaugeOptions.tickLargeSize ===
      0
    );
  }

  /**
   * x,y軸の向きを逆にして, 原点をゲージの円弧の中心から, Canvasのもともとある原点に移す座標変換を行う.
   * @param point ゲージを中心として, 左向きにx軸正方向, 上向きにy軸正方向にとった時の位置
   * @param gaugeCircleCenter ゲージの中心
   */
  private translateToCanvasCoordinate(
    point: CanvasPoint,
    gaugeCircleCenter: CanvasPoint
  ): CanvasPoint {
    // canvasの座標軸と真逆なのでそろえてあげる.
    const axisAjustedInnerEdge = negateCanvasPoint(point);
    // ゲージの中心からcanvasの原点への座標軸の平行移動
    const translatedPoint: CanvasPoint = translateInCartesian(
      axisAjustedInnerEdge,
      negateCanvasPoint(gaugeCircleCenter)
    );
    return translatedPoint;
  }

  /**
   * 内側に伸びる目盛りについて, 内側にどの程度伸びるべきかを決定し, その伸びる先の座標を返す.(ただし, 円弧の中心は原点に存在するとしているので, そうではない場合には平行移動を必要とする.)
   * @param tick 目盛りの現在の個数的なもの?
   * @param rad 描きたい目盛りの位置するゲージ内での角度
   */
  private calculateScaleLineInnerEdgeXYPosition(
    tick: number,
    rad: number
  ): CanvasPoint {
    const point: CanvasPoint = this.isThickScale(tick)
      ? polarToCartesianCoordinates(
          rad,
          this.adjustedGaugeConst.innerCircleRadiusForThickScaleEdge
        )
      : polarToCartesianCoordinates(
          rad,
          this.adjustedGaugeConst.innerCircleRadiusForThinScaleEdge
        );

    return point;
  }

  /**
   * 内側に伸びる目盛りについて, 目盛りの線の外側の端点の位置を決定する. その端点は円弧上に存在するとしている.
   * ただし, 円弧の中心は原点に存在するとしているので, そうではない場合には平行移動を必要とする.
   */
  private calculateScaleLineOuterEdgeXYPosition(rad: number): CanvasPoint {
    return polarToCartesianCoordinates(
      rad,
      this.adjustedGaugeConst.outerCircleRadius
    );
  }

  /**
   * 目盛りにつける文字について, 文字を書き始める位置を決定する.
   * ただし, 円弧の中心は原点に存在するとしているので, そうではない場合には平行移動を必要とする.
   */
  private calculateScaleTextXYPosition(rad: number): CanvasPoint {
    return polarToCartesianCoordinates(
      rad,
      this.adjustedGaugeConst.innerCircleRadiusForScaleText
    );
  }

  /**
   *  ゲージの円の目盛りの線を太線で描くか, 細線で描くかを判別し, 線の太さを返す
   */
  private decideScaleLineType(tick: number): number {
    const lineThickness = this.isThickScale(tick)
      ? this.adjustedGaugeConst.thickScaleThickness
      : this.adjustedGaugeConst.thinScaleThickness;
    return lineThickness;
  }

  private fineTuneTextPosition(tick: number, Point: CanvasPoint): CanvasPoint {
    let innerX = Point.x;
    let innerY = Point.y;
    const textNumber = this.calculateScaleTextNumber(tick);
    // 一桁の時配置が汚くなるので調整
    if (textNumber >= 0 && textNumber < 10) {
      innerX += this.adjustedGaugeConst.scaleTextPositionTuningValueForOneDigit;
    }
    innerX -= this.adjustedGaugeConst.fineTuningX;
    innerY -= this.adjustedGaugeConst.fineTuningY;
    return createCanvasPoint(innerX, innerY);
  }
  //#endregion
}
