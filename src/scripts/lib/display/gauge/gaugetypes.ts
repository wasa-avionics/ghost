/**
 * ゲージの半径は, ゲージの描画に割けるCanvasの縦か横か短い方に合わせているが, そのどちらかが選択された際に,
 * 半径をその選択された方の長さの何倍のものとして取るかを保持する.
 * 円弧の直径を短い方に合わせたいときには0.5を入力する.
 */
export type GaugeRadius = {
  RatioToCanvasWidth: number;
  RatioToCanvasHeight: number;
};

/**
 * 既に描かれているゲージの円弧の中心の白丸はCanvasの何割の部分に描かれているのかを保持する.
 * cssのgaugeCenterのleftとtopの設定と合わせないと, ゲージの中心が既に描かれている白丸にあわない
 */
export type GaugeCenterCanvasPositionRatio = {
  horizontalRatio: number;
  verticalRatio: number;
};

/**
 * ゲージを表示する際の描画のオプション
 */
export type GaugeOptions = {
  isCadence: boolean;
  /**
   * 目盛りの線の表示間隔.
   */
  scaleInterval: number;
  /**
   * ラジアンでの指定. ゲージの中心から見て東に向かう方向の線が0である.
   */
  arcStart: number;
  /**
   * ラジアンでの指定. ゲージの中心から見て東に向かう方向の線が0である.
   */
  arcEnd: number;
  isAntiClockwise: boolean;
  tickMin: number;
  tickMax: number;
  /**
   * ゲージの中心から見て東に向かう方向の線を基準にして, 何目盛り分だけ目盛りの0の位置をずらすか.
   * 目盛りの線の間隔に応じてずらす値は変化するので注意.
   */
  tickDrawOffset: number;
  /**
   * 文字盤のラベルを入れる数字はいくつからtickLargeSizeの数ごとに増えるのかがいいかを決める. 現在はtickを10ごとに増やしているので,
   * この数も10の倍数でなければいけない.
   */
  tickLargeOffset: number;
  /**
   * 大きい目盛りを何度ごとに入れるかの設定 例えば90と入れたなら, 文字は0, 90, 180で表示される
   */
  tickLargeSize: number;
  /**
   * 針の初期位置の決定. ゲージの中心から見て東に向かう方向の線が0である.
   */
  tickOffset: number;
  /**
   * 針がどちらの向きに動くか.
   * 文字盤がisAnticlockwiseだったならこれも多分-1になるのでこれいらなくないと思うかもしれないが, scaleIntervalに0.9などを掛けて入れた場合には
   * こちらも0.9としておかないと, 針の動きがおかしくなる.
   */
  tickMultiplier: number;
  /**
   * 文字盤の0を何度ずらすか. ゲージの中心から見て東に向かう方向の線が0である.
   */
  textOffset: number;
  /**
   * 文字盤の目盛りの数の大きさの調整用. 例えば, 10とやると, 300, 600, 900...となったりする.
   */
  textMultiplier: number;
  /**
   * 数字を小数点以下何桁まで表示するか.
   */
  fixed: number;
  gaugeRadius: GaugeRadius;
  gaugeCircleCenterPoint: GaugeCenterCanvasPositionRatio;
};
