export type CanvasPoint = {
  x: number;
  y: number;
};

export function createCanvasPoint(x: number, y: number): CanvasPoint {
  return { x: x, y: y };
}

/**
 * canvasに円弧を描く下準備を行う.
 * canvasにstrokeメソッドで描画する処理についての責任は持たないので, 呼び出した側の責任でstrokeメソッドする
 */
export function createArc(
  canvasContext: CanvasRenderingContext2D,
  center: CanvasPoint,
  radius: number,
  startAngle: number,
  endAngle: number,
  color: string,
  lineWidth: number,
  anticlockwise?: boolean
): void {
  canvasContext.arc(
    center.x,
    center.y,
    radius,
    startAngle,
    endAngle,
    anticlockwise
  );
  canvasContext.strokeStyle = color;
  canvasContext.lineWidth = lineWidth;
}

/**
 * canvasにstrokeメソッドで描画する処理についての責任は持たないので, 呼び出した側の責任でstrokeメソッドする
 */
export function createLine(
  context: CanvasRenderingContext2D,
  startPoint: CanvasPoint,
  endPoint: CanvasPoint,
  lineWidth: number,
  color: string
): void {
  context.moveTo(startPoint.x, startPoint.y);
  context.lineTo(endPoint.x, endPoint.y);
  context.lineWidth = lineWidth;
  context.strokeStyle = color;
}

export function createText(
  context: CanvasRenderingContext2D,
  text: string,
  point: CanvasPoint,
  color: string,
  textBaseLine: CanvasTextBaseline,
  font: string
): void {
  context.fillStyle = color; //文字の色
  context.font = font; // fontの指定. cssと同じ
  context.textBaseline = textBaseLine; //基準線をどこにひくか
  context.fillText(text, point.x, point.y);
}

/**
 * 平行移動を行う.
 * @param translatedPoint 平行移動されるCanvaspoint
 * @param difference 平行移動元に減算される差分.
 */
export function translateInCartesian(
  translatedPoint: CanvasPoint,
  difference: CanvasPoint
): CanvasPoint {
  return {
    x: translatedPoint.x - difference.x,
    y: translatedPoint.y - difference.y,
  };
}

/**
 * 符号反転を行う.
 * canvasのx,y軸と, 極座標からxy座標への変換を考える際のx,y軸がともに逆になっているので, canvas上に描く際には注意.
 * @param point 符号反転されるCanvasPoint
 */
export function negateCanvasPoint(point: CanvasPoint): CanvasPoint {
  return { x: -point.x, y: -point.y };
}

/**
 * 二次元極座標から二次元直交座標への変換を行う.
 * @param angle 角度
 * @param radius 半径
 */
export function polarToCartesianCoordinates(
  angle: number,
  radius: number
): CanvasPoint {
  return { x: radius * Math.cos(angle), y: radius * Math.sin(angle) };
}

/**
 * 定数を掛ける
 */
export function multiplyCanvasPointByConst(
  point: CanvasPoint,
  constant: number
): CanvasPoint {
  return { x: point.x * constant, y: point.y * constant };
}
