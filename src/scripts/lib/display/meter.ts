import $ from 'jquery';
import EventEmitter from 'events';
import { ReactiveProperty } from '../reactiveProperty';

/**
 * メーターの矢印と数値の表示, 更新を担当する
 */
export class MeterInfo extends EventEmitter {
  private data: ReactiveProperty;
  private arrowNode: JQuery<HTMLElement>;
  private valueNode: JQuery<HTMLElement>;
  /**
   * 実際に表示する数値
   */
  private displayedValue: number;
  private parent: Meter | undefined;

  constructor(
    data: ReactiveProperty,
    arrowNode: JQuery<HTMLElement>,
    valueNode: JQuery<HTMLElement>
  ) {
    super();
    this.data = data;
    this.arrowNode = arrowNode;
    this.valueNode = valueNode;
    this.displayedValue = 0;
    this.parent = undefined;
    // [NOTE] updateEventEmitterToGraphicsからupdateが発火される
    this.on('update', () => {
      this.updateDisplayedValue();
      this.setArrowPosition();
    });
  }

  private setArrowPosition(): void {
    if (this.data.isDupe() || this.parent === undefined) return;

    const position = this.calculateArrowPosition(this.parent);
    const x: string = position.x;
    const y: string = position.y;
    // [NOTE] 30ミリ秒かけて矢印を動かす
    this.parent.IsRightMeter
      ? this.arrowNode.animate({ bottom: y, right: x }, 30)
      : this.arrowNode.animate({ bottom: y, left: x }, 30);
  }

  public resizeArrowPosition(): void {
    if (this.data.isDupe() || this.parent === undefined) return;

    const position = this.calculateArrowPosition(this.parent);
    const x: string = position.x;
    const y: string = position.y;

    this.parent.IsRightMeter
      ? this.arrowNode.css({ bottom: y, right: x })
      : this.arrowNode.css({ bottom: y, left: x });
  }

  private updateDisplayedValue(): void {
    this.displayedValue = this.data.getValue();
    // [NOTE] 表示する値は小数点二桁まで
    this.valueNode.html((this.displayedValue * 1.0).toFixed(2));
  }

  private calculateArrowPosition(parent: Meter): { x: string; y: string } {
    const fraction = this.displayedValue / parent.numberNodeCount;
    const y: string =
      (
        parent.bottom +
        // [NOTE] 数値が1刻みであることを利用し, 現在の数値はメーターの最大値の何分の1かという, fractionを掛けて, 数値と位置が対応するようにする.
        parent.height * fraction -
        (this.arrowNode?.outerHeight() as number) / 2
      ).toString() + 'px';
    let x: string;
    // [NOTE] 矢印ノードの位置は, 数値が, 全目盛りの数の半分未満の時には弧を描くように移動するようにする.
    if (fraction < 0.5) {
      x = (Math.sqrt(28.8 * fraction) + 5.5).toString() + '%';
    } else {
      x = '10%';
    }
    return { x, y };
  }
  /**
   * MeterInfoを取り付ける親Meterを設定する
   */
  public setParentMeter(ParentMeter: Meter): void {
    this.parent = ParentMeter;
  }
}
/**
 * メーターに必要な縦に伸びるゲージの描画とresizeイベントの処理を行うモジュール.
 * resizeイベント時に, MeterInfoへの関数呼び出しでのresize処理も行う.
 */
export class Meter extends EventEmitter {
  public IsRightMeter: boolean;
  /**
   * 目盛りがいくつ存在するかを表す. setup.tsで目盛りの個数の指定, 各目盛りの色の指定を行っている配列の長さが入る.
   */
  public numberNodeCount: number;
  public height: number;
  public bottom: number;

  private windowNode: JQuery<Window & typeof globalThis>;
  private meterName: string;
  private meterWrapperNode: JQuery<HTMLElement>;
  private meterNode: JQuery<HTMLElement>;
  private meterInfoArray: MeterInfo[];
  private meterInfoCount: number;
  private meterColorArray: string[];
  private numberNodeArray: JQuery<HTMLElement>[];
  private unit: number;
  constructor(
    windowNode: JQuery<Window & typeof globalThis>,
    meterName: string,
    meterIsRight: boolean,
    meterWrapperNode: JQuery<HTMLElement>,
    meterNode: JQuery<HTMLElement>,
    meterInfoArray: MeterInfo[],
    meterColorArray: string[]
  ) {
    super();
    this.windowNode = windowNode;
    this.meterName = meterName;
    this.IsRightMeter = meterIsRight;
    this.meterWrapperNode = meterWrapperNode;
    this.meterNode = meterNode;
    this.meterInfoArray = meterInfoArray;
    this.meterInfoCount = this.meterInfoArray.length;
    for (let i = 0; i < this.meterInfoCount; i++) {
      this.meterInfoArray[i].setParentMeter(this);
    }
    this.meterColorArray = meterColorArray;
    this.numberNodeArray = [];
    this.numberNodeCount = this.meterColorArray.length;
    this.height = 0;
    this.unit = 0;
    this.bottom = 0;
    this.draw();
    this.windowNode.on('resize', () => {
      this.resize();
    });
  }

  private updateSize(): void {
    this.height = this.meterNode.height() as number;
    this.unit = this.height / this.numberNodeCount;
    this.bottom =
      (this.windowNode.height() as number) -
      (this.meterNode.offset() as JQuery.Coordinates).top -
      this.height;
  }

  /**
   * 目盛りの色を描き, 目盛り間に白線を入れる.
   * @see https://developer.mozilla.org/ja/docs/Web/CSS/CSS_Images/Using_CSS_gradients
   */
  private drawLines(): void {
    // [NOTE] 目盛り間に白線を書く際に,linear-gradientの%表示を用いて行うので, メーター全体に対して, 目盛り一つの占める割合が何%であるかを計算する.
    const unit = parseInt((100 / this.numberNodeCount).toFixed(2));
    // [NOTE] linear-gradientはノードの全体を100%として, 例えばto bottom, white 0%, white 1%, red 30%)なら, ノードの上から全体の0%から1%の長さは完全な白で, 1%から30%にかけて段々と赤になり, 30%で完全な赤になることを示している.
    let backgroundString = 'linear-gradient(to bottom, white 0%, white 1%, ';
    for (let i = 0; i < this.numberNodeCount; i++) {
      backgroundString += this.meterColorArray[i];
      backgroundString += ' ';
      backgroundString += i * unit + (i === 0 ? 1 : 0); // 意図が不明
      backgroundString += '%, ';
      backgroundString += this.meterColorArray[i];
      backgroundString += ' ';
      backgroundString += i * unit + (unit - 1);
      backgroundString += '%, white ';
      // [NOTE] 二回white指定が続いているのは, ここでメーターの数時間の白線を描きたいため
      backgroundString += i * unit + (unit - 1);
      backgroundString += '%, white ';
      backgroundString += (i + 1) * unit;
      backgroundString += '%';
      if (i < this.numberNodeCount - 1) backgroundString += ', ';
    }
    backgroundString += ')';
    this.meterNode.css({ background: backgroundString });
  }

  /**
   * Meterの弧状になっていない上半分の文字の位置を設定する.弧状の部分の文字の位置は positionNumbersにて上書きする形で行う.
   */
  private drawNumbers(characterPosition: number): void {
    for (let i = 0; i <= this.numberNodeCount; i++) {
      const numberNode = this.createNumberNode(i, this.meterName);
      this.meterWrapperNode.append(numberNode);
      this.numberNodeArray[i] = numberNode;

      if (this.IsRightMeter) {
        numberNode.css({
          right: characterPosition + '%',
          'text-align': 'right',
        });
        continue;
      }
      numberNode.css({ left: characterPosition + '%' });
    }
  }

  private createNumberNode(
    index: number,
    meterName: string
  ): JQuery<HTMLElement> {
    let numberNodeString = "<div id='";
    numberNodeString += meterName;
    numberNodeString += 'Num';
    numberNodeString += index.toString();
    numberNodeString += "' class='meter-number' >";
    numberNodeString += index.toString();
    numberNodeString += '</div>';
    return $(numberNodeString);
  }

  /**
   * Meterの弧状の部分への文字の配置を行う際にどれくらい反らせるかの計算を行う.
   */
  private meterOffset(index: number): string {
    return Math.sqrt(2.4 * index) + 3.5 + '%';
  }
  /**
   * Meterの弧状の部分の数値の文字の配置を行う.
   */
  private positionNumbers(): void {
    for (let i = 0; i <= this.numberNodeCount; i++) {
      const numberNode = this.numberNodeArray[i];
      numberNode.css({
        bottom: ((this.bottom + this.unit * i) * 0.97).toFixed(3) + 'px',
      });

      if (i < this.numberNodeCount / 2) {
        if (this.IsRightMeter) numberNode.css({ right: this.meterOffset(i) });
        else numberNode.css({ left: this.meterOffset(i) });
      }
    }
  }

  private moveArrows(): void {
    for (let i = 0; i < this.meterInfoCount; i++) {
      this.meterInfoArray[i].resizeArrowPosition();
    }
  }

  private draw(): void {
    const MeterNumberPosition = 7;
    this.updateSize();
    this.drawNumbers(MeterNumberPosition);
    this.positionNumbers();
    this.drawLines();
    this.moveArrows();
  }

  private resize(): void {
    this.updateSize();
    this.positionNumbers();
    this.moveArrows();
  }
}
