import EventEmitter from 'events';
import { ReactiveProperty } from '../reactiveProperty';
// [NOTE] メーターなどの針を回転させたりするモジュール. orientationは方位の意味.
export class Orientation extends EventEmitter {
  private data: ReactiveProperty;
  private orientationNode: JQuery<HTMLElement>;
  public value: number;
  public lastValue: number | undefined;

  constructor(data: ReactiveProperty, orientationNode: JQuery) {
    super();
    this.data = data;
    this.orientationNode = orientationNode;
    this.value = 0;
    // [NOTE] GraphicsManagerからupdateが発火される
    this.on('update', () => {
      this.setOrientation();
    });
  }

  /**
   * cssアニメーションを用いて, 飛行機のアイコンを回転させる.
   */
  private setOrientation(): void {
    const value = this.data.getValue();
    if (this.data.isDupe()) return;
    this.orientationNode.animate(
      { degree: value },
      {
        duration: 30,
        // [NOTE] 針を動かす
        step: (now: number): void => {
          this.orientationNode.css({
            '-webkit-transform': 'rotate(' + now + 'deg)',
          });
        },
      }
    );
    this.lastValue = value;
  }
}
