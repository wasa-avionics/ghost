import EventEmitter from 'events';
// [NOTE] Timeクラスの関数を扱うため.
import { TimeObservable } from '../reactiveProperty';

// [NOTE] 表示時間の更新を行う
export class Clock extends EventEmitter {
  private timeObservable: TimeObservable;
  private dateNode: JQuery<HTMLElement>;
  private timeNode: JQuery<HTMLElement>;
  private lastDate: string | undefined;
  //private timeVal: number

  constructor(timedata: TimeObservable, dateNode: JQuery, timeNode: JQuery) {
    super();
    this.timeObservable = timedata;
    this.dateNode = dateNode;
    this.timeNode = timeNode;
    this.lastDate = undefined;
    //this.timeVal = Date.now()
    // [NOTE] updateはTimeクラスの中で, きっちり1秒ごとになるように発火される.
    this.timeObservable.on('update', () => {
      this.displayTime();
    });
  }
  // [NOTE] 表示されている時間と日付を更新する
  displayTime(): void {
    if (this.timeObservable.dateString !== this.lastDate) {
      this.dateNode.html(this.timeObservable.dateString);
      this.lastDate = this.timeObservable.dateString;
    }
    this.timeNode.html(this.timeObservable.timeString);
  }
}
