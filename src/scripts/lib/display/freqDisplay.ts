import EventEmitter from 'events';
import { TimeObservable, SerialComFreqObservable } from '../reactiveProperty';

// [NOTE] 更新頻度の設定とその表示の責任を負う
export class FreqDisplay extends EventEmitter {
  private dataObservable: TimeObservable;
  private serialCommunicationFreqObservable: SerialComFreqObservable;
  private freqNode: JQuery<HTMLElement>;
  private timeout: NodeJS.Timeout | undefined;

  constructor(
    dataDate: TimeObservable,
    dataFreq: SerialComFreqObservable,
    freqNode: JQuery
  ) {
    super();
    this.dataObservable = dataDate;
    this.serialCommunicationFreqObservable = dataFreq;
    this.freqNode = freqNode;
    this.timeout = undefined;
    //#region イベントリスナーの登録
    this.serialCommunicationFreqObservable.on('update', () => {
      this.updateConnectionStatusColorToNotifyUser();
    });
    this.dataObservable.on('update', () => {
      this.displayFreq();
    });
    //#endregion
    this.displayFreq();
  }
  /**
   * シリアル通信で値が来たら, 通信が生きているかどうかを表す色表示の更新を行う.
   * 正常なら緑, 切断なら黒. 最後の応答から1秒以上応答がなかったら黒色で表示する.
   */
  private updateConnectionStatusColorToNotifyUser(): void {
    this.disposeTimeoutIfExist(this.timeout);
    this.freqNode.css({ backgroundColor: 'green' });
    this.timeout = this.createTimeoutForColoringHTMLNodeBlackInOneSecond();
  }
  private displayFreq(): void {
    this.freqNode.html(
      this.serialCommunicationFreqObservable.getValue() + 'Hz'
    );
    this.serialCommunicationFreqObservable.reset(); // setValue(0) では毎秒更新が行われてしまう (緑になる)
  }

  //#region 補助的なメソッド
  private disposeTimeoutIfExist(timeout: NodeJS.Timeout | undefined): void {
    if (!timeout) {
      return;
    }
    clearTimeout(timeout);
  }

  private createTimeoutForColoringHTMLNodeBlackInOneSecond(): NodeJS.Timeout {
    return setTimeout(() => {
      this.freqNode.css({ backgroundColor: 'black' });
    }, 1000);
  }
  //#endregion
}
