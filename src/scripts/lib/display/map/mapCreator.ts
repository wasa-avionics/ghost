import { EventEmitter } from 'events';
import mapboxgl from 'mapbox-gl';
import { Point } from 'geojson';
import $ from 'jquery';

// [NOTE] もし仮に, mapboxをオンラインで, 地図情報も含めて利用する場合にはアクセストークンが必要. 今はローカルに地図情報を落してきて, mapboxの提供するgraphic系のライブラリをオフラインで使っているだけなのでいらない.
// [NOTE] オンラインでの利用の際には, 自分のアカウントを https://account.mapbox.com/で作成し, accountページからpublicなアカウントを持ってくる必要がある.
const accessToken = 'NOT-REQUIRED-WITH-YOUR-VECTOR-TILES-DATA';

export interface MapCreator {
  create(tileURL: string[]): Promise<mapboxgl.Map>;
}

// [NOTE] MapBoxのmap本体の作成を担当する.
export class MapCreatorCore extends EventEmitter implements MapCreator {
  private mapNode: JQuery<HTMLElement>;
  constructor(mapNode: JQuery<HTMLElement>) {
    super();
    this.mapNode = mapNode;
    mapboxgl.accessToken = accessToken;
  }

  // [NOTE] ここはvscodeなどのエディタでの一括変更が効かない(文字列型ばかりのため)ので変更する際には注意
  /**
   * mapboxで地図を表示するときに, 地図情報が入った地図を用いるのだが, その時に, 桶川でテストフライトを行うので, そこにフォーカスできるようにここで準備をしておく.
   */
  private mapStyle: mapboxgl.Style = {
    version: 8,
    sources: {
      'openmaptiles-japan': {
        type: 'vector',
        tiles: [] as string[],
        minzoom: 0,
        maxzoom: 14,
      },
      // eslint-disable-next-line @typescript-eslint/camelcase
      okegawa_point: {
        type: 'geojson',
        data: {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [139.523889, 35.975278],
          },
          properties: {},
        },
      },
    },
    layers: [
      {
        id: 'water',
        source: 'openmaptiles-japan',
        'source-layer': 'water',
        interactive: true,
        type: 'line',
        paint: {
          'line-color': '#0761FC',
        },
      },
      {
        id: 'aeroway',
        source: 'openmaptiles-japan',
        'source-layer': 'aeroway',
        interactive: true,
        type: 'line',
        paint: {
          'line-color': '#FC7907',
          'line-width': 5,
        },
      },
      {
        id: 'boundary',
        source: 'openmaptiles-japan',
        'source-layer': 'boundary',
        interactive: true,
        type: 'line',
        paint: {
          'line-color': '#66FF99',
          'line-width': 1,
        },
      },
      {
        id: 'transportation',
        source: 'openmaptiles-japan',
        'source-layer': 'transportation',
        interactive: true,
        type: 'line',
        paint: {
          'line-color': '#660099',
          'line-width': 1,
        },
      },
      {
        id: 'okegawa_point',
        type: 'circle',
        source: 'okegawa_point',
        paint: {
          'circle-radius': 10,
          'circle-color': '#007CBF',
        },
        minzoom: 3,
      },
    ],
  };

  private mapOptions: mapboxgl.MapboxOptions = {
    // [NOTE] mapのcanvasが格納されるHTMLElementのID名
    container: 'map',
    center: [139.523889, 35.975278],
    zoom: 13,
    minZoom: 1,
    maxZoom: 18,
    attributionControl: false,
    style: this.mapStyle,
  };

  public create = async (tileURL: string[]): Promise<mapboxgl.Map> => {
    this.clearMapNode();
    // [NOTE] MapBoxの表示にあたっては, MBTileデータベースの存在する場所を登録する必要がある.
    (this.mapStyle.sources?.[
      'openmaptiles-japan'
    ] as mapboxgl.VectorSource).tiles = tileURL;
    const map = new mapboxgl.Map(this.mapOptions);

    this.setEventHandler(map);
    return new Promise((resolve, reject) => {
      map.on('load', () => {
        resolve(map);
      });
      map.on('error', (error: Error) => {
        reject(error);
      });
    });
  };

  private clearMapNode(): void {
    this.mapNode.empty();
  }

  private setEventHandler(map: mapboxgl.Map): void {
    map.on('load', () => {
      // ズーム・方向ボタンの設置
      map.addControl(new mapboxgl.NavigationControl(), 'top-right');

      // マウスの先の座標を表示する
      const propertyList = $('#propertyList');

      map.on('mousemove', (event) => {
        this.mapMouseMoveEventHandler(event, propertyList, map);
      });
      this.setOkegawaPoint(map);
      this.setOkegawaPointClickEventHandler(map);
    });

    map.on('error', (error: Error) => {
      console.log(error);
    });
  }

  /**
   * マウスの先の座標を表示する
   * @param propertyList マップ上でのマウスの指す場所の緯度経度を表示したいHTMLElement
   * @param this.map? 表示されているmap
   */
  private mapMouseMoveEventHandler(
    e: mapboxgl.MapMouseEvent & mapboxgl.EventData,
    propertyList: JQuery<HTMLElement>,
    mapBoxMap: mapboxgl.Map
  ): void {
    propertyList.empty();
    const features = mapBoxMap.queryRenderedFeatures(e.point, {
      filter: ['in', '$type', 'Point'], // typeがPointのやつのみを拾ってくる. 誤差楕円は特にpropertiesを拾ってこない.
    });
    if (features[0]) {
      propertyList.html(
        JSON.stringify(e.lngLat, null, 2) +
          '<br/>' +
          JSON.stringify(features[0].properties, null, 2) // propertiesで何か表示「したいものがあれば表示できるようにする
      );
    } else {
      propertyList.html(JSON.stringify(e.lngLat, null, 2));
    }
  }

  private setOkegawaPoint(mapBoxMap: mapboxgl.Map): void {
    // 桶川ポイントの設置
    mapBoxMap.on('click', 'okegawa_point', (e) => {
      if (e.features === undefined) {
        return;
      }
      mapBoxMap.flyTo({
        center: (e.features[0].geometry as Point).coordinates as [
          number,
          number
        ],
        zoom: 13,
        speed: 0.9,
        curve: 1,
        easing: (t) => {
          return t;
        },
      });
    });
  }

  private setOkegawaPointClickEventHandler(mapBoxMap: mapboxgl.Map): void {
    // [NOTE] 桶川の飛行場の中心にカーソルが移動したら, カーソルの形を変える.
    const mapCanvas = mapBoxMap.getCanvas();
    mapBoxMap.on('mouseenter', 'okegawa_point', () => {
      mapCanvas.style.cursor = 'pointer';
    });
    mapBoxMap.on('mouseleave', 'okegawa_point', () => {
      mapCanvas.style.cursor = '';
    });
  }
}
