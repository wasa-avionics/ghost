import {
  GeoJSONSource,
  GeoJSONSourceRaw,
  GeoJSONSourceOptions,
} from 'mapbox-gl';

export type ImageOnMap = { width: number; height: number; data: Uint8Array };

/**
 * HEXカラーからRGBへと変換する.
 */
function hexToRGB(hex: string): { r: number; g: number; b: number } {
  if (hex.slice(0, 1) == '#') hex = hex.slice(1);
  return {
    r: parseInt(hex.slice(0, 2), 16),
    g: parseInt(hex.slice(2, 4), 16),
    b: parseInt(hex.slice(4, 6), 16),
  };
}

/**
 * 機体の進行方向を表す十字模様を意味する配列を返す
 * 描画する処理はここでは行わない
 */
export function createPlaneNav(
  hexColor: string,
  alphaValue: number,
  planeWidth = 128
): ImageOnMap {
  const planeData = new Uint8Array(planeWidth * planeWidth * 4);
  const rgb = hexToRGB(hexColor);
  for (let i = 0; i < planeWidth; i++) {
    for (let j = 0; j < planeWidth; j++) {
      const offset = (j * planeWidth + i) * 4;
      // [NOTE] 十字部分のみに色を入れておく
      if ((i >= 61 && i <= 66) || (j >= 23 && j <= 28)) {
        planeData[offset + 0] = rgb.r; // R
        planeData[offset + 1] = rgb.g; // G
        planeData[offset + 2] = rgb.b; // B
        planeData[offset + 3] = alphaValue; // A
      }
    }
  }
  return {
    width: planeWidth,
    height: planeWidth,
    data: planeData,
  };
}

/**
 * 機体の姿勢を表す矢印を意味する配列を返す
 * 描画する処理はここでは行わない
 */
export function createArrow(
  hexColor: string,
  alphaValue: number,
  arrowWidth = 128
): ImageOnMap {
  const rgb = hexToRGB(hexColor);
  const arrowWidthHalf = arrowWidth / 2;
  const arrowData = new Uint8Array(arrowWidth * arrowWidth * 4);
  for (let i = 0; i < arrowWidth; i++) {
    for (let j = 0; j <= arrowWidth; j++) {
      // タテの線を書く
      const sum = i + j;
      if (j >= 61 && j <= 66) {
        const offset = (i * arrowWidth + j) * 4;
        arrowData[offset + 0] = rgb.r;
        arrowData[offset + 1] = rgb.g;
        arrowData[offset + 2] = rgb.b;
        arrowData[offset + 3] = alphaValue;
      } else if (sum >= arrowWidthHalf - 3 && sum <= arrowWidthHalf + 3) {
        const offset = (i * arrowWidth + j) * 4;
        arrowData[offset + 0] = rgb.r;
        arrowData[offset + 1] = rgb.g;
        arrowData[offset + 2] = rgb.b;
        arrowData[offset + 3] = alphaValue;
        const xFlipOffset = (i * arrowWidth + arrowWidth - j - 1) * 4;
        arrowData[xFlipOffset + 0] = rgb.r;
        arrowData[xFlipOffset + 1] = rgb.g;
        arrowData[xFlipOffset + 2] = rgb.b;
        arrowData[xFlipOffset + 3] = alphaValue;
      }
    }
  }
  const ArrowImage = {
    width: arrowWidth,
    height: arrowWidth,
    data: arrowData,
  };

  return ArrowImage;
}

export function createGeoJSONEllipse(
  center: number[],
  radiusInMetersX: number,
  radiusInMetersY: number,
  DegToRad: number,
  pointCount = 64
): GeoJSONSource {
  const coords = {
    latitude: center[1],
    longitude: center[0],
  };

  const kmX = radiusInMetersX / 1000;
  const kmY = radiusInMetersY / 1000;

  const ret = [];
  // [NOTE] mapboxのpointは経度, 緯度で指定するので, メートルでの入力から度での入力に変換する必要がある
  // [NOTE] 緯度の一度は111.32kmに相当する.
  const distanceX = kmX / (111.32 * Math.cos(coords.latitude * DegToRad));
  // [NOTE] 経度の一度は110.574kmに相当する.(これも変動して, 極付近では一度当たり111.694kmだけどね) ちなみに1海里（マイル）：40,000km[地球の円周]÷360[度]÷60[分]=1,852m
  const distanceY = kmY / 110.574;

  let theta, x, y;
  // [NOTE] 楕円を作るために楕円の淵となる点をガンガン打っていく. デフォルトでは64点打つ
  for (let i = 0; i < pointCount; i++) {
    theta = (i / pointCount) * (2 * Math.PI); //2πで一周
    x = distanceX * Math.cos(theta);
    y = distanceY * Math.sin(theta);

    ret.push([coords.longitude + x, coords.latitude + y]);
  }

  const geojson: GeoJSONSourceRaw & GeoJSONSourceOptions = {
    type: 'geojson',
    data: {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [ret],
      },
      properties: {},
    },
  };
  return geojson as GeoJSONSource;
}
