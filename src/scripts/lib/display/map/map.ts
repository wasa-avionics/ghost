import {
  MapGraphicsUpdater,
  MapGraphicsUpdaterCreatorCore,
} from './mapGraphicsUpdater';
import { MapCreator } from './mapCreator';
import { EventEmitter } from 'events';
import { MBTileURLCreator } from './mbTileURLCreator';
import { MBTileDownLoader } from './mbTileDownloader';
import { GhostMenu } from '../../menu';
import { remote, MessageBoxReturnValue } from 'electron';
import $ from 'jquery';
// [NOTE] remoteはMainProcess(ウィンドウの作成を担当するところのapp.js)の関数をrendererから呼ぶためのものだが, 責務が曖昧になりやすいので注意
//        remoteからMenuItem, dialogを呼ばないと, undefinedのプロパティ~~は存在しませんのエラーが出るので留意. レンダラープロセスでは内部で直接はよべないようになっている.
const { MenuItem, dialog } = remote;

/**
 * MapBox関連の集約ルートとなるクラス
 */
export class Map extends EventEmitter {
  private mapCreator: MapCreator;
  private mapUpdater: MapGraphicsUpdater | undefined;
  private mapGraphicsUpdaterCreator: MapGraphicsUpdaterCreatorCore;
  private mbTileURLCreator: MBTileURLCreator;
  private mbTiledownloader: MBTileDownLoader;

  private mapDragDropNode: JQuery<HTMLElement>;
  private mapImportButtonNode: JQuery<HTMLElement>;
  private menu: GhostMenu;
  private downloadNode: JQuery<HTMLElement>;

  constructor(
    mapCreator: MapCreator,
    mbTileURLCreator: MBTileURLCreator,
    mbTileDownloader: MBTileDownLoader,
    mapGraphicsUpdaterCreator: MapGraphicsUpdaterCreatorCore,
    menu: GhostMenu,
    mapDragDropNode: JQuery<HTMLElement>,
    mapImportButtonNode: JQuery<HTMLElement>
  ) {
    super();
    this.mapCreator = mapCreator;
    this.mbTileURLCreator = mbTileURLCreator;
    this.mbTiledownloader = mbTileDownloader;
    this.mapGraphicsUpdaterCreator = mapGraphicsUpdaterCreator;

    this.mbTiledownloader.setDestination(mbTileURLCreator.getMBTileDirectory());

    this.mapDragDropNode = mapDragDropNode;
    this.mapImportButtonNode = mapImportButtonNode;
    this.menu = menu;
    // [TODO] download-mapというidの作成自体はmbTileDownloaderが行っているがこれはまずいので修正する.
    this.downloadNode = $('#download-map');

    this.setupEventHandler();
    this.setupMenu();

    this.loadMap();
    this.on('downloadCompleted', () => {
      this.loadMap();
    });
    this.on('load', () => {
      this.loadMap();
    });
    this.on('update', () => {
      this.updateGraphics();
    });
  }

  private loadMap = async (): Promise<void> => {
    const possibleTileURLs = await this.mbTileURLCreator.create();
    const tileURL = possibleTileURLs
      ? possibleTileURLs
      : this.mbTiledownloader.download();
    if (this.mbTiledownloader.isDownloading()) {
      this.displayDownloading();
      return;
    }
    if (!tileURL) {
      this.displayImportError(
        new Error(
          'MBTileファイルの読み込みに失敗しました. ファイルのダウンロードに失敗した可能性があります.'
        )
      );
      this.mbTiledownloader.displayNoMBTile();
      return;
    }
    this.mbTiledownloader.cancel();
    await this.mapCreator
      .create(tileURL)
      .then((map: mapboxgl.Map) => {
        return new Promise(() => {
          this.mapUpdater = this.mapGraphicsUpdaterCreator.create(map);
        });
      })
      .catch((error: Error) => {
        console.log(error);
        return new Promise(() => (this.mapUpdater = undefined));
      });
  };

  private updateGraphics(): void {
    if (!this.mapUpdater) {
      return;
    }
    this.mapUpdater.update();
  }

  private setupEventHandler(): void {
    this.setupClickEventhandler();
    this.setupDragDropEventHandler();
    this.setupDownLoadEventHandler();
  }

  /**
   * Menuの方からもmapTilesをimport, mbtilesファイルの存在するディレクトリをopen, ダウンロードの中断を行えるようにする.
   */
  private setupMenu(): void {
    this.menu.subMenuOfEdit.insert(
      2,
      new MenuItem({
        label: 'Import Map Tiles',
        click: (): void => {
          this.loadMap();
        },
      })
    );
    this.menu.subMenuOfEdit.insert(
      3,
      new MenuItem({
        label: 'Open Map Directory',
        click: (): void => {
          this.mbTileURLCreator.openMapDirectory();
        },
      })
    );
    this.menu.subMenuOfEdit.insert(
      4,
      new MenuItem({
        label: 'Cancel MBTiles download',
        click: async (): Promise<void> => {
          const result = await this.displayDownloadCancelchecker();
          if (result.response === 1) {
            return;
          }
          this.mbTiledownloader.cancel();
          this.displayDownloadCanceled();
        },
      })
    );
  }
  //#region  初期設定用のメソッド

  /**
   * アイコンをクリックしたらimportできるようにする
   */
  private setupClickEventhandler(): void {
    this.mapDragDropNode.on('click', () => {
      this.loadMap();
    });
    this.mapImportButtonNode.on('click', () => {
      this.loadMap();
    });
  }

  /**
   * デフォルトのDOMシステムのイベント機能を無効にして, mbtilesファイルをmapノードにdropした際にも,
   * mbtilesファイルを取り込むことができるようにイベントを準備する
   */
  private setupDragDropEventHandler(): void {
    $(document).on('drop', function (e) {
      e.preventDefault();
      e.stopPropagation();
    });
    $(document).on('dragover', function (e) {
      e.preventDefault();
      e.stopPropagation();
    });
    this.mapDragDropNode.on('drop', this.mapDropEventHandler.bind(this));
  }

  private mapDropEventHandler(
    event: JQuery.DropEvent<HTMLElement, undefined, HTMLElement, HTMLElement>,
    dropErr: Error
  ): void {
    event.preventDefault();
    event.stopPropagation();
    if (
      event.originalEvent !== undefined &&
      event.originalEvent.dataTransfer !== null
    ) {
      if (dropErr) {
        this.displayImportError(dropErr);
        return;
      }
      const items = Array.from(event.originalEvent.dataTransfer.files);
      const filePaths = items.map((item) => {
        return item.path;
      });
      this.mbTileURLCreator.createFromDesignatedMBTileOf(filePaths);
    }
  }

  /**
   * mbtTilesが見つからなかった時に, ノードへのイベントリスナの用意と, ユーザーに対してmbtilesをdlするように促すdialogの表示を行う
   */
  private setupDownLoadEventHandler(): void {
    this.downloadNode.on('click', (event) => {
      event.stopPropagation();
      this.mbTiledownloader.download();
    });
    // [NOTE] 色を変えたほうが分かりやすいので変えるように設定する
    this.downloadNode.on('mouseenter', () => {
      this.downloadNode?.css({ color: '#FF0000' });
    });
    this.downloadNode.on('mouseleave', () => {
      this.downloadNode?.css({ color: '#00FFFF' });
    });
  }
  //#endregion

  //#region ダイアログを開いてユーザーに情報を知らせる関数

  private displayImportError(err: Error): Promise<MessageBoxReturnValue> {
    console.log(err);
    return dialog.showMessageBox({
      title: 'Import Error',
      type: 'error',
      buttons: ['OK'],
      message: 'Import Error' + err.message,
      detail: 'Unable to Import Map Data.',
    });
  }

  private displayDownloading(): Promise<MessageBoxReturnValue> {
    return dialog.showMessageBox({
      title: 'Now Downloading',
      type: 'error',
      buttons: ['OK'],
      message: 'Now Downloading',
      detail: 'Please wait for dowlond to complete',
    });
  }
  /**
   * ダウンロードを手動でキャンセルする際に, その旨を確認する.
   */
  private displayDownloadCancelchecker(): Promise<MessageBoxReturnValue> {
    return dialog.showMessageBox({
      title: 'download cancel',
      type: 'error',
      buttons: ['Yes, I do.', "No, I don't"],
      message: 'Download is in progress. Are you sure you want to cancel?',
    });
  }

  private displayDownloadCanceled(): Promise<MessageBoxReturnValue> {
    return dialog.showMessageBox({
      title: 'download canceled',
      type: 'info',
      buttons: ['OK'],
      message: 'canceled download',
    });
  }
  //#endregion
}
