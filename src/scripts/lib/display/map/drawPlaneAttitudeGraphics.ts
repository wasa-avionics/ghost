import {
  ImageOnMap,
  createGeoJSONEllipse,
} from './preparePlaneAttitudeGraphics';

// [NOTE] mapBoxは位置情報のソースとよばれるものに画像などをレイヤーと呼ばれるものとして重ねて表示します.

/**
 * imageがundefinedであった場合には, 何も処理を行わない.
 * imageがundefinedでなければ, mapBox上に画像を表示する.
 * icon-imageオプションはidの値と同じになります.
 * @param mapBoxMap mapBoxのmap本体
 * @param id mapBox上で管理する際に必要な一意な名前
 * @param image 表示したい画像の情報
 * @param iconMagnificationPower 表示する画像の拡大率.
 * @param iconOffset 位置調整用
 */
export function drawMapBoxImage(
  mapBoxMap: mapboxgl.Map,
  id: string,
  source: string,
  image: ImageOnMap | undefined,
  iconMagnificationPower: number,
  iconOffset: number[]
): void {
  if (!image) {
    return;
  }
  mapBoxMap.addImage(id, image);
  mapBoxMap.addLayer({
    id: id,
    type: 'symbol',
    source: source,
    layout: {
      'icon-image': id,
      'icon-size': iconMagnificationPower,
      'icon-offset': iconOffset,
      'icon-rotation-alignment': 'map',
      'icon-allow-overlap': true,
      'icon-ignore-placement': true,
    },
  });
}

/**
 * Layerのidとsource名は同じ名前になります.
 */
export function drawErrorEllipse(
  mapBoxMap: mapboxgl.Map,
  id: string,
  degToRad: number,
  center: number[]
): mapboxgl.GeoJSONSource {
  mapBoxMap.addSource(
    id,
    createGeoJSONEllipse(
      center,
      10, // updateする前の初期値なので意味はない.
      10,
      degToRad
    )
  );
  mapBoxMap.addLayer({
    id: id,
    type: 'fill',
    source: id,
    layout: {},
    paint: {
      'fill-color': 'blue',
      'fill-opacity': 0.6,
    },
  });
  return mapBoxMap.getSource(id) as mapboxgl.GeoJSONSource;
}
