import { EventEmitter } from 'events';
import { IpcRendererEvent, ipcRenderer } from 'electron';

/* [NOTE] mapbox単体では, オンラインでつながっていないとmbtiles形式でのファイルを使えないのでここで地図情報をダウンロードします.
   [TODO] 以下のURLは古いもので代替のものを見つけてくる必要があります. 候補https://openmaptiles.com/downloads/tileset/osm/asia/japan/tokyo/
          ここはリンクをアカウントごとに自動生成するみたいなので今のmapのままではつらいです. 
          案としては, 
          [1] ユーザーにこのサイトまで誘導する手順をお願いして, リンクを生成したら手動でURLを貼り付けてもらう
          [2] 開発者ツールを見て, 通信がどういうHTTPリクエストを投げているかを見て, これをこちらで模倣してなんとかやる
*/
const mapDownloadURL = 'http://www.space.tokyo.jp/ftp1/osm_tiles.mbtiles';

/**
 * MBTileDownLoaderを使用する際には必ず, ダウンロード先をセットしてください.
 */
export interface MBTileDownLoader {
  isDownloading(): boolean;
  setDestination(dirName: string): void;
  displayNoMBTile(): void;
  download(): void;
  cancel(): void;
}

/**
 * ユーザーがMBTilesファイルをダウンロードしたい場合にダウンロードを実行することを担当する.
 * ダウンロード先は, interfaceを使う側で設定してもらう.
 */
export class MBTileDownloaderCore extends EventEmitter
  implements MBTileDownLoader {
  private downloadStatusListener:
    | undefined
    | ((event: IpcRendererEvent, ...args: string[]) => void);
  private downloadStateListener:
    | undefined
    | ((event: IpcRendererEvent, ...args: string[]) => void);
  private dragDropLayoverNode: JQuery<HTMLElement>;
  private mapTilesDirectory: string | undefined;
  private isDownloadStarted: boolean;

  constructor(dragDropLayoverNode: JQuery<HTMLElement>) {
    super();
    this.dragDropLayoverNode = dragDropLayoverNode;
    this.isDownloadStarted = false;
    this.dragDropLayoverNode.html(
      "Import your .mbtiles file here <br/> or <a id='download-map'style='z-index:30; background-color:black; color:#00FFFF;'>click here to download (1.74GB)</a>"
    );
  }

  public setDestination(dirName: string): void {
    this.mapTilesDirectory = dirName;
  }

  public isDownloading(): boolean {
    return this.isDownloadStarted;
  }

  /**
   * ユーザーがまだmbtilesファイルをダウンロードしていない場合に, メインプロセスにマップをダウンロードをお願いするイベントを通知し
   * リスナを用意して, メインプロセスからの応答を待機することを行う
   */
  public download = async (): Promise<void> => {
    if (this.isDownloadStarted) {
      console.log('ダウンロード中です.');
      return;
    }
    this.dragDropLayoverNode.html('downloading...');
    ipcRenderer.send(
      'downloadFiles',
      'Map',
      mapDownloadURL,
      this.mapTilesDirectory
    );
    this.appendDownloadIPCListener();
    this.isDownloadStarted = true;
    return Promise.resolve();
  };

  public cancel(): void {
    ipcRenderer.send('downloadCancel', 'Map');
    this.isDownloadStarted = false;
    this.removeDownloadIPCListener();
    this.displayNoMBTile();
  }

  public displayNoMBTile(): void {
    this.dragDropLayoverNode.html(
      "Import your .mbtiles file here <br/> or <a id='download-map'style='z-index:30; background-color:black; color:#00FFFF;'>click here to download (1.74GB)</a>"
    );
  }

  /**
   * ダウンロードをメインプロセスに依頼する際に必要になった不要なリスナを解除する
   * イベント名はメインプロセス(app.ts)で生成されるものに対応しているので変更する場合には両方変える.
   */
  private removeDownloadIPCListener(): void {
    if (this.downloadStatusListener !== undefined) {
      ipcRenderer.removeListener(
        'downloadStatusMap',
        this.downloadStatusListener
      );
      this.downloadStatusListener = undefined;
    }
    if (this.downloadStateListener !== undefined) {
      ipcRenderer.removeListener(
        'downloadStateMap',
        this.downloadStateListener
      );
      this.downloadStateListener = undefined;
    }
  }

  /**
   * app.tsのメインプロセスからダウンロード状況が送られてくるので監視する.
   */
  private appendDownloadIPCListener(): void {
    this.downloadStatusListener = (
      _event: IpcRendererEvent,
      receivedBytes: string
    ): void => {
      this.dragDropLayoverNode.html(
        'downloading... <br>' +
          (parseFloat(receivedBytes) / 17408901.12).toFixed(1) +
          '% of 1.74GB'
      );
    };
    this.downloadStateListener = (): void => {
      this.isDownloadStarted = false;
      this.emit('downloadCompleted');
    };
    ipcRenderer.on('downloadStatusMap', this.downloadStatusListener);
    ipcRenderer.once('downloadStateMap', this.downloadStateListener);
  }
}
