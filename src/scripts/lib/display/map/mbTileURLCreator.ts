import path from 'path';
import settings from 'electron-settings';
import fs from 'fs';
import mv from 'mv';
import {
  remote,
  MessageBoxReturnValue,
  OpenDialogReturnValue,
  shell,
} from 'electron';
const { dialog } = remote;
import { EventEmitter } from 'events';

// 型情報のないモジュールなので, Typescript側では処理しない. requireのままにしておく.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const MBTiles = require('@mapbox/mbtiles');

/**
 * ユーザーのappディレクトリ内にmbtilesファイルを格納する際に, 新しいディレクトリを作成するために必要.
 * ここに入れた名前のフォルダが作成されて, そこにmbtilesファイルが移動される
 * @see openMapDirectory
 * @see mapTilesDirectory
 */
const mapTilesDirName = 'mapTiles';

// 有効ではないMBTileを保持するフォルダの名前
const invalidFilesDirName = 'invalids';

const serverLocation = 'http://localhost:3000';

export interface MBTileURLCreator {
  create(): Promise<string[] | undefined>;
  createFromDesignatedMBTileOf(
    filePath: string[]
  ): Promise<string[] | undefined>;
  getMBTileDirectory(): string;
  openMapDirectory(): void;
}

/**
 * 有効なMBtileの選択において, Promiseチェインで流す際に, 真偽値も共に流す必要があった.
 */
type MBTileFileNameWithValidity = {
  fileName: string;
  isValid: boolean;
};

/**
 * MapBoxがmapを表示させる時に必要な, mapの各種座標を管理するデータベースの中心となるmbtiles形式のファイルの専用のURLを作成することを担当する.
 * app.tsの方で, expressを用いてgetリクエストを処理する部分に対応するリクエストをmapboxの内部処理が投げるように設定する.
 */
export class MBTileURLCreatorCore extends EventEmitter
  implements MBTileURLCreator {
  private mapTilesDirectory: string;
  private invalidMBTileFileDirectory: string;

  constructor() {
    super();
    this.mapTilesDirectory = path.join(
      path.dirname(settings.file()),
      mapTilesDirName
    );
    this.invalidMBTileFileDirectory = path.join(
      this.mapTilesDirectory,
      invalidFilesDirName
    );
  }

  public getMBTileDirectory(): string {
    return this.mapTilesDirectory;
  }

  /**
   * electron-settingsで決められているデフォルトのフォルダに作成されたmbtiles用のフォルダを開く
   */
  public openMapDirectory(): void {
    this.makeNotExistingDirectoryOf(this.mapTilesDirectory);
    shell.openItem(this.mapTilesDirectory);
  }

  /**
   * データベースサーバーを立ち上げる際に有効であるMBTileのURLを返すPromiseを作成する.
   * 有効なMBTileが見つからなかった場合には, ユーザーにファイルを別のフォルダから拾ってきてもらい,
   * それでも見つからなかった場合には, undefinedを返すPromiseを作成する.
   */
  public create = async (): Promise<string[] | undefined> => {
    const x = await this.fetchMBTilesToValidate();
    const MBTileFileNames = x ? x : await this.selectMBTile();

    if (!MBTileFileNames) {
      return undefined;
    }

    const PromiseArray: Promise<
      MBTileFileNameWithValidity
    >[] = this.createPromiseArrayFromArray<string, MBTileFileNameWithValidity>(
      MBTileFileNames,
      this.validateMBTileFiles
    );

    const result: MBTileFileNameWithValidity[] = await Promise.all(
      PromiseArray
    );
    await this.removeInvalidMBTileFiles(result);
    const validFileNames = this.getValidMBTileFiles(result);
    const validFileURLs = validFileNames?.map((validFileName) => {
      return this.createTileURL(validFileName);
    });
    return validFileURLs;
  };

  /**
   * 専用のフォルダ以外にあるMBTileを読み込み専用のURLを作成する.
   * @param filePath ファイルのパス
   */
  public createFromDesignatedMBTileOf(
    filePath: string[]
  ): Promise<string[] | undefined> {
    this.moveMBTileFilesOf(filePath);
    return this.create();
  }

  //#region メインのprivateメソッド

  /**
   * mapTilesDirectoryに存在しているであろうmbtilesを読み込む.
   */
  private fetchMBTilesToValidate = async (): Promise<string[] | undefined> => {
    this.makeNotExistingDirectoryOf(this.mapTilesDirectory);

    const items = await fs.promises
      .readdir(this.mapTilesDirectory)
      .catch((err) => {
        console.log('Attempt Map Load', err);
        return undefined;
      });

    return this.createOnlyMBTileFilesArray(items);
  };

  /**
   * mbTileファイルが見つからなかった時に,
   * ユーザーにお願いして, .mbtileファイルを選択してもらい, 専用のフォルダに移動する.
   * ユーザーが.mbtileファイルを選択しなかった場合にはundefinedを返す.
   * @returns 拡張子を含むファイルの名前.
   */
  private selectMBTile = async (): Promise<string[] | undefined> => {
    const messageBoxReturnValue: MessageBoxReturnValue = await this.displayNoMBTiles();
    if (messageBoxReturnValue.response != 1) {
      return undefined;
    }

    const result: OpenDialogReturnValue = await this.displayOpenMbtiles();

    if (!result.filePaths || result.filePaths.length <= 0) {
      return undefined;
    }
    this.moveMBTileFilesOf(result.filePaths);

    return result.filePaths.map((filePath) => {
      return path.basename(filePath);
    });
  };

  /**
   * MBTileサーバを試験的に起動してみて, エラーが出るかを判断し,
   *  エラーが出たらそのfileは有効でない, エラーがでなければそのfileは有効であるとして,
   * 引数に与えられたMBTileファイルが有効かどうかの確認を行う.
   * @param fileName MBTileFileの名前(Pathではない)
   */
  private validateMBTileFiles = (
    fileName: string
    // [NOTE] クラス内メソッドでpromiseを用いる際には, thisが外れるので, 関数型的に書くか, .bind(this)をつける.
  ): Promise<MBTileFileNameWithValidity> => {
    return new Promise(
      (resolve) =>
        new MBTiles(
          path.join(this.mapTilesDirectory, fileName),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (err: Error, mbtiles: any) => {
            if (err) {
              resolve({
                fileName: fileName,
                isValid: false,
              });
            }
            mbtiles.getTile(0, 0, 0, (err: Error) => {
              if (err) {
                resolve({
                  fileName: fileName,
                  isValid: false,
                });
              }
              resolve({
                fileName: fileName,
                isValid: true,
              });
            });
          }
        )
    );
  };

  /**
   * 有効だと分かったMBTileの名前のみからなる配列を返す.
   * 配列の要素数が0であった場合にはundefinedを返す.
   */
  private getValidMBTileFiles(
    fileNames: MBTileFileNameWithValidity[]
  ): string[] | undefined {
    const validFileNames = fileNames
      .filter((mbTileFilePathWithValidity: MBTileFileNameWithValidity) => {
        return mbTileFilePathWithValidity.isValid;
      })
      .map(
        (mbTileFilePathWithValidity: MBTileFileNameWithValidity) =>
          mbTileFilePathWithValidity.fileName
      );
    return validFileNames.length > 0 ? validFileNames : undefined;
  }

  private removeInvalidMBTileFiles = async (
    fileNames: MBTileFileNameWithValidity[]
  ): Promise<void> => {
    const invalidFileNames = fileNames
      .filter((mbTileFilePathWithValidity: MBTileFileNameWithValidity) => {
        return !mbTileFilePathWithValidity.isValid;
      })
      .map(
        (mbTileFilePathWithValidity: MBTileFileNameWithValidity) =>
          mbTileFilePathWithValidity.fileName
      );
    // [NOTE] 空の配列に対して, reduce関数を適用すると, Reduce of empty array with no initial valueエラーが出るため, はじいておく.
    if (invalidFileNames.length <= 0) {
      return;
    }

    await this.displayInvalidMbtilesFiles(
      invalidFileNames.reduce((prev, next) => prev + '/n' + next)
    );

    for (const invalidFilePath of invalidFileNames) {
      mv(
        path.join(this.mapTilesDirectory, invalidFilePath),
        path.join(this.invalidMBTileFileDirectory, invalidFilePath),
        (err) => {
          if (err) console.log('mv', err);
        }
      );
    }
  };

  /**
   * mapboxにおいて表示する, 特別な意味をもつmapのURLを生成する. mapStyleの設定時に必要.
   * expressと連携して用いる. serverLocationが3000ポートであることと, app.tsで3000ポートをListenすることは対応している.
   * /{z}/{x}/{y}の部分にはそれぞれzoomのレベル, 世界を分割した際に左上を原点としてとった分割タイルのx座標, y座標が入る.
   * https://docs.mapbox.com/ios/api/maps/5.8.0/tile-url-templates.html
   */
  private createTileURL(mbTileFileName: string): string {
    return (
      serverLocation +
      '/' +
      mapTilesDirName +
      '/' +
      path.parse(mbTileFileName).name +
      '/{z}/{x}/{y}.pbf'
    );
  }

  /**
   * mbtilesファイルを専用のフォルダへ移動する.
   * @param filePaths ディレクトリ名を含むファイルパス
   */
  private moveMBTileFilesOf(filePaths: string[]): void {
    for (const filePath of filePaths) {
      mv(
        filePath,
        path.join(this.mapTilesDirectory, path.basename(filePath)),
        (err): void => {
          if (err) {
            console.log(new Error(err));
          }
        }
      );
    }
  }
  //#endregion

  //#region 補助的な関数群

  /**
   * .mbTIles拡張子のファイルのみからなる配列を返す
   * 受け取ったファイルの配列の中に.mbTiles拡張子のファイルが存在しなかった場合にはundefinedを返す
   */
  private createOnlyMBTileFilesArray(
    files: string[] | undefined
  ): string[] | undefined {
    const MBTilesFiles = files?.filter((fileName) =>
      this.isMbtilesFile(fileName)
    );
    if (!MBTilesFiles) {
      return undefined;
    }
    return MBTilesFiles.length > 0 ? MBTilesFiles : undefined;
  }
  private isMbtilesFile(filePath: string): boolean {
    return path.parse(filePath).ext === '.mbtiles';
  }

  private makeNotExistingDirectoryOf(path: fs.PathLike): void {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }
  }

  /**
   * ある配列があった時に, その中身一つ一つに対してある型を返すPromiseを作成して, Promiseの配列を作る関数
   * @param Array ある一つの型で作られている配列
   * @param func Arrayの中身の型を引数にとって, Promise化して返す関数
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private createPromiseArrayFromArray<T extends any, K extends any>(
    Array: T[],
    func: (x: T) => Promise<K>
  ): Promise<K>[] {
    const PromiseArray: Promise<K>[] = [];
    for (const x of Array) {
      PromiseArray.push(func(x));
    }
    return PromiseArray;
  }

  //#endregion

  //#region ダイアログを開いてユーザーに情報を知らせる, もしくは, ファイルを選択させるメソッド

  /**
   * 拡張子はmbtilesだが, 中身が不正なファイルが見つかった時に使用する
   */
  private displayInvalidMbtilesFiles(
    invalidFileNames: string
  ): Promise<MessageBoxReturnValue> {
    return dialog.showMessageBox({
      title: 'Invalid Map Data Files Found',
      type: 'error',
      buttons: ['OK'],
      message: 'Invalid Map Data Files Found',
      detail: invalidFileNames,
    });
  }

  private displayOpenMbtiles(): Promise<OpenDialogReturnValue> {
    return dialog.showOpenDialog({
      title: 'Import MBTiles',
      properties: ['openFile', 'treatPackageAsDirectory'],
      filters: [{ name: 'MBTiles', extensions: ['mbtiles'] }],
    });
  }

  private displayNoMBTiles(): Promise<MessageBoxReturnValue> {
    return dialog.showMessageBox({
      title: 'Missing Map Tiles',
      type: 'error',
      buttons: ['Ignore', 'Import'],
      message: 'Missing Map Tiles',
      detail: 'Supported data type: mbtiles',
    });
  }

  //#endregion
}
