import mapboxgl, { GeoJSONSourceRaw } from 'mapbox-gl';
import { EventEmitter } from 'events';
import { ReactiveProperty } from '../../reactiveProperty';
import { Point } from 'geojson';
import {
  ImageOnMap,
  createGeoJSONEllipse,
  createArrow,
  createPlaneNav,
} from './preparePlaneAttitudeGraphics';
import { drawErrorEllipse, drawMapBoxImage } from './drawPlaneAttitudeGraphics';

const DegToRad = Math.PI / 180;

export interface MapGraphicsUpdater {
  update(): Promise<void>;
}

/**[NOTE] MapBoxのmap上に表示している矢印の方向をデータに一致させるように更新するなど, グラフィック内部のデータの更新を行う.
 *  [TODO] データを中心の設計にして, データを監視しておき, 変更が見られたら自動で更新するようにするべきであるが, 巨大クラスの分離後に行う.
 */
class MapGraphicsUpdaterCore extends EventEmitter
  implements MapGraphicsUpdater {
  private dataLongitude: ReactiveProperty;
  private dataLatitude: ReactiveProperty;
  private dataYaw: ReactiveProperty;
  private dataLongitudeError: ReactiveProperty;
  private dataLatitudeError: ReactiveProperty;
  private dataHdop: ReactiveProperty;
  private dataAccelX: ReactiveProperty;
  private dataAccelY: ReactiveProperty;
  private dataGPSSpeed: ReactiveProperty;
  private dataGPSCourse: ReactiveProperty;

  private planeNavigationPoint: Point;
  private planeSource: mapboxgl.GeoJSONSource | undefined;
  private planeCircle: mapboxgl.GeoJSONSource | undefined;
  private courseArrowVisible: boolean;
  private accelArrowVisible: boolean;

  private planeImage: ImageOnMap | undefined;
  private courseArrowImage: ImageOnMap | undefined;
  private accelArrowImage: ImageOnMap | undefined;

  private map: mapboxgl.Map;

  constructor(
    dataLongitude: ReactiveProperty,
    dataLatitude: ReactiveProperty,
    dataYaw: ReactiveProperty,
    dataLongitudeError: ReactiveProperty,
    dataLatitudeError: ReactiveProperty,
    dataHdop: ReactiveProperty,
    dataAccelX: ReactiveProperty,
    dataAccelY: ReactiveProperty,
    dataSpeed: ReactiveProperty,
    dataCourse: ReactiveProperty,
    map: mapboxgl.Map
  ) {
    super();
    this.dataLongitude = dataLongitude;
    this.dataLatitude = dataLatitude;
    this.dataYaw = dataYaw;
    this.dataLongitudeError = dataLongitudeError;
    this.dataLatitudeError = dataLatitudeError;
    this.dataHdop = dataHdop;
    this.dataAccelX = dataAccelX;
    this.dataAccelY = dataAccelY;
    this.dataGPSSpeed = dataSpeed;
    this.dataGPSCourse = dataCourse;
    this.map = map;
    this.planeNavigationPoint = {
      type: 'Point',
      coordinates: [139.523889, 35.975278],
    };

    this.courseArrowVisible = true;
    this.accelArrowVisible = true;
    this.courseArrowImage = createArrow('#00FF00', 255);
    this.accelArrowImage = createArrow('#FF00FF', 255);
    this.planeImage = createPlaneNav('#FF0000', 255);
    this.drawPlaneAttitudeGraphics();
  }

  public async update(): Promise<void> {
    this.updatePlanePosition();
    this.updateErrorEllipse();
    this.updatePlaneNose();
    this.updatePlaneGPSDirection();
    this.updateAccelerationDirection();
  }

  //#region update関連のメソッド

  /**
   * 位置情報の更新
   */
  private updatePlanePosition(): void {
    if (this.isAllDupe(this.dataLongitude, this.dataLatitude)) {
      return;
    }
    this.planeNavigationPoint.coordinates[0] = this.dataLongitude.getValue();
    this.planeNavigationPoint.coordinates[1] = this.dataLatitude.getValue();
    this.planeSource?.setData({
      type: 'Feature',
      properties: {},
      geometry: this.planeNavigationPoint,
    });
  }

  /**
   * 誤差楕円の更新
   */
  private updateErrorEllipse(): void {
    if (
      this.isAllDupe(
        this.dataLongitude,
        this.dataLatitude,
        this.dataHdop,
        this.dataLongitudeError,
        this.dataLatitudeError
      )
    ) {
      return;
    }
    if (this.planeCircle === undefined) {
      return;
    }
    const lngCalcError = this.getProbability95Radius(
      this.dataLongitude,
      this.dataHdop
    );
    const latCalcError = this.getProbability95Radius(
      this.dataLatitudeError,
      this.dataHdop
    );
    const x = (createGeoJSONEllipse(
      this.planeNavigationPoint.coordinates,
      lngCalcError,
      latCalcError,
      DegToRad
    ) as GeoJSONSourceRaw).data;
    if (x !== undefined) {
      this.planeCircle.setData(x);
    }
  }

  /**
   *測位点はUERE(利用者等価測距誤差UERE(UserEquivalent Ranging Error))に水平精度低下率HDOPをかけたものの2倍を半径とする円内に95%が入るので, その半径を求める
   * @param uere 利用者等価測距誤差
   * @param hdop 水平精度低下率
   */
  private getProbability95Radius(
    uere: ReactiveProperty,
    hdop: ReactiveProperty
  ): number {
    return uere.getValue() * hdop.getValue() * 2;
  }

  /**
   * 機首方向の更新
   */
  private updatePlaneNose(): void {
    if (this.dataYaw.isDupe()) {
      return;
    }
    if (this.map === null) {
      return;
    }
    this.map.setLayoutProperty(
      'planeImage',
      'icon-rotate',
      this.dataYaw.getValue()
    );
  }

  /*
   * GPS進行方向の更新
   */
  private updatePlaneGPSDirection(): void {
    if (this.isAllDupe(this.dataGPSCourse, this.dataGPSSpeed)) {
      return;
    }
    if (this.map === null) {
      return;
    }
    const gpsCourse = this.dataGPSCourse.getValue();
    const gpsSpeed = this.dataGPSSpeed.getValue();
    // [NOTE] 測定精度的に静止していても完全に0にはならないので, 0.1より大きいという条件を付している
    if (gpsSpeed > 0.1) {
      // もしvisibleでない時にはまず, visibleにする.
      if (!this.courseArrowVisible) {
        this.map.setLayoutProperty('courseArrowImage', 'visibility', 'visible');
        this.courseArrowVisible = true;
      }
      this.map.setLayoutProperty('courseArrowImage', 'icon-rotate', gpsCourse); // gpsCourseの分だけ回転させる
      // ほとんど進んでいない時には進行方向は表示しない.
    } else {
      if (this.courseArrowVisible) {
        this.map.setLayoutProperty('courseArrowImage', 'visibility', 'none');
        this.courseArrowVisible = false;
      }
    }
  }

  /**
   * 加速度方向の更新
   */
  private updateAccelerationDirection(): void {
    if (this.isAllDupe(this.dataAccelX, this.dataAccelY, this.dataYaw)) {
      return;
    }
    const accelX = this.dataAccelX.getValue();
    const accelY = this.dataAccelY.getValue();
    const accel = Math.sqrt(accelX * accelX + accelY * accelY);
    if (accel > 1) {
      let accelAngle =
        this.dataYaw.getValue() + Math.atan(accelX / accelY) / DegToRad; // 加速度矢印の角度(degree)
      if (accelY < 0) accelAngle += 180;
      if (this.map !== null) {
        // もしvisibleでない時にはまず, visibleにする.
        if (!this.accelArrowVisible) {
          this.map.setLayoutProperty(
            'accelArrowImage',
            'visibility',
            'visible'
          );
          this.accelArrowVisible = true;
        }
        this.map.setLayoutProperty(
          'accelArrowImage',
          'icon-rotate',
          accelAngle
        );
      }
      // ほとんど加速度が生じていない時には進行方向は表示しない.
    } else {
      if (this.map !== null && this.accelArrowVisible) {
        this.map.setLayoutProperty('accelArrowImage', 'visibility', 'none');
        this.accelArrowVisible = false;
      }
    }
  }

  private isAllDupe(...args: ReactiveProperty[]): boolean {
    for (const x of args) {
      if (!x.isDupe()) {
        return false; // ひとつでもDupeでないのがあったら即座にfalseを返す
      }
    }
    return true;
  }
  //#endregion

  /**
   * mapに表示する各種画像の初期設定
   */
  private drawPlaneAttitudeGraphics(): void {
    // 誤差楕円の設置
    this.planeCircle = drawErrorEllipse(
      this.map,
      'planeCircle',
      DegToRad,
      this.planeNavigationPoint.coordinates
    );

    // 機体位置情報のソース設定
    this.map.addSource('planeNav', {
      type: 'geojson',
      data: {
        type: 'Feature',
        properties: {},
        geometry: this.planeNavigationPoint, // 位置
      },
    });
    this.planeSource = this.map.getSource('planeNav') as mapboxgl.GeoJSONSource;

    // GPS進行方向矢印の設定・設置
    drawMapBoxImage(
      this.map,
      'courseArrowImage',
      'planeNav',
      this.courseArrowImage,
      1.0,
      [0, -63]
    );

    // 加速度矢印の設定・設置
    drawMapBoxImage(
      this.map,
      'accelArrowImage',
      'planeNav',
      this.accelArrowImage,
      1.0,
      [0, -63]
    );

    // 機体位置の設定・設置
    drawMapBoxImage(this.map, 'planeImage', 'planeNav', this.planeImage, 1.0, [
      0,
      39,
    ]);
  }
}

export interface MapGraphicsUpdaterCreatorCore {
  create(map: mapboxgl.Map): MapGraphicsUpdater;
}
/**
 * mapを拾ってきて, MapGraphicsUpdaterを作ることを担当する.
 */
export class MapGraphicsUpdaterCreatorCore {
  private dataLongitude: ReactiveProperty;
  private dataLatitude: ReactiveProperty;
  private dataYaw: ReactiveProperty;
  private dataLongitudeError: ReactiveProperty;
  private dataLatitudeError: ReactiveProperty;
  private dataHdop: ReactiveProperty;
  private dataAccelX: ReactiveProperty;
  private dataAccelY: ReactiveProperty;
  private dataGPSSpeed: ReactiveProperty;
  private dataGPSCourse: ReactiveProperty;
  constructor(
    dataLongitude: ReactiveProperty,
    dataLatitude: ReactiveProperty,
    dataYaw: ReactiveProperty,
    dataLongitudeError: ReactiveProperty,
    dataLatitudeError: ReactiveProperty,
    dataHdop: ReactiveProperty,
    dataAccelX: ReactiveProperty,
    dataAccelY: ReactiveProperty,
    dataGPSSpeed: ReactiveProperty,
    dataGPSCourse: ReactiveProperty
  ) {
    this.dataLongitude = dataLongitude;
    this.dataLatitude = dataLatitude;
    this.dataYaw = dataYaw;
    this.dataLongitudeError = dataLongitudeError;
    this.dataLatitudeError = dataLatitudeError;
    this.dataHdop = dataHdop;
    this.dataAccelX = dataAccelX;
    this.dataAccelY = dataAccelY;
    this.dataGPSSpeed = dataGPSSpeed;
    this.dataGPSCourse = dataGPSCourse;
  }
  public create(map: mapboxgl.Map): MapGraphicsUpdater {
    return new MapGraphicsUpdaterCore(
      this.dataLongitude,
      this.dataLatitude,
      this.dataYaw,
      this.dataLongitudeError,
      this.dataLatitudeError,
      this.dataHdop,
      this.dataAccelX,
      this.dataAccelY,
      this.dataGPSSpeed,
      this.dataGPSCourse,
      map
    );
  }
}
