/*eslint-disable*/
const path = require('path')
const merge = require('webpack-merge') // webpack-merge
const common = require('./webpack.common.js') // 汎用設定をインポート// common設定とマージする

const mainIndex = common.findIndex(config => config.target === 'electron-main');
const rendererIndex = common.findIndex(config => config.target === 'electron-renderer');

const devConfigForMain = {
    mode: "development", //開発用
    devtool: 'source-map',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'bundledJS')
    },
};

const devconfigForRenderer = {
    mode: "development", //開発用
    devtool: 'source-map',
    output: {
        filename: 'renderer.js',
        path: path.resolve(__dirname, 'bundledJS')
    },
}

const main = merge(common[mainIndex], devConfigForMain);
const renderer = merge(common[rendererIndex], devconfigForRenderer);

module.exports = [main, renderer];