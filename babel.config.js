const presets = [
    ["@babel/preset-env",
        {
            "targets": {
                "electron": "6.1.9" 
            }
    }]
];
module.exports = {
    presets
}