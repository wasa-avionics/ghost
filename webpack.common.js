/*eslint-disable*/
// [NOTE] やっていること: ts->node/electron6.1.9のバージョンによって使えなかったりするのでES5へと変換
//        -> css html font(.otf) 画像ファイルのパス解決-> 小さくまとめたファイルにする(バンドル)
//        ts変換: ts-loaderを使用. 
//        css html fontの解決についてはhtml - webpack - plugin / css - loader / file - loaderを使用
//        mapboxはsqlがない(これはmbtilesのlibフォルダにある.)などなんかいろいろと文句をいうので, mapboxについてはexternalsにしてbundleされるのを防止するべき. 
//        もし, typescriptからの変換でおかしいみたいなのがあれば, babelの検討もどうぞ.
//        開発用と本番用で分けているのは, 開発用だと最小化がきかないため.
// [NOTE] エラーへの対処法
//        1. ctrl+cで一回webpackのwatchモードを終了. キャッシュが悪さをする場合があるので, フォルダをいじったりした後は特に.
//        2. externalsへの追加の検討. モジュールが__dirNameなどを使っている場合にはbundleすると, ファイルの場所が違ってしまうので, そういうものは全てexternalsに突っ込んで, bundleを阻止する.
const path = require('path');
// [NOTE] htmlにscriptsTagを自動で埋め込んでくれるもの.
const HtmlWebpackPlugin = require('html-webpack-plugin')

var main = {
    target: 'electron-main',
    entry: {
        main: './src/app.ts'
    },
    node: {
        __dirname: false,
        __filename: false
    },
    module: {
        // [NOTE] rulesは入力されたファイルの依存しているファイルについて芋づる式に処理していく, その芋づる式につるされたモジュールを, 下から順にtestにかけて, マッチしていたら処理をloaderに投げるということを行う.
        //        下から順番に処理を充てられるので順番に注意.
        rules: [{
                test: /.ts?$/,
                use: {
                    loader: 'ts-loader'
                },
                include: [
                    path.resolve(__dirname, 'src'),
                ],
                exclude: [
                    path.resolve(__dirname, 'node_modules'),
                ]
            },
            {
                test: /\.(png|svg|jpg|gif|otf|css|json|mp3)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        // [NOTE] 名前を指定しないとmd5Hashでファイル名が出力されるので注意
                        name: '[name].[ext]',
                        // [NOTE] 出力先
                        outputPath: "static/",
                        // [NOTE] 出力先がどのパスをbaseにして決定されているのかをloaderに伝えてやる.
                        publicPath: './static'
                    }
                }]
            }
        ]
    },
    // [NOTE]sqlite3は動的importをしているためにwebpackで静的に依存を解消することができないためexternalsが必要(この部分はpackingしないで外部依存を残すという意味)
    externals: {
        sqlite3: 'commonjs sqlite3',
        express: 'commonjs express',
        // [NOTE] schemaがないっていわれるのでパックしない. __dirNameが使われているモジュールのその関数を用いている時には弾いてやらないとパスがおかしくなって死ぬ. 
        //        __dirNameはその関数が呼ばれた場所を示すので, webpackされることを考えていないものもパックしてしまうとライブラリ作成者がそれを意図していないので死亡する.
        '@mapbox/mbtiles': 'commonjs @mapbox/mbtiles',
    },
    resolve: {
        extensions: ['.js', '.ts']
    },
};
var renderer = {
    target: 'electron-renderer',
    entry: './src/scripts/setup.ts',
    node: {
        __dirname: false,
        __filename: false
    },
    resolve: {
        extensions: ['.json', '.js', '.jsx', '.css', '.ts', '.tsx']
    },
    module: {
        rules: [{
                test: /\.(tsx|ts)$/,
                use: {
                    loader: 'ts-loader'
                },
                include: [
                    path.resolve(__dirname, 'src'),
                    // [NOTE] rendererのほうにはnode_modulesの関数も埋め込んでしまう. バグの温床になるが高速になるのでそこはトレードオフ.
                    // [NOTE] これをしているのでmapboxやserialportをexternalsに入れて変なエラーが出ないようにしてやる必要がある. ライブラリの製作者が, 
                    //        関数がどこで実行されてもその挙動が不変であるように組んでいない関数(__dirNameがある)はwebpackでバンドルしてはいけない.externalsへどうぞ.
                    path.resolve(__dirname, 'node_modules'),
                ]
            },
            {
                test: /\.(html)$/,
                use: {
                    // [NOTE] htmlのimgタグに埋め込まれている画像のパスを解決して出力してくれる. それ以外に余計なことはさせない
                    loader: 'html-loader',
                    options: {
                        // [NOTE] html-loaderはデフォルトでは, imgタグのsrcのみを参照解決してくれるが, もし他のもいれたい時は, 
                        // [NOTE] 例えば, sourceタグのsrcsetも解決させたいと思ったらのattrs: ['img:src', 'source:srcset']とする.
                        attributes: {
                            list: [{
                                    tag: 'img',
                                    attribute: 'src',
                                    type: 'src',
                                },
                                {
                                    tag: 'link',
                                    attribute: 'href',
                                    type: 'src',
                                }
                            ]
                        },
                        // [NOTE] htmlの圧縮は意図しない動作をする危険性があるのでしない.
                        minimize: false,
                    }
                }
            },
            {
                // [NOTE] pngはこっちにはいれない. url-loaderで処理したものがおかしくなる　webpackのoneOfとか使うとうまくいくかもね.
                test: /\.(svg|jpg|gif|otf|css|json|mp3)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        // [NOTE] 名前を指定しないとmd5Hashでファイル名が出力されるので注意
                        name: '[name].[ext]',
                        // [NOTE] 出力先
                        outputPath: "static/",
                        // [NOTE] 出力先がどのパスをbaseにして決定されているのかをloaderに伝えてやる.
                        publicPath: './static'
                    }
                }]
            },
            {
                // [NOTE] JQueryがattr関数内でパスを求めているが,パスを直接指定すると, webpackが追跡の対象外(webpackはrequire\importのみを見る)にしてしまい,  
                //        パスを解決してくれない. したがって, requireで画像を拾ってくる必要があるが, すると今度はJqueryのattr(src, ~~~)で[object module]みたいなエラーが出てしまう.
                //        これはパスを解決したが, 画像が画像のままstringで付与されてしまうため, html側が何かわからなくて死んでいる.これを回避するには, Jqueryのattrに埋め込む際に既にbase64エンコード
                //        されていないといけない. 幸い, 既存の使われている画像のサイズはそれほど大きくないのでbase64エンコードしても大丈夫.
                //        そもそもこの問題はJqery側の関数でBase64エンコードなりしてくれないことや, reactのように画像を画像としてimportすることができないことに起因しているので, 
                //        何って, JQueryはwebpackと合わせるには向いていない.　JSはこの規模には保守コスト的にもう向かず, typescriptを使う以上, 既存のDOMの操作にはreactなりvueなり使うべき.
                //        力尽きたので今はすべてBase64エンコードで埋め込んでいます.
                test: /\.(png)$/i,
                use: [{
                    loader: 'url-loader',
                    options: {
                        // [NOTE] 名前を指定しないとmd5Hashでファイル名が出力されるので注意 ここではすべて埋め込むので関係ない.
                        // name: '"[path][name][hash]"',
                        // [NOTE] もともとpngなのでpngで出力する.
                        mimetype: 'image/png',
                        encoding: 'base64',
                        // [NOTE] コードの中で使われている連中は全部埋め込んでほしいのでfile-loaderに処理を任せる.　react使ってればこんなことする必要ないと思うのですがね.
                        limit: undefined,
                        fallback: require.resolve('file-loader'),
                        // [NOTE] url-loaderはデフォルトでESmoduleを吐いてしまうため. ESmoduleしてもts-loaderが良しなにしてくれるんじゃないのって思っても, 
                        //        おそらく吐くESmoduleはtypescriptが変換しようとする構文ではない(import/export)ために, [object module]が出てしまうのだと思われる.
                        esModule: false,
                    },
                }],

            },
        ]
    },
    externals: {
        sqlite3: 'commonjs sqlite3',
        // [NOTE] 書き方について, 上だと成功. 下だと失敗. commonjsってちゃんと入れようね. nodeはESではなくてcommonjsだからね. JSは複数の規格があるんやで.
        // [NOTE] serialport: 'serialport'
        // [NOTE]"serialport": "require('serialport')",
        serialport: 'commonjs serialport',
        '@mapbox/mbtiles': 'commonjs @mapbox/mbtiles',
    },
    plugins: [
        // [NOTE] webpackはrequire/importされていないhtmlファイルは見てくれないので追加で見てくれるようにする. 
        // [NOTE] imgタグ内のsrcを出力後の画像フォルダ内の画像を参照するようにしてやる.
        new HtmlWebpackPlugin({
            // [NOTE electronのmainプロセス側から使うので名前は変えない.
            filename: 'index.html',
            template: './src/index.html'
        }),
        /* [NOTE] もし将来的にbootstrapとかを使いたいと考えているならばこういうのを使ってみるといいかも
        new InterpolateHtmlPlugin({
            'bootstrap_css_cdn': 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css'
        })*/
    ]
};
// [NOTE] webpack-mergeを使う際に気を付けて. webpackのexportsをarrayのままwebpack mergeしたらbuildコケたのを修正した話 https://qiita.com/LuckyRetriever/items/7d2a52f0a6a5847890d0
//        mergeを使う側からこのファイルを拾ってきて, 直接mergeをしようとすると, configuration has an unknown property '1'. These properties are valid:
//        というエラーが出るので, 配列の中身とmergeしないといけなくて, devとprod側では配列の中身を取り出す作業を先に行っている. 
module.exports = [main, renderer];