# Ghost
Ghost is a BADASS Telemetry Monitor Application for Waseda Aeronautics and Space Associations's Human Powered Aircraft Project.

[日本語README](./README_jp.md)

## Installation
```
//TODO
```

## Contribution

[Node.js](https://nodejs.org/) must be installed for development.

#### - Setup

1. Open terminal for UNIX Systems or Command Prompt for Windows

2. Simply clone this repository to your computer. You may have to [install git](https://git-scm.com/downloads) or [GitHub Desktop](https://desktop.github.com). Since this is a private repository, you may also have to enter your GitHub username and password.
 
```bash
$ git clone https://github.com/MojamojaK/ghost.git
```

    The above command may pull old-version source code from old repositry in which the person before me deveopped fundamental system, so please type the below command. The repositry below is also private repositry. 
```bash
& git clone https://gitlab.com/wasa-avionics/ghost.git
```

3. Install all packages.
```bash
$ npm install
```

4. If you are on Windows, install [windows-build-tools](https://www.npmjs.com/package/windows-build-tools) to allow python use of npm. This enables rebuilding from source for the [serialport](https://www.npmjs.com/package/serialport) module.
```bash
$ npm --add-python-to-path='true' --debug install --global windows-build-tools
```

5. Run rebuild command. This rebuilds all plaftorm native packages to become compatible to [Electron](https://electronjs.org).
```bash
$ npm run rebuild
```
6. Transpile typescript into javascript. 

    Webpack will run in watch mode(see npm scripts), which mean you cannot use the terminal with which you type below command, so you should open two terminal if you want to do something with terminal, for example, running electron. 

    The photo was taken when I was developing. 

<img src="./assetForReadme/developingDisplay.JPG" alt="can not load" >

    For development
```bash
$ npm run webpack_dev
```
    For production
```bash
$ npm run webpack_prod
``` 

7. You may now start and run the application with.
```bash
$ npm run start
```

All codes should be implemented in the **src** directory.

Build resources such as application icons should be in the **build** directory.

#### - Building

Run the commands below for each platform.

Since codes are left unsigned, Operating Systems may initially reject installation.

Building Configurations should be in **build-config.json** . See [electron-builder](https://www.electron.build).

Built installers will be put in the **dist** directory

##### Windows
This will build a nsis installer for Windows 64bit.

**Will only work on Windows.** May work on Mac with [wine](https://www.winehq.org) installed, but is known to be unreliable.
```bash
$ npm run build_win
```

##### Mac 
This will build a dmg image for MacOS.
Works on all plaforms.
```bash
$ npm run build_mac
```

##### Linux
This will build an AppItem for Linux.
Works on all plaforms.
```bash
$ npm run build_linux
```
