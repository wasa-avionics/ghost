/*eslint-disable*/
const path = require('path')
const merge = require('webpack-merge') // webpack-merge
const common = require('./webpack.common.js') // 汎用設定をインポート

const mainIndex = common.findIndex(config => config.target === 'electron-main');
const rendererIndex = common.findIndex(config => config.target === 'electron-renderer');

const devConfigForMain = {
    mode: "production", //本番用 
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'bundledJS')
    }
};

const devConfigForRenderer = {
    mode: "production", //本番用 
    output: {
        filename: 'renderer.js',
        path: path.resolve(__dirname, 'bundledJS')
    }
};

const main = merge(common[mainIndex], devConfigForMain);
const renderer = merge(common[rendererIndex], devConfigForRenderer);

module.exports = [main, renderer];